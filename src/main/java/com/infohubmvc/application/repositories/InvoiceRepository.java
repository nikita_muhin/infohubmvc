package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    List<Invoice> findAllByUserId(Long userId);

    List<Invoice> findAllByCondominiumId(Long condominiumId);

    List<Invoice> findAllByBeneficiaryId(Long beneficiaryId);

}
