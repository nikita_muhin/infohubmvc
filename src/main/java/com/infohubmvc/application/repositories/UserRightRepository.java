package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.UserRight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRightRepository extends JpaRepository<UserRight, Long> {

    List<UserRight> findAllByUserId(Long userId);
    List<UserRight> findAllByUserIdAndCondominiumId(Long userId, Long condominiumId);
    void deleteByUserIdAndCondominiumIdAndHouseId(Long userId, Long condominiumId, Long houseId);
    void deleteByUserIdAndCondominiumId(Long userId, Long condominiumId);
}
