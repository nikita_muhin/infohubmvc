package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.DiscountType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DiscountTypeRepository extends JpaRepository<DiscountType, String> {

    Optional<DiscountType> getByCode(String code);

    Optional<DiscountType> findByCode(String code);

}
