package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Property;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Long> {
    List<Property> findAllByUserId(Long userId);

    List<Property> findAllByHouseId(Long houseId);

    Page<Property> findAllByUserId(Long userId, Pageable pageable);

    Page<Property> findAllByHouseId(Long houseId, Pageable pageable);

}
