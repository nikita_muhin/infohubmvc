package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.CondominiumAdministrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CondominiumAdministratorRepository extends JpaRepository<CondominiumAdministrator, Long> {

}
