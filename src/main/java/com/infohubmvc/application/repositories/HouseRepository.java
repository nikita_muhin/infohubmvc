package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.House;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface HouseRepository extends JpaRepository<House, Long> {

    Page<House> findAllByUserId(Long userId, Pageable pageable);

    List<House> findAllByCondominiumId(Long id);

    Page<House> findAllByCondominiumId(Long condominiumId, Pageable pageable);
}
