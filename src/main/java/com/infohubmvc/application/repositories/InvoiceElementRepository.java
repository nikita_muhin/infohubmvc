package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.InvoiceElement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceElementRepository extends JpaRepository<InvoiceElement, Long> {

    InvoiceElement getById(Long id);

    List<InvoiceElement> findAllByInvoiceId(Long id);

}
