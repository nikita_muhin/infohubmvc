package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Beneficiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long> {

    Optional<Beneficiary> getByServiceName(String serviceName);

    List<Beneficiary> findAllByUserId(Long userId);

}
