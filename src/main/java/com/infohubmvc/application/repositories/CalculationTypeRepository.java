package com.infohubmvc.application.repositories;


import com.infohubmvc.application.data.entity.CalculationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CalculationTypeRepository extends JpaRepository<CalculationType, String> {

    Optional<CalculationType> getByCode(String code);

    Optional<CalculationType> findByCode(String code);

}
