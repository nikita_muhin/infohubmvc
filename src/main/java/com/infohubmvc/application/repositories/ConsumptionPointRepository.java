package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.infohubmvc.application.data.entity.ServiceType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConsumptionPointRepository extends JpaRepository<ConsumptionPoint, Long> {

    Optional<ConsumptionPoint> getByServiceType(ServiceType serviceType);

    List<ConsumptionPoint> findAllByParentId(Long parentId);

    List<ConsumptionPoint> findAllByCondominiumId(Long condominiumId);

    List<ConsumptionPoint> findAllByHouseId(Long houseId);

    List<ConsumptionPoint> findAllByPropertyId(Long propertyId);

    @Query(value = "SELECT c FROM ConsumptionPoint c join UserRight r on c.property = r.property where r.user.id = ?1")
    Page<ConsumptionPoint> getAllForUserId(Long userId, Pageable pageable);

}
