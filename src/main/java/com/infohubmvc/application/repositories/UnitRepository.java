package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UnitRepository extends JpaRepository<Unit, String> {

    Optional<Unit> getByCode(String code);

    Optional<Unit> findByCode(String code);

}
