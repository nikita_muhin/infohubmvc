package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.PropertyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PropertyTypeRepository extends JpaRepository<PropertyType, String> {

    Optional<PropertyType> getByCode(String code);

    Optional<PropertyType> findByCode(String code);

}
