package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictionaryRepository extends JpaRepository<Dictionary, String> {

    List<Dictionary> findAll();
}
