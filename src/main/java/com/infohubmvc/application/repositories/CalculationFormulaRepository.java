package com.infohubmvc.application.repositories;


import com.infohubmvc.application.data.entity.CalculationFormula;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CalculationFormulaRepository extends JpaRepository<CalculationFormula, Long> {

    Optional<CalculationFormula> getByCode(String code);

    List<CalculationFormula> findAllByCalculationType(String calculationType);

    Page<CalculationFormula> findAllByCalculationType(String calculationType, Pageable pageable);

    List<CalculationFormula> findAllByUserId(Long userId);

}
