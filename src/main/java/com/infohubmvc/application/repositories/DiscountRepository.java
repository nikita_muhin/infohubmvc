package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.Discount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, Long> {

    @Query(value = "SELECT d FROM Discount d where ?1 between d.dateFrom and d.dateTo and d.active = true and d.discountType.code = ?2 ")
    Optional<Discount> getByDiscountType(LocalDate date, String discountTypeCode);

    List<Discount> findAllByUserId(Long userId);

    @Query(value = "SELECT d FROM Discount d where ?1 between d.dateFrom and d.dateTo and d.active = true ")
    List<Discount> findAllActiveForDate(LocalDate date);

    @Query(value = "SELECT d FROM Discount d where ?1 between d.dateFrom and d.dateTo and d.active = true ")
    Page<Discount> findAllActiveForDate(LocalDate date, Pageable pageable);

    @Query(value = "SELECT d FROM Discount d where d.dateTo < ?1 and d.active = true ")
    Page<Discount> findAllExpiredForDate(LocalDate date, Pageable pageable);

    @Query(value = "SELECT d FROM Discount d where d.dateFrom > ?1 and d.active = true ")
    Page<Discount> findAllScheduledForDate(LocalDate date, Pageable pageable);

    @Query(value = "SELECT d FROM Discount d where d.active = false ")
    Page<Discount> findAllDisabledForDate(Pageable pageable);

}
