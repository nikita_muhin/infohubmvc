package com.infohubmvc.application.repositories;

import com.infohubmvc.application.data.entity.ServiceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ServiceTypeRepository extends JpaRepository<ServiceType, String> {

    Optional<ServiceType> getByCode(String code);

    Optional<ServiceType> findByCode(String code);

}
