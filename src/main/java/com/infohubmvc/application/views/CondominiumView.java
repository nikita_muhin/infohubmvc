package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.CondominiumDialog;
import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.grids.CondominiumsGrid;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.service.CondominiumService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.tabs.content.HouseTabContent;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.security.PermitAll;
import java.util.Optional;

@PermitAll
@PageTitle("Condominiums")
@Route(value = "condominiums", layout = MainLayout.class)
@RouteAlias(value = "condominiums", layout = MainLayout.class)
@Transactional
public class CondominiumView extends AbstractView {

    private Condominium selectCondominium;
    private CondominiumService condominiumService;
    private ListDataProvider<Condominium> condominiumListDataProvider;
    private CondominiumsGrid condominiumsGrid;
    private Tabs tabs;
    private final Tab houseTab = new Tab("House");
    private final Tab condominiumTab = new Tab("Condominium");
    private VerticalLayout content;

    public CondominiumView() {
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        condominiumListDataProvider = new ListDataProvider<>(condominiumService.getAll());
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        configureTabs();
        add(tabs, content);
    }

    @Override
    protected void configureGrid() {
        condominiumsGrid = new CondominiumsGrid();
        condominiumsGrid.setHeightFull();
        condominiumsGrid.setDataProvider(condominiumListDataProvider);
        if (condominiumListDataProvider.getItems().size() != 0) {
            selectCondominium = condominiumListDataProvider.getItems().stream().findFirst().get();
        }
        condominiumsGrid.select(selectCondominium);
        condominiumsGrid.addSelectionListener(selection -> {
            Optional<Condominium> optionalCondominium = selection.getFirstSelectedItem();
            optionalCondominium.ifPresent(condominium -> selectCondominium = condominium);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openCondominiumDialog("Create condominium", new Condominium(), true, true));
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        setCreateButton(createButton);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addClickListener(event -> openCondominiumDialog("Edit", selectCondominium, true, false));
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        setEditButton(editButton);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete condominium \"%s\"?", selectCondominium.getName()),
                    "Are you sure you want to delete this user permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                condominiumService.deleteCondominium(selectCondominium);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addClickListener(event -> openCondominiumDialog("Information", selectCondominium, false, false));
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        condominiumListDataProvider.setFilter(s -> {
            boolean containsName = caseInsensitiveContains(s.getName(), event.getValue());
            boolean containsRegistrationNumber = caseInsensitiveContains(s.getRegistrationNumber(), event.getValue());
            boolean containsOfficeAddress = caseInsensitiveContains(s.getOfficeAddress(), event.getValue());
            boolean containsInfohubContractNr = caseInsensitiveContains(s.getInfohubContractNr(), event.getValue());
            boolean containsPhone = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getUserRoles(), event.getValue());
            return containsName || containsRegistrationNumber || containsOfficeAddress || containsInfohubContractNr || containsPhone;
        });
    }

    private void configureTabs() {
        tabs = new Tabs();
        tabs.add(condominiumTab);
        tabs.add(houseTab);
        tabs.setWidthFull();
        tabs.addSelectedChangeListener(event ->
                setContent(event.getSelectedTab())
        );
        content = new VerticalLayout();
        content.setSpacing(false);
        setContent(tabs.getSelectedTab());
    }

    private void setContent(Tab tab) {
        content.removeAll();
        content.setHeight("90%");
        content.getStyle().set("padding-top", "0%");
        content.setWidthFull();
        if (tab.equals(condominiumTab)) {
            content.add(topHorizontalLayout(), condominiumsGrid);
        } else {
            content.add(new HouseTabContent(selectCondominium));
        }
    }

    private void openCondominiumDialog(String headerString, Condominium condominium, boolean isEditable, boolean isCreate) {
        CondominiumDialog condominiumDialog;
        condominiumDialog = new CondominiumDialog(headerString, condominium, isEditable);
        condominiumDialog.getSaveButton().addClickListener(event -> {
            if (isCreate) {
                refreshGrid();
            } else {
                condominiumListDataProvider.refreshAll();
            }
        });
        condominiumDialog.getSaveButton().setEnabled(false);
        condominiumDialog.open();
    }

    private void refreshGrid() {
        condominiumListDataProvider = new ListDataProvider<>(condominiumService.getAll());
        condominiumsGrid.setDataProvider(condominiumListDataProvider);
    }
}
