package com.infohubmvc.application.views;


import com.infohubmvc.application.components.appnav.AppNav;
import com.infohubmvc.application.components.appnav.AppNavItem;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.dom.ThemeList;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.theme.lumo.Lumo;

/**
 * The main view is a top-level placeholder for other views.
 */
public class MainLayout extends AppLayout {

    private H1 viewTitle;

    public MainLayout() {
        setPrimarySection(Section.DRAWER);
        addToNavbar(true, createHeaderContent());
        addToDrawer(createDrawerContent());
    }

    private Component createHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
        toggle.addClassNames("view-toggle");
        toggle.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
        toggle.getElement().setAttribute("aria-label", "Menu toggle");

        viewTitle = new H1();
        viewTitle.addClassNames("view-title");

        Anchor logout = new Anchor("logout");
        logout.add("Log Off");
        logout.getStyle().set("margin-left", "auto");
        logout.getStyle().set("margin-right", "15px");

        Header header = new Header(toggle, viewTitle, logout);
        header.addClassNames("view-header");
        return header;
    }

    private Component createDrawerContent() {
        H2 appName = new H2("InfoHUBMVC");
        appName.addClassNames("app-name");

        com.vaadin.flow.component.html.Section section = new com.vaadin.flow.component.html.Section(appName,
                createNavigation(), createFooter());
        section.addClassNames("drawer-section");
        return section;
    }

    private AppNav createNavigation() {
        AppNav nav = new AppNav();
        nav.addClassNames("app-nav");
        nav.addItem(new AppNavItem("Houses", HouseView.class, "la la-home"));
        nav.addItem(new AppNavItem("Condominiums", CondominiumView.class, "la la-building"));
        nav.addItem(new AppNavItem("Users", UsersView.class, "la la-users"));
        nav.addItem(new AppNavItem("Discount", DiscountView.class, "la la-percent"));
        nav.addItem(new AppNavItem("Beneficiary", BeneficiaryView.class, "la la-sitemap"));
        nav.addItem(new AppNavItem("Dictionaries", DictionaryView.class, "la la-cog"));
        nav.addItem(new AppNavItem("Formula", FormulaView.class, "la la-calculator"));
        nav.addItem(new AppNavItem("Service", ServiceView.class, "la la-coins"));
        nav.addItem(new AppNavItem("Invoice", InvoiceView.class, "la la-clipboard-list"));
        nav.addItem(new AppNavItem("Consumption point", ConsumptionPointView.class, "la la-lightbulb"));

        return nav;
    }

    private Footer createFooter() {
        Button button = new Button("Dark theme");
        button.setWidth("100%");
        button.addClickListener(e -> {
            ThemeList themeList = UI.getCurrent().getElement().getThemeList(); // (1)

            if (themeList.contains(Lumo.DARK)) { // (2)
                themeList.remove(Lumo.DARK);
                button.setText("Dark theme");
            } else {
                themeList.add(Lumo.DARK);
                button.setText("Light theme");
            }
        });
        Footer layout = new Footer(button);
        layout.addClassNames("app-nav-footer");

        return layout;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        viewTitle.setText(getCurrentPageTitle());
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }
}
