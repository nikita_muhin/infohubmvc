package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.FormulaDialog;
import com.infohubmvc.application.components.grids.FormulaGrid;
import com.infohubmvc.application.data.entity.CalculationFormula;
import com.infohubmvc.application.service.CalculationFormulaService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;

@PermitAll
@PageTitle("Formula")
@Route(value = "formula", layout = MainLayout.class)
@RouteAlias(value = "formula", layout = MainLayout.class)
public class FormulaView extends AbstractView{

    private Grid<CalculationFormula> calculationFormulaGrid;
    private ListDataProvider<CalculationFormula> dataProvider;
    private CalculationFormulaService calculationFormulaService;
    private CalculationFormula selectCalculationFormula;

    public FormulaView() {
        calculationFormulaService = WebApplicationUtils.get(CalculationFormulaService.class);
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), calculationFormulaGrid);
    }

    @Override
    protected void configureGrid() {
        calculationFormulaGrid = new FormulaGrid();
        dataProvider = new ListDataProvider<>(calculationFormulaService.findAll());
        calculationFormulaGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectCalculationFormula = dataProvider.getItems().stream().findFirst().get();
        }
        calculationFormulaGrid.select(selectCalculationFormula);
        calculationFormulaGrid.addSelectionListener(selection -> {
            Optional<CalculationFormula> optionalCalculationFormula = selection.getFirstSelectedItem();
            optionalCalculationFormula.ifPresent(calculationFormula -> selectCalculationFormula = calculationFormula);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openFormulaDialog("Create formula", new CalculationFormula(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openFormulaDialog("Edit formula", selectCalculationFormula, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete house \"%s\"?", selectCalculationFormula.getFormula()),
                    "Are you sure you want to delete this formula permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                calculationFormulaService.delete(selectCalculationFormula);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openFormulaDialog("Details house", selectCalculationFormula, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsCode = caseInsensitiveContains(s.getCode(), event.getValue());
            boolean containsDescription = caseInsensitiveContains(s.getDescription(), event.getValue());
            boolean containsType = caseInsensitiveContains(s.getCalculationType().getDescription(), event.getValue());
            boolean containsFormula = caseInsensitiveContains(s.getFormula(), event.getValue());
            boolean containsUser = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue());
            return containsCode || containsDescription || containsType || containsFormula || containsUser;
        });
    }

    private void openFormulaDialog(String header, CalculationFormula calculationFormula, boolean isEditable, boolean isCreate) {
        FormulaDialog formulaDialog = new FormulaDialog(header, calculationFormula, isEditable, isCreate);
        formulaDialog.open();
        formulaDialog.getSaveButton().setEnabled(false);
        formulaDialog.getSaveButton().addClickListener(e -> {
            if(isCreate) {
                refreshGrid();
            } else {
                dataProvider.refreshAll();
            }
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(calculationFormulaService.findAll());
        calculationFormulaGrid.setDataProvider(dataProvider);
    }
}
