package com.infohubmvc.application.views;

import com.infohubmvc.application.data.entity.Dictionary;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.tabs.content.TypeTabContent;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;


@PermitAll
@PageTitle("Type")
@Route(value = "type", layout = MainLayout.class)
@RouteAlias(value = "type", layout = MainLayout.class)
public class DictionaryView extends VerticalLayout {

    private Grid<Dictionary> dictionaryGrid;
    private ListDataProvider<Dictionary> dataProvider;
    private TextField searchTextField;
    private Tabs tabs;
    private final Tab allTypeTab = new Tab("Dictionary");
    private final Tab selectTypeTab = new Tab("Details");
    private VerticalLayout content;
    private DictionaryService dictionaryService;
    private Dictionary selectDictionary;

    public DictionaryView() {
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        setWidthFull();
        setHeightFull();
        configureGrid();
        configureSearchTextField();
        configureTabs();
        add(tabs, content);
    }

    protected void configureGrid() {
        dictionaryGrid = new Grid<>();
        dictionaryGrid.addColumn(Dictionary::getCode).setHeader("Table");
        dictionaryGrid.addColumn(Dictionary::getDescription).setHeader("Description");
        dictionaryGrid.addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        dictionaryGrid.addThemeVariants(GridVariant.LUMO_COMPACT);
        dataProvider = new ListDataProvider<>(dictionaryService.findAll());
        dictionaryGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectDictionary = dataProvider.getItems().stream().findFirst().get();
        }
        dictionaryGrid.select(selectDictionary);
        dictionaryGrid.addSelectionListener(selection -> {
            Optional<Dictionary> optionalDictionary = selection.getFirstSelectedItem();
            optionalDictionary.ifPresent(dictionary -> selectDictionary = dictionary);
        });
    }

    private void configureSearchTextField() {
        searchTextField = new TextField();
        searchTextField.setPlaceholder("Search");
        searchTextField.setWidth("30%");
        searchTextField.setClearButtonVisible(true);
        searchTextField.setValueChangeMode(ValueChangeMode.LAZY);
        searchTextField.addValueChangeListener(this::onNameFilterTextChange);
    }

    private void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsType = caseInsensitiveContains(s.getCode(), event.getValue());
            boolean containsDescription = caseInsensitiveContains(s.getDescription(), event.getValue());
            return containsType || containsDescription;
        });
    }

    public Boolean caseInsensitiveContains(String where, String what) {
        if (where == null) return false;
        return where.toLowerCase().contains(what.toLowerCase());
    }

    private void configureTabs() {
        tabs = new Tabs();
        tabs.add(allTypeTab);
        tabs.add(selectTypeTab);
        tabs.setWidthFull();
        tabs.addSelectedChangeListener(event ->
                setContent(event.getSelectedTab())
        );
        content = new VerticalLayout();
        content.setSpacing(false);
        setContent(tabs.getSelectedTab());
    }

    private void setContent(Tab tab) {
        content.removeAll();
        content.setHeight("90%");
        content.getStyle().set("padding-top", "0%");
        content.setWidthFull();
        if (tab.equals(allTypeTab)) {
            content.add(searchTextField, dictionaryGrid);
        } else {
            content.add(new TypeTabContent(selectDictionary));
        }
    }
}
