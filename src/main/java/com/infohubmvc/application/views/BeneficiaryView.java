package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.BeneficiaryDialog;
import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.grids.BeneficiaryGrid;
import com.infohubmvc.application.data.entity.Beneficiary;
import com.infohubmvc.application.service.BeneficiaryService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;

@PermitAll
@PageTitle("Beneficiary")
@Route(value = "beneficiary", layout = MainLayout.class)
@RouteAlias(value = "beneficiary", layout = MainLayout.class)
public class BeneficiaryView extends AbstractView{

    private Grid<Beneficiary> beneficiaryGrid;
    private ListDataProvider<Beneficiary> dataProvider;
    private BeneficiaryService beneficiaryService;
    private Beneficiary selectBeneficiary;

    public BeneficiaryView() {
        beneficiaryService = WebApplicationUtils.get(BeneficiaryService.class);
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), beneficiaryGrid);
    }

    @Override
    protected void configureGrid() {
        beneficiaryGrid = new BeneficiaryGrid();
        dataProvider = new ListDataProvider<>(beneficiaryService.findAll());
        beneficiaryGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectBeneficiary = dataProvider.getItems().stream().findFirst().get();
        }
        beneficiaryGrid.select(selectBeneficiary);
        beneficiaryGrid.addSelectionListener(selection -> {
            Optional<Beneficiary> optionalBeneficiary = selection.getFirstSelectedItem();
            optionalBeneficiary.ifPresent(beneficiary -> selectBeneficiary = beneficiary);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openDiscountDialog("Create beneficiary", new Beneficiary(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openDiscountDialog("Edit beneficiary", selectBeneficiary, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete beneficiary \"%s\"?", selectBeneficiary.getServiceName()),
                    "Are you sure you want to delete this beneficiary permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                beneficiaryService.delete(selectBeneficiary);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openDiscountDialog("Details beneficiary", selectBeneficiary, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsCondominium = caseInsensitiveContains(s.getCondominium().getName(), event.getValue());
            boolean containsHouse = caseInsensitiveContains(s.getHouse().getName(), event.getValue());
            boolean containsService = caseInsensitiveContains(s.getService().getName(), event.getValue());
            boolean containsServiceName = caseInsensitiveContains(s.getServiceName(), event.getValue());
            boolean containsUser = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue());
            return containsCondominium || containsHouse || containsService || containsServiceName || containsUser;
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(beneficiaryService.findAll());
        beneficiaryGrid.setDataProvider(dataProvider);
    }

    private void openDiscountDialog(String header, Beneficiary beneficiary, boolean isEditable, boolean isCreate) {
        BeneficiaryDialog beneficiaryDialog = new BeneficiaryDialog(header, beneficiary, isEditable, isCreate);
        beneficiaryDialog.open();
        beneficiaryDialog.getSaveButton().setEnabled(false);
        beneficiaryDialog.getSaveButton().addClickListener(e -> {
            if(isCreate) {
                refreshGrid();
            } else {
                dataProvider.refreshAll();
            }
        });
    }
}
