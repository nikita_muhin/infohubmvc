package com.infohubmvc.application.views;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;

public abstract class AbstractView extends VerticalLayout {
    private TextField searchTextField;
    private Button createButton;
    private Button editButton;
    private Button deleteButton;
    private Button detailsButton;


    public HorizontalLayout topHorizontalLayout() {
        HorizontalLayout topHorizontalLayout = new HorizontalLayout();
        HorizontalLayout searchLayout = new HorizontalLayout();
        configureSearchTextField();
        searchLayout.add(searchTextField);
        topHorizontalLayout.setWidthFull();
        topHorizontalLayout.addAndExpand(searchLayout);
        topHorizontalLayout.add(createButton, editButton, detailsButton, deleteButton);
        topHorizontalLayout.setAlignSelf(FlexComponent.Alignment.END, createButton);
        return topHorizontalLayout;
    }

    private void configureSearchTextField() {
        searchTextField = new TextField();
        searchTextField.setPlaceholder("Search");
        searchTextField.setWidth("30%");
        searchTextField.setClearButtonVisible(true);
        searchTextField.setValueChangeMode(ValueChangeMode.LAZY);
        searchTextField.addValueChangeListener(this::onNameFilterTextChange);

    }

    public Boolean caseInsensitiveContains(String where, String what) {
        if (where == null) return false;
        return where.toLowerCase().contains(what.toLowerCase());
    }

    protected abstract void configureGrid();

    protected abstract void createButton();

    protected abstract void editButton();

    protected abstract void deleteButton();

    protected abstract void detailsButton();

    protected abstract void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event);

    public void setCreateButton(Button createButton) {
        this.createButton = createButton;
    }

    public void setEditButton(Button editButton) {
        this.editButton = editButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    public void setDetailsButton(Button detailsButton) {
        this.detailsButton = detailsButton;
    }
}
