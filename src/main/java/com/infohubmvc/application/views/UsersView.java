package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.UsersDialog;
import com.infohubmvc.application.components.grids.UserGrid;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.tabs.content.CondominiumTabContent;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.Optional;

@RolesAllowed("ROLE_ADMIN")
@PermitAll
@PageTitle("Users")
@Route(value = "users", layout = MainLayout.class)
@RouteAlias(value = "users", layout = MainLayout.class)
@Transactional
public class UsersView extends AbstractView {

    private UserService userService;
    private Grid<User> gridUser;
    private ListDataProvider<User> dataProvider;
    private Tabs tabs;
    private final Tab usersTab = new Tab("Users");
    private final Tab condominiumTab = new Tab("Condominium");
    private User selectUser;
    private VerticalLayout content;

    public UsersView() {
        userService = WebApplicationUtils.get(UserService.class);
        this.setSizeFull();
        dataProvider = new ListDataProvider<>(userService.getAll());
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        configureTabs();
        add(tabs, content);
    }

    @Override
    protected void configureGrid() {
        selectUser = dataProvider.getItems().stream().findFirst().get();
        gridUser = new UserGrid();
        gridUser.setDataProvider(dataProvider);
        gridUser.select(selectUser);
        gridUser.addSelectionListener(selection -> {
            Optional<User> optionalPerson = selection.getFirstSelectedItem();
            if (optionalPerson.isPresent()) {
                selectUser = optionalPerson.get();
                System.out.printf("Selected person: %s%n", optionalPerson.get().getFirstName());
            }
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openCreateDialog("Create user", new User(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openCreateDialog("Edit user", selectUser, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete user \"%s\"?", selectUser.getUsername()),
                    "Are you sure you want to delete this user permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                userService.deleteUser(selectUser);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openCreateDialog("Edit user", selectUser, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsName = caseInsensitiveContains(s.getFirstName(), event.getValue());
            boolean containsSurname = caseInsensitiveContains(s.getLastName(), event.getValue());
            boolean containsEmail = caseInsensitiveContains(s.getEmail(), event.getValue());
            boolean containsUsername = caseInsensitiveContains(s.getUsername(), event.getValue());
            boolean containsPhone = caseInsensitiveContains(s.getPhoneNumber(), event.getValue());
            return containsName || containsSurname || containsEmail || containsUsername || containsPhone;
        });
    }

    private void configureTabs() {
        tabs = new Tabs();
        tabs.add(usersTab);
        tabs.add(condominiumTab);

        tabs.setWidthFull();
        tabs.addSelectedChangeListener(event ->
                setContent(event.getSelectedTab())
        );
        content = new VerticalLayout();
        content.setSpacing(false);
        setContent(tabs.getSelectedTab());
    }

    private void setContent(Tab tab) {
        content.removeAll();
        content.setHeight("90%");
        content.getStyle().set("padding-top", "0%");
        content.setWidthFull();
        if (tab.equals(usersTab)) {
            content.add(topHorizontalLayout(), gridUser);
        } else {
            content.add(new CondominiumTabContent(selectUser));
        }
    }

    private void openCreateDialog(String headerString, User user, boolean isEditable, boolean isCreate) {
        UsersDialog userDialog;
        try {
            userDialog = new UsersDialog(headerString, user, isEditable, false);
            userDialog.getSaveButton().addClickListener(event -> {
                        if (isCreate) {
                            refreshGrid();
                        } else {
                            dataProvider.refreshAll();
                        }
                    }
            );
            userDialog.getSaveButton().setEnabled(false);
        } catch (ValidationException e) {
            throw new RuntimeException(e);
        }
        userDialog.open();
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(userService.getAll());
        gridUser.setDataProvider(dataProvider);
    }
}
