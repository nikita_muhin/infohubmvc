package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.ServiceDialog;
import com.infohubmvc.application.components.grids.ServiceGrid;
import com.infohubmvc.application.data.entity.Service;
import com.infohubmvc.application.service.ServiceService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;

@PermitAll
@PageTitle("Services")
@Route(value = "services", layout = MainLayout.class)
@RouteAlias(value = "services", layout = MainLayout.class)
public class ServiceView extends AbstractView {

    private Grid<Service> serviceGrid;
    private ListDataProvider<Service> dataProvider;
    private ServiceService serviceService;
    private Service selectService;

    public ServiceView() {
        serviceService = WebApplicationUtils.get(ServiceService.class);
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), serviceGrid);
    }

    @Override
    protected void configureGrid() {
        serviceGrid = new ServiceGrid();
        dataProvider = new ListDataProvider<>(serviceService.findAll());
        serviceGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectService = dataProvider.getItems().stream().findFirst().get();
        }
        serviceGrid.select(selectService);
        serviceGrid.addSelectionListener(selection -> {
            Optional<Service> optionalService = selection.getFirstSelectedItem();
            optionalService.ifPresent(service -> selectService = service);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openServiceDialog("Create service", new Service(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openServiceDialog("Edit service", selectService, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete service \"%s\"?", selectService.getName()),
                    "Are you sure you want to delete this service permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                serviceService.delete(selectService);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openServiceDialog("Details service", selectService, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsName = caseInsensitiveContains(s.getName(), event.getValue());
            boolean containsContractNumber = caseInsensitiveContains(s.getContractNumber(), event.getValue());
            boolean containsType = caseInsensitiveContains(s.getServiceType().getDescription(), event.getValue());
            boolean containsUser = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue());
            return containsName || containsContractNumber || containsType || containsUser;
        });
    }

    private void openServiceDialog(String header, Service service, boolean isEditable, boolean isCreate) {
        ServiceDialog serviceDialog = new ServiceDialog(header, service, isEditable, isCreate);
        serviceDialog.open();
        serviceDialog.getSaveButton().setEnabled(false);
        serviceDialog.getSaveButton().addClickListener(e -> {
            if(isCreate) {
                refreshGrid();
            } else {
                dataProvider.refreshAll();
            }
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(serviceService.findAll());
        serviceGrid.setDataProvider(dataProvider);
    }

}
