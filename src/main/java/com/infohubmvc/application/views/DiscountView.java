package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.DiscountDialog;
import com.infohubmvc.application.components.grids.DiscountGrid;
import com.infohubmvc.application.data.entity.Discount;
import com.infohubmvc.application.service.DiscountService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;

@PermitAll
@PageTitle("Discount")
@Route(value = "discount", layout = MainLayout.class)
@RouteAlias(value = "discount", layout = MainLayout.class)
public class DiscountView extends AbstractView {

    private Grid<Discount> discountGrid;
    private ListDataProvider<Discount> dataProvider;
    private DiscountService discountService;
    private Discount selectDiscount;

    public DiscountView() {
        discountService = WebApplicationUtils.get(DiscountService.class);
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), discountGrid);
    }

    @Override
    protected void configureGrid() {
        discountGrid = new DiscountGrid();
        dataProvider = new ListDataProvider<>(discountService.findAll());
        discountGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectDiscount = dataProvider.getItems().stream().findFirst().get();
        }
        discountGrid.select(selectDiscount);
        discountGrid.addSelectionListener(selection -> {
            Optional<Discount> optionalDiscount = selection.getFirstSelectedItem();
            optionalDiscount.ifPresent(discount -> selectDiscount = discount);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openDiscountDialog("Create discount", new Discount(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openDiscountDialog("Edit discount", selectDiscount, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete discoun \"%s\"?", selectDiscount.getDescription()),
                    "Are you sure you want to delete this discount permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                discountService.deleteDiscount(selectDiscount);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openDiscountDialog("Details discount", selectDiscount, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsType = caseInsensitiveContains(s.getDiscountType().getDescription(), event.getValue());
            boolean containsDescription = caseInsensitiveContains(s.getDescription(), event.getValue());
            boolean containsUnit = caseInsensitiveContains(s.getDiscountUnit().getDescription(), event.getValue());
            boolean containsUser = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue());
            return containsType || containsDescription || containsUnit || containsUser;
        });
    }

    private void openDiscountDialog(String header, Discount discount, boolean isEditable, boolean isCreate) {
        DiscountDialog discountDialog = new DiscountDialog(header, discount, isEditable, isCreate);
        discountDialog.open();
        discountDialog.getSaveButton().setEnabled(false);
        discountDialog.getSaveButton().addClickListener(e -> {
           if(isCreate) {
               refreshGrid();
           } else {
               dataProvider.refreshAll();
           }
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(discountService.findAll());
        discountGrid.setDataProvider(dataProvider);
    }
}
