package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.UsersDialog;
import com.infohubmvc.application.data.entity.User;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;


@PageTitle("Login")
@Route("login-overlay-header")
public class LoginView extends Div implements BeforeEnterObserver {

    private LoginOverlay loginOverlay;

    public LoginView() {
        LoginI18n i18n = LoginI18n.createDefault();
        LoginI18n.Form i18nForm = i18n.getForm();
        i18nForm.setForgotPassword("Registration");
        i18n.setForm(i18nForm);
        loginOverlay = new LoginOverlay();
        loginOverlay.setTitle("InfoHub");
        loginOverlay.setDescription("Example description");
        loginOverlay.setAction("login");
        loginOverlay.setI18n(i18n);
        add(loginOverlay);
        loginOverlay.setOpened(true);
        loginOverlay.getElement().setAttribute("no-autofocus", "");
        loginOverlay.addForgotPasswordListener(e -> openCreateUserDialog());
    }


    @Override
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (beforeEnterEvent.getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error")) {
            loginOverlay.setError(true);
        }
    }


    private void openCreateUserDialog() {
        UsersDialog userDialog;
        try {
            userDialog = new UsersDialog("New user", new User(), true, true);
            userDialog.getSaveButton().setEnabled(false);
        } catch (ValidationException e) {
            throw new RuntimeException(e);
        }
        userDialog.open();
    }

}

