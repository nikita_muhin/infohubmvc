package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.HouseDialog;
import com.infohubmvc.application.components.grids.HousesGrid;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.service.HouseService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.tabs.content.PropertyTabContent;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.security.PermitAll;
import java.util.Optional;


@PermitAll
@PageTitle("Houses")
@Route(value = "house", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
@Transactional
public class HouseView extends AbstractView {

    private House selectHouse;
    private ListDataProvider<House> dataProvider;
    private HouseService houseService;
    private Grid<House> houseGrid;
    private Tabs tabs;
    private final Tab usersTab = new Tab("House");
    private final Tab condominiumTab = new Tab("Property");
    private VerticalLayout content;

    public HouseView() {
        houseService = WebApplicationUtils.get(HouseService.class);
        dataProvider = new ListDataProvider<>(houseService.getAll());
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        configureTabs();
        add(tabs, content);
    }

    @Override
    protected void configureGrid() {
        houseGrid = new HousesGrid();
        houseGrid.setDataProvider(dataProvider);
        houseGrid.setHeightFull();
        if (dataProvider.getItems().size() != 0) {
            selectHouse = dataProvider.getItems().stream().findFirst().get();
        }
        houseGrid.select(selectHouse);
        houseGrid.addSelectionListener(selection -> {
            Optional<House> optionalCondominium = selection.getFirstSelectedItem();
            optionalCondominium.ifPresent(condominium -> selectHouse = condominium);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openHouseDialog("Create house", new House(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openHouseDialog("Edit house", selectHouse, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete house \"%s\"?", selectHouse.getName()),
                    "Are you sure you want to delete this user permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                houseService.deleteHouse(selectHouse);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openHouseDialog("Details house", selectHouse, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsCondominium = caseInsensitiveContains(s.getCondominium().getName(), event.getValue());
            boolean containsName = caseInsensitiveContains(s.getName(), event.getValue());
            boolean containsHouseAddress = caseInsensitiveContains(s.getHouseAddress(), event.getValue());
            return containsCondominium || containsName || containsHouseAddress;
        });
    }

    private void configureTabs() {
        tabs = new Tabs();
        tabs.add(usersTab);
        tabs.add(condominiumTab);

        tabs.setWidthFull();
        tabs.addSelectedChangeListener(event ->
                setContent(event.getSelectedTab())
        );
        content = new VerticalLayout();
        content.setSpacing(false);
        setContent(tabs.getSelectedTab());
    }

    private void setContent(Tab tab) {
        content.removeAll();
        content.setHeight("90%");
        content.getStyle().set("padding-top", "0%");
        content.setWidthFull();
        if (tab.equals(usersTab)) {
            content.add(topHorizontalLayout(), houseGrid);
        } else {
            content.add(new PropertyTabContent(selectHouse));
        }
    }

    private void openHouseDialog(String headerString, House house, boolean isEditable, boolean isCreate) {
        HouseDialog newHouseDialog;
        try {
            newHouseDialog = new HouseDialog(headerString, house, isEditable);
            newHouseDialog.getSaveButton().addClickListener(event -> {
                if (isCreate) {
                    refreshGrid();
                } else {
                    dataProvider.refreshAll();
                }
            });
            newHouseDialog.getSaveButton().setEnabled(false);
        } catch (ValidationException e) {
            throw new RuntimeException(e);
        }
        newHouseDialog.open();
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(houseService.getAll());
        houseGrid.setDataProvider(dataProvider);
    }

}
