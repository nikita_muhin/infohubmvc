package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.ConsumptionPointDialog;
import com.infohubmvc.application.components.grids.ConsumptionPointGrid;
import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.infohubmvc.application.service.ConsumptionPointService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;


@PermitAll
@PageTitle("ConsumptionPoint")
@Route(value = "consumptionPoint", layout = MainLayout.class)
@RouteAlias(value = "consumptionPoint", layout = MainLayout.class)
public class ConsumptionPointView extends AbstractView {

    private Grid<ConsumptionPoint> consumptionPointGrid;
    private ListDataProvider<ConsumptionPoint> dataProvider;
    private ConsumptionPointService consumptionPointService;
    private ConsumptionPoint selectConsumptionPoint;

    private ConsumptionPointView() {
        consumptionPointService = WebApplicationUtils.get(ConsumptionPointService.class);
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), consumptionPointGrid);
    }

    @Override
    protected void configureGrid() {
        consumptionPointGrid = new ConsumptionPointGrid();
        dataProvider = new ListDataProvider<>(consumptionPointService.findAll());
        consumptionPointGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectConsumptionPoint = dataProvider.getItems().stream().findFirst().get();
        }
        consumptionPointGrid.select(selectConsumptionPoint);
        consumptionPointGrid.addSelectionListener(selection -> {
            Optional<ConsumptionPoint> optionalConsumptionPoint = selection.getFirstSelectedItem();
            optionalConsumptionPoint.ifPresent(consumptionPoint -> selectConsumptionPoint = consumptionPoint);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openConsumptionPointView("Create consumption point", new ConsumptionPoint(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openConsumptionPointView("Edit consumption point", selectConsumptionPoint, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }


    // delete not working
    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete beneficiary \"%s\"?", selectConsumptionPoint.getCounterNumber()),
                    "Are you sure you want to delete this beneficiary permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                consumptionPointService.delete(selectConsumptionPoint);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openConsumptionPointView("Details consumption point", selectConsumptionPoint, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }


    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsCondominium = caseInsensitiveContains(s.getCondominium().getName(), event.getValue());
            boolean containsHouse = caseInsensitiveContains(s.getHouse().getName(), event.getValue());
            boolean containsServiceType = caseInsensitiveContains(s.getServiceType().getDescription(), event.getValue());
            boolean containsCalculationType = caseInsensitiveContains(s.getCalculationType().getDescription(), event.getValue());
            boolean containsUser = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue());
            boolean containsCounterNumber = caseInsensitiveContains(s.getCounterNumber(), event.getValue());
            return containsCondominium || containsHouse || containsServiceType || containsCalculationType || containsUser || containsCounterNumber;
        });
    }

    private void openConsumptionPointView(String header, ConsumptionPoint consumptionPoint, boolean isEditable, boolean isCreate) {
        ConsumptionPointDialog consumptionPointDialog = new ConsumptionPointDialog(header, consumptionPoint, isEditable, isCreate);
        consumptionPointDialog.open();
        consumptionPointDialog.getSaveButton().setEnabled(false);
        consumptionPointDialog.getSaveButton().addClickListener(e -> {
            if (isCreate) {
                refreshGrid();
            } else {
                dataProvider.refreshAll();
            }
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(consumptionPointService.findAll());
        consumptionPointGrid.setDataProvider(dataProvider);
    }

}
