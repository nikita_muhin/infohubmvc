package com.infohubmvc.application.views;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.InvoiceDialog;
import com.infohubmvc.application.components.grids.InvoiceGrid;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.service.InvoiceService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.tabs.content.InvoiceElementTabContent;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

import javax.annotation.security.PermitAll;
import java.util.Optional;

@PermitAll
@PageTitle("Invoice")
@Route(value = "invoice", layout = MainLayout.class)
@RouteAlias(value = "invoice", layout = MainLayout.class)
public class InvoiceView extends AbstractView {

    private Grid<Invoice> invoiceGrid;
    private ListDataProvider<Invoice> dataProvider;
    private InvoiceService invoiceService;
    private Invoice selectInvoice;
    private Tabs tabs;
    private final Tab invoiceTab = new Tab("Invoice");
    private final Tab invoiceElementTab = new Tab("Invoice element");
    private VerticalLayout content;

    public InvoiceView() {
        invoiceService = WebApplicationUtils.get(InvoiceService.class);
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        configureTabs();
        add(tabs, content);
    }

    private void configureTabs() {
        tabs = new Tabs();
        tabs.add(invoiceTab);
        tabs.add(invoiceElementTab);
        tabs.setWidthFull();
        tabs.addSelectedChangeListener(event ->
                setContent(event.getSelectedTab())
        );
        content = new VerticalLayout();
        content.setSpacing(false);
        setContent(tabs.getSelectedTab());
    }

    private void setContent(Tab tab) {
        content.removeAll();
        content.setHeight("90%");
        content.getStyle().set("padding-top", "0%");
        content.setWidthFull();
        if (tab.equals(invoiceTab)) {
            content.add(topHorizontalLayout(), invoiceGrid);
        } else {
            content.add(new InvoiceElementTabContent(selectInvoice));
        }
    }

    @Override
    protected void configureGrid() {
        invoiceGrid = new InvoiceGrid();
        dataProvider = new ListDataProvider<>(invoiceService.findAll());
        invoiceGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectInvoice = dataProvider.getItems().stream().findFirst().get();
        }
        invoiceGrid.select(selectInvoice);
        invoiceGrid.addSelectionListener(selection -> {
            Optional<Invoice> optionalInvoice = selection.getFirstSelectedItem();
            optionalInvoice.ifPresent(invoice -> selectInvoice = invoice);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openInvoiceDialog("Create invoice", new Invoice(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openInvoiceDialog("Edit invoice", selectInvoice, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete invoice \"%s\"?", selectInvoice.getCalculatedAmount()),
                    "Are you sure you want to delete this invoice permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                invoiceService.delete(selectInvoice);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openInvoiceDialog("Details invoice", selectInvoice, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsCondominium = caseInsensitiveContains(s.getCondominium().getName(), event.getValue());
            boolean containsBeneficiary = caseInsensitiveContains(s.getBeneficiary().getCalculationCode(), event.getValue());
            boolean containsUser = caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue());
            return containsCondominium || containsBeneficiary || containsUser;
        });
    }

    private void openInvoiceDialog(String header, Invoice invoice, boolean isEditable, boolean isCreate) {
        InvoiceDialog invoiceDialog = new InvoiceDialog(header, invoice, isEditable, isCreate);
        invoiceDialog.open();
        invoiceDialog.getSaveButton().setEnabled(false);
        invoiceDialog.getSaveButton().addClickListener(e -> {
            if(isCreate) {
                refreshGrid();
            } else {
                dataProvider.refreshAll();
            }
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(invoiceService.findAll());
        invoiceGrid.setDataProvider(dataProvider);
    }
}
