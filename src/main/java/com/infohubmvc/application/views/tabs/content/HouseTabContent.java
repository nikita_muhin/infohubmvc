package com.infohubmvc.application.views.tabs.content;

import com.infohubmvc.application.components.dialogs.SelectHouseDialog;
import com.infohubmvc.application.components.grids.HousesGrid;
import com.infohubmvc.application.components.grids.PropertyGrid;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.Property;
import com.infohubmvc.application.service.HouseService;
import com.infohubmvc.application.service.PropertyService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.provider.ListDataProvider;

import java.util.Optional;

public class HouseTabContent extends VerticalLayout {

    private Grid<House> houseGrid;
    private Grid<Property> propertyGrid;
    private House selectHouse;
    private Condominium selectCondominium;
    private PropertyService propertyService;
    private HouseService houseService;

    public HouseTabContent(Condominium selectCondominium) {
        propertyService = WebApplicationUtils.get(PropertyService.class);
        houseService = WebApplicationUtils.get(HouseService.class);
        this.selectCondominium = selectCondominium;
        setWidthFull();
        setHeightFull();
        getStyle().set("letter-spacing", "0px");
        configureHouseGrid();
        configurePropertyGrid();
        add(topInHouseTab(), houseGrid, headerPropertyGrid(), propertyGrid);
    }

    private FormLayout topInHouseTab() {
        Label label = new Label("Condominium: ");

        TextField nameTextField = new TextField();
        nameTextField.setValue(selectCondominium.getName());
        nameTextField.setReadOnly(true);
        nameTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        TextField registrationNumberTextField = new TextField();
        registrationNumberTextField.setValue(selectCondominium.getRegistrationNumber());
        registrationNumberTextField.setReadOnly(true);
        registrationNumberTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        TextField statusTextFiled = new TextField();
        statusTextFiled.setValue(selectCondominium.getStatus().name());
        statusTextFiled.setReadOnly(true);
        statusTextFiled.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        Button addExistingButton = addExistingPropertiesToHouse();

        FormLayout formLayout = new FormLayout();
        formLayout.setWidthFull();
        formLayout.add(label, nameTextField, registrationNumberTextField, statusTextFiled, addExistingButton);
        formLayout.setColspan(label, 2);
        formLayout.setColspan(nameTextField, 3);
        formLayout.setColspan(registrationNumberTextField, 3);
        formLayout.setColspan(statusTextFiled, 3);
        formLayout.setColspan(addExistingButton, 3);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("670px", 14)
        );
        return formLayout;
    }

    private Button addExistingPropertiesToHouse() {
        Button button = new Button("Add registered houses");
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
        button.addClickListener(e -> {
            SelectHouseDialog selectHouseDialog = new SelectHouseDialog(selectCondominium, houseService.findAllByCondominium(selectCondominium));
            selectHouseDialog.open();
            selectHouseDialog.getSaveButton().addClickListener(event -> {
                refreshHouseGrid();
                propertyGrid.setDataProvider(new ListDataProvider<>(propertyService.findAllPropertiesByHouseId(selectHouse)));
            });
        });
        return button;
    }

    private void refreshHouseGrid() {
        ListDataProvider<House> houseListDataProvider = new ListDataProvider<>(houseService.findAllByCondominium(selectCondominium));
        houseGrid.setDataProvider(houseListDataProvider);
        if(houseListDataProvider.getItems().size() != 0) {
            selectHouse = houseListDataProvider.getItems().stream().findFirst().get();
        }
        houseGrid.select(selectHouse);
    }

    private void configureHouseGrid() {
        houseGrid = new HousesGrid();
        houseGrid.setHeight("50%");
        houseGrid.getStyle().set("margin-bottom", "0px");
        selectHouse = null;
        refreshHouseGrid();
        houseGrid.addSelectionListener(selection -> {
            Optional<House> optionalHouse = selection.getFirstSelectedItem();
            optionalHouse.ifPresent(house -> selectHouse = house);
            propertyGrid.setDataProvider(new ListDataProvider<>(propertyService.findAllPropertiesByHouseId(selectHouse)));
        });
    }

    private Label headerPropertyGrid() {
        Label label = new Label("List of properties of the selected house");
        label.getStyle().set("margin-bottom", "0px");
        return label;
    }

    private void configurePropertyGrid() {
        propertyGrid = new PropertyGrid();
        propertyGrid.setHeight("49%");
        propertyGrid.getStyle().set("margin-top", "0px");
        propertyGrid.setDataProvider(new ListDataProvider<>(propertyService.findAllPropertiesByHouseId(selectHouse)));
    }

}
