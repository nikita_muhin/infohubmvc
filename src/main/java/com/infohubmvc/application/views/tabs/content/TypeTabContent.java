package com.infohubmvc.application.views.tabs.content;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.TypeDialog;
import com.infohubmvc.application.controller.dto.DictionaryDTO;
import com.infohubmvc.application.controller.mapper.DictionaryMapper;
import com.infohubmvc.application.data.entity.Dictionary;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.AbstractView;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;

import java.util.Optional;

public class TypeTabContent extends AbstractView {

    private Grid<DictionaryDTO> dictionaryDTOGrid;
    private ListDataProvider<DictionaryDTO> dataProvider;
    private DictionaryService dictionaryService;
    private DictionaryDTO selectDictionaryDTO;
    private Dictionary dictionary;

    public TypeTabContent(Dictionary dictionary) {
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        this.dictionary = dictionary;
        setSpacing(false);
        getStyle().set("padding", "0%");
        setHeightFull();
        configureDataProvider();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        configureDataProvider();
        add(topHorizontalLayout(), dictionaryDTOGrid);
    }

    @Override
    protected void configureGrid() {
        dictionaryDTOGrid = new Grid<>();
        dictionaryDTOGrid.addColumn(DictionaryDTO::getCode).setHeader("Code");
        dictionaryDTOGrid.addColumn(DictionaryDTO::getDescription).setHeader("Description");
        dictionaryDTOGrid.addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        dictionaryDTOGrid.addThemeVariants(GridVariant.LUMO_COMPACT);
        dictionaryDTOGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectDictionaryDTO = dataProvider.getItems().stream().findFirst().get();
        }
        dictionaryDTOGrid.select(selectDictionaryDTO);
        dictionaryDTOGrid.addSelectionListener(selection -> {
            Optional<DictionaryDTO> optionalDictionaryDTO = selection.getFirstSelectedItem();
            optionalDictionaryDTO.ifPresent(dictionaryDTO -> selectDictionaryDTO = dictionaryDTO);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openTypeDialog("Create beneficiary", new DictionaryDTO(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openTypeDialog("Edit beneficiary", selectDictionaryDTO, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete beneficiary \"%s\"?", selectDictionaryDTO.getDescription()),
                    "Are you sure you want to delete this beneficiary permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                dictionaryService.delete(selectDictionaryDTO, dictionary.getCode());
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openTypeDialog("Details beneficiary", selectDictionaryDTO, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsCode = caseInsensitiveContains(s.getCode(), event.getValue());
            boolean containsDescription = caseInsensitiveContains(s.getDescription(), event.getValue());
            return containsCode || containsDescription;
        });
    }

    private void openTypeDialog(String header, DictionaryDTO dictionaryDTO, boolean isEditable, boolean isCreate) {
        TypeDialog typeDialog = new TypeDialog(header, dictionaryDTO, isEditable, isCreate, dictionary.getCode());
        typeDialog.open();
        typeDialog.getSaveButton().addClickListener(e -> {
            refreshGrid();
        });
    }

    private void configureDataProvider() {
        switch (dictionary.getCode()) {
            case ("CALCULATION_TYPE"):
                dataProvider = new ListDataProvider<>(DictionaryMapper.toCalculationTypeDTOS(dictionaryService.getAllCalculationTypes()));
                break;
            case ("DISCOUNT_TYPE"):
                dataProvider = new ListDataProvider<>(DictionaryMapper.toDiscountTypeDTOS(dictionaryService.getAllDiscountTypes()));
                break;
            case ("PROPERTY_TYPE"):
                dataProvider = new ListDataProvider<>(DictionaryMapper.toPropertyTypeDTOS(dictionaryService.getAllPropertyTypes()));
                break;
            case ("SERVICE_TYPE"):
                dataProvider = new ListDataProvider<>(DictionaryMapper.toServiceTypeDTOS(dictionaryService.getAllServiceTypes()));
                break;
            case ("STATUS_CODE"):
                dataProvider = new ListDataProvider<>(DictionaryMapper.toStatusCodeDTOS(dictionaryService.getAllStatusCodes()));
                break;
            case ("UNITS"):
                dataProvider = new ListDataProvider<>(DictionaryMapper.toUnitDTOS(dictionaryService.getAllUnits()));
                break;
        }
    }

    private void refreshGrid() {
        configureDataProvider();
        dictionaryDTOGrid.setDataProvider(dataProvider);
    }
}
