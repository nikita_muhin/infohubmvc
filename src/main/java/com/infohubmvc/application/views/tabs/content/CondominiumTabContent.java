package com.infohubmvc.application.views.tabs.content;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.SelectCondominiumsDialog;
import com.infohubmvc.application.components.grids.CondominiumsGrid;
import com.infohubmvc.application.components.grids.HousesGrid;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.data.entity.UserRight;
import com.infohubmvc.application.service.CondominiumService;
import com.infohubmvc.application.service.UserRightServer;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.provider.ListDataProvider;

import javax.swing.text.TabExpander;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CondominiumTabContent extends VerticalLayout {

    private Condominium selectCondominium;
    private Grid<Condominium> condominiumGrid;
    private Grid<House> houseGrid;
    private UserRightServer userRightServer;
    private CondominiumService condominiumService;
    private User selectUser;

    public CondominiumTabContent(User selectUser) {
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        userRightServer = WebApplicationUtils.get(UserRightServer.class);
        this.selectUser = selectUser;
        setWidthFull();
        setHeightFull();
        configureCondominiumGrid();
        add(topInCondominiumTab(), condominiumGrid, headerHouseGrid(), houseGrid);
    }

    private void configureCondominiumGrid() {
        ListDataProvider<Condominium> condominiumDataProvider = new ListDataProvider<>(condominiumService.findCondominiumByUserId(selectUser.getId()));
        selectCondominium = null;
        condominiumGrid = new CondominiumsGrid();
        condominiumGrid.setDataProvider(condominiumDataProvider);
        condominiumGrid.setHeight("50%");
        if (condominiumDataProvider.getItems().size() != 0) {
            selectCondominium = condominiumDataProvider.getItems().stream().findFirst().get();
            condominiumGrid.select(selectCondominium);
        }
        configureHouseGrid();
        condominiumGrid.addSelectionListener(selection -> {
            Optional<Condominium> optionalCondominium = selection.getFirstSelectedItem();
            optionalCondominium.ifPresent(condominium -> selectCondominium = condominium);
            houseGrid.setDataProvider(new ListDataProvider<>(getListHouseForUser()));
        });
    }

    private void configureHouseGrid() {
        ListDataProvider<House> houseDataProvider = new ListDataProvider<>(getListHouseForUser());
        houseGrid = new HousesGrid();
        houseGrid.addComponentColumn(this::deleteIcon).setAutoWidth(true);
        houseGrid.setDataProvider(houseDataProvider);
        houseGrid.setHeight("49%");
        houseGrid.getStyle().set("margin-bottom", "0px");
    }

    private FormLayout topInCondominiumTab() {
        Label label = new Label("User: ");

        TextField usernameTextField = new TextField();
        usernameTextField.setValue(selectUser.getUsername());
        usernameTextField.setReadOnly(true);
        usernameTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        TextField fullNameTextField = new TextField();
        fullNameTextField.setValue(selectUser.getFirstName() + " " + selectUser.getLastName());
        fullNameTextField.setReadOnly(true);
        fullNameTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        TextField statusTextFiled = new TextField();
        statusTextFiled.setValue(selectUser.getStatus().name());
        statusTextFiled.setReadOnly(true);
        statusTextFiled.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        Button addExistingButton = addExistingHouseToCondominium();

        FormLayout formLayout = new FormLayout();
        formLayout.setWidthFull();
        formLayout.add(label, usernameTextField, fullNameTextField, statusTextFiled, addExistingButton);
        formLayout.setColspan(label, 1);
        formLayout.setColspan(usernameTextField, 4);
        formLayout.setColspan(fullNameTextField, 4);
        formLayout.setColspan(statusTextFiled, 4);
        formLayout.setColspan(addExistingButton, 3);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("670px", 16)
        );
        return formLayout;
    }

    private Button addExistingHouseToCondominium() {
        Button button = new Button("Add registered condominium");
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_SMALL);
        button.addClickListener(event -> {
            SelectCondominiumsDialog selectHouseDialog = new SelectCondominiumsDialog(condominiumService.findCondominiumByUserId(selectUser.getId()), selectUser);
            selectHouseDialog.open();
            selectHouseDialog.getSaveButton().addClickListener(e -> {
                ListDataProvider<Condominium> condominiumListDataProvider = new ListDataProvider<>(condominiumService.findCondominiumByUserId(selectUser.getId()));
                condominiumGrid.setDataProvider(condominiumListDataProvider);
                if (condominiumListDataProvider.getItems().size() != 0) {
                    selectCondominium = condominiumListDataProvider.getItems().stream().findFirst().get();
                }
                condominiumGrid.select(selectCondominium);
                houseGrid.setDataProvider(new ListDataProvider<>(getListHouseForUser()));
            });
        });
        return button;
    }

    private Icon deleteIcon(House house) {
        Icon icon = new Icon(VaadinIcon.TRASH);
        icon.setColor("red");
        icon.addClickListener(e ->{
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete house \"%s\"?", selectUser.getUsername()),
                    "Are you sure you want to delete this house permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                userRightServer.deleteUserRightsByHouse(selectCondominium, selectUser, house);
                houseGrid.setDataProvider(new ListDataProvider<>(getListHouseForUser()));
            });
            confirmDialog.open();
        });
        return icon;
    }

    private TextField headerHouseGrid() {
        TextField label = new TextField();
        label.setValue("List of houses of the selected condominium");
        label.setReadOnly(true);
        label.getStyle().set("margin-bottom", "0px");
        label.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        label.setWidth("400px");
        return label;
    }

    private List<House> getListHouseForUser() {
        List<UserRight> userRightList = userRightServer.findAllByUserAndCondominium(selectCondominium, selectUser);
        List<House> houseList = new ArrayList<>();
        userRightList.forEach(e -> houseList.add(e.getHouse()));
        return houseList;
    }
}
