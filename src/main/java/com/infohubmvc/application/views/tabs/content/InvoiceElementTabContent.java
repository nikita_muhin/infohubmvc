package com.infohubmvc.application.views.tabs.content;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.InvoiceDialog;
import com.infohubmvc.application.components.dialogs.InvoiceElementDialog;
import com.infohubmvc.application.components.grids.InvoiceElementGrid;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.InvoiceElement;
import com.infohubmvc.application.service.InvoiceElementService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.AbstractView;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;

import java.util.Optional;

public class InvoiceElementTabContent extends AbstractView {

    private Grid<InvoiceElement> invoiceElementGrid;
    private ListDataProvider<InvoiceElement> dataProvider;
    private InvoiceElementService invoiceElementService;
    private InvoiceElement selectInvoiceElement;
    private Invoice selectInvoice;

    public InvoiceElementTabContent(Invoice invoice) {
        this.selectInvoice = invoice;
        invoiceElementService = WebApplicationUtils.get(InvoiceElementService.class);
        setSpacing(false);
        getStyle().set("padding", "0%");
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), invoiceElementGrid);
    }

    @Override
    protected void configureGrid() {
        invoiceElementGrid = new InvoiceElementGrid();
        dataProvider = new ListDataProvider<>(invoiceElementService.findAllByInvoice(selectInvoice));
        invoiceElementGrid.setDataProvider(dataProvider);
        if (dataProvider.getItems().size() != 0) {
            selectInvoiceElement = dataProvider.getItems().stream().findFirst().get();
        }
        invoiceElementGrid.select(selectInvoiceElement);
        invoiceElementGrid.addSelectionListener(selection -> {
            Optional<InvoiceElement> optionalInvoiceElement = selection.getFirstSelectedItem();
            optionalInvoiceElement.ifPresent(invoiceElement -> selectInvoiceElement = invoiceElement);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openInvoiceDialog("Create invoice element", new InvoiceElement(), true, true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openInvoiceDialog("Edit invoice element", selectInvoiceElement, true, false));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete invoice \"%s\"?", selectInvoiceElement.getCalculatedAmount()),
                    "Are you sure you want to delete this invoice permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                invoiceElementService.delete(selectInvoiceElement);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openInvoiceDialog("Details invoice element", selectInvoiceElement, false, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> (boolean) caseInsensitiveContains(s.getUser().getFirstName() + " " + s.getUser().getLastName(), event.getValue()));
    }

    private void openInvoiceDialog(String header, InvoiceElement invoiceElement, boolean isEditable, boolean isCreate) {
        InvoiceElementDialog invoiceElementDialog = new InvoiceElementDialog(header, selectInvoice, invoiceElement, isEditable, isCreate);
        invoiceElementDialog.open();
        invoiceElementDialog.getSaveButton().setEnabled(false);
        invoiceElementDialog.getSaveButton().addClickListener(e -> {
            if (isCreate) {
                refreshGrid();
            } else {
                dataProvider.refreshAll();
            }
        });
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(invoiceElementService.findAllByInvoice(selectInvoice));
        invoiceElementGrid.setDataProvider(dataProvider);
    }
}
