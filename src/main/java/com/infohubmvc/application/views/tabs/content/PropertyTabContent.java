package com.infohubmvc.application.views.tabs.content;

import com.infohubmvc.application.components.dialogs.ConfirmDialog;
import com.infohubmvc.application.components.dialogs.PropertyDialog;
import com.infohubmvc.application.components.grids.PropertyGrid;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.Property;
import com.infohubmvc.application.service.PropertyService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.infohubmvc.application.views.AbstractView;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.provider.ListDataProvider;

import java.util.Optional;

public class PropertyTabContent extends AbstractView {

    private House selectHouse;
    private Property selectProperty;
    private PropertyService propertyService;
    private ListDataProvider<Property> dataProvider;
    private Grid<Property> propertyGrid;

    public PropertyTabContent(House selectHouse) {
        propertyService = WebApplicationUtils.get(PropertyService.class);
        this.selectHouse = selectHouse;
        dataProvider = new ListDataProvider<>(propertyService.findAllPropertiesByHouseId(selectHouse));
        setSpacing(false);
        getStyle().set("padding", "0%");
        setWidthFull();
        setHeightFull();
        configureGrid();
        createButton();
        editButton();
        deleteButton();
        detailsButton();
        add(topHorizontalLayout(), propertyGrid);
    }

    @Override
    protected void configureGrid() {
        propertyGrid = new PropertyGrid();
        propertyGrid.setDataProvider(dataProvider);
        propertyGrid.setHeightFull();
        if (dataProvider.getItems().size() != 0) {
            selectProperty = dataProvider.getItems().stream().findFirst().get();
        }
        propertyGrid.select(selectProperty);
        propertyGrid.addSelectionListener(selection -> {
            Optional<Property> optionalProperty = selection.getFirstSelectedItem();
            optionalProperty.ifPresent(condominium -> selectProperty = condominium);
        });
    }

    @Override
    protected void createButton() {
        Button createButton = new Button("Add");
        createButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        createButton.addClickListener(event -> openPropertyDialog("New", new Property(), true));
        setCreateButton(createButton);
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void editButton() {
        Button editButton = new Button("Edit");
        editButton.setIcon(VaadinIcon.PLUS_CIRCLE_O.create());
        editButton.addClickListener(event -> openPropertyDialog("Edit", selectProperty, true));
        setEditButton(editButton);
        editButton.setIcon(VaadinIcon.EDIT.create());
        editButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
    }

    @Override
    protected void deleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(e -> {
            ConfirmDialog confirmDialog = new ConfirmDialog(
                    String.format("Delete property \"%s\"?", selectProperty.getPropertyType().getDescription()),
                    "Are you sure you want to delete this user permanently?");
            confirmDialog.getDeleteButton().addClickListener(event -> {
                propertyService.deleteProperty(selectProperty);
                refreshGrid();
            });
            confirmDialog.open();
        });
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.setIcon(VaadinIcon.TRASH.create());
        setDeleteButton(deleteButton);
    }

    @Override
    protected void detailsButton() {
        Button detailsButton = new Button("Details");
        detailsButton.addClickListener(event -> openPropertyDialog("Details", selectProperty, false));
        detailsButton.setIcon(VaadinIcon.INFO_CIRCLE.create());
        detailsButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        detailsButton.setVisible(false);
        setDetailsButton(detailsButton);
    }

    @Override
    protected void onNameFilterTextChange(HasValue.ValueChangeEvent<String> event) {
        dataProvider.setFilter(s -> {
            boolean containsHouse = caseInsensitiveContains(s.getHouse().getName(), event.getValue());
            boolean containsName = caseInsensitiveContains(s.getFirstName() + " " + s.getLastName(), event.getValue());
            boolean containsType = caseInsensitiveContains(s.getPropertyType().getDescription(), event.getValue());
            return containsHouse || containsName || containsType;
        });
    }

    private void openPropertyDialog(String header, Property property, boolean isEditable) {
        PropertyDialog propertyDialog = new PropertyDialog(header, selectHouse, property, isEditable);
        propertyDialog.getSaveButton().addClickListener(event -> refreshGrid());
        propertyDialog.getSaveButton().setEnabled(false);
        propertyDialog.open();
    }

    private void refreshGrid() {
        dataProvider = new ListDataProvider<>(propertyService.findAllPropertiesByHouseId(selectHouse));
        propertyGrid.setDataProvider(dataProvider);
    }
}
