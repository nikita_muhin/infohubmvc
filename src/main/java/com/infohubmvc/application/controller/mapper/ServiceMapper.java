package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.ServiceDTO;
import com.infohubmvc.application.data.entity.Service;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceMapper {

    public static Service toService(ServiceDTO serviceDTO, User user) {
        return Service.builder()
                .name(serviceDTO.getName())
                .contractNumber(serviceDTO.getContractNumber())
                .serviceType(serviceDTO.getCalculationType())
                .tarifUnit(serviceDTO.getTarifUnit())
                .tarif(serviceDTO.getTarif())
                .dateFrom(serviceDTO.getDateFrom())
                .dateTo(serviceDTO.getDateTo())
                .user(user)
                .build();
    }

    public static ServiceDTO toServiceDTO(Service service) {
        return ServiceDTO.builder()
                .id(service.getId())
                .name(service.getName())
                .contractNumber(service.getContractNumber())
                .calculationType(service.getServiceType())
                .tarifUnit(service.getTarifUnit())
                .tarif(service.getTarif())
                .dateFrom(service.getDateFrom())
                .dateTo(service.getDateTo())
                .dateCreated(service.getDateCreated())
                .build();
    }
}
