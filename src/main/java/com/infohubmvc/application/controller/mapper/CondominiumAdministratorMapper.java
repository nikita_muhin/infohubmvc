package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.CondominiumAdministratorDTO;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.CondominiumAdministrator;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CondominiumAdministratorMapper {

    public static CondominiumAdministratorDTO toCondominiumAdministratorDTO(CondominiumAdministrator condominiumAdministrator) {
        return CondominiumAdministratorDTO.builder()
                .id(condominiumAdministrator.getId())
                .jobFunction(condominiumAdministrator.getJobFunction())
                .firstName(condominiumAdministrator.getFirstName())
                .lastName(condominiumAdministrator.getLastName())
                .dateStart(condominiumAdministrator.getDateStart())
                .dateEnd(condominiumAdministrator.getDateEnd())
                .email(condominiumAdministrator.getEmail())
                .phoneNumbers(condominiumAdministrator.getPhoneNumbers())
                .dateCreated(condominiumAdministrator.getDateCreated())
                .build();
    }

    public static CondominiumAdministrator toCondominiumAdministrator(CondominiumAdministratorDTO condominiumAdministratorDTO,
                                                                      Condominium condominium, User user) {
        return CondominiumAdministrator.builder()
                .jobFunction(condominiumAdministratorDTO.getJobFunction())
                .firstName(condominiumAdministratorDTO.getFirstName())
                .lastName(condominiumAdministratorDTO.getLastName())
                .dateStart(condominiumAdministratorDTO.getDateStart())
                .dateEnd(condominiumAdministratorDTO.getDateEnd())
                .email(condominiumAdministratorDTO.getEmail())
                .phoneNumbers(condominiumAdministratorDTO.getPhoneNumbers())
                .dateCreated(condominiumAdministratorDTO.getDateCreated())
                .condominium(condominium)
                .user(user)
                .build();
    }
}
