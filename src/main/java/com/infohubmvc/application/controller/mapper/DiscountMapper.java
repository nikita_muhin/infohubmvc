package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.DiscountDTO;
import com.infohubmvc.application.data.entity.Discount;
import com.infohubmvc.application.data.entity.DiscountType;
import com.infohubmvc.application.data.entity.Unit;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.utils.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DiscountMapper {

    public static DiscountDTO toDiscountDTO(Discount discount) {
        return DiscountDTO.builder()
                .id(discount.getId())
                .discountType((discount.getDiscountType() == null) ? "" : discount.getDiscountType().getCode())
                .description(discount.getDescription())
                .discountValue(discount.getDiscountValue())
                .discountUnit((discount.getDiscountUnit() == null) ? "" : discount.getDiscountUnit().getCode())
                .dateFrom((discount.getDateFrom() == null) ? "" : DateUtil.getDateAsString(discount.getDateFrom()))
                .dateTo((discount.getDateTo() == null) ? "" : DateUtil.getDateAsString(discount.getDateTo()))
                .dateCreated(discount.getDateCreated())
                .active(discount.getActive())
                .build();
    }

    public static Discount toDiscount(DiscountDTO discountDTO, User user, DiscountType discountType, Unit unit) {
        return Discount.builder()
                .discountType(discountType)
                .description(discountDTO.getDescription())
                .discountValue(discountDTO.getDiscountValue())
                .discountUnit(unit)
                .dateFrom(DateUtil.getLocalDateFromString(discountDTO.getDateFrom()))
                .dateTo(DateUtil.getLocalDateFromString(discountDTO.getDateTo()))
                .user(user)
                .active(discountDTO.getActive())
                .build();
    }

}
