package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.CondominiumDTO;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CondominiumMapper {

    public static Condominium toCondominium(CondominiumDTO condominiumDTO, User user) {
        return Condominium.builder()
                .name(condominiumDTO.getName())
                .registrationNumber(condominiumDTO.getRegistrationNumber())
                .officeAddress(condominiumDTO.getOfficeAddress())
                .infohubContractNr(condominiumDTO.getInfohubContractNr())
                .status(condominiumDTO.getStatus())
                .user(user)
                .build();
    }

    public static Condominium toCondominiumForEdit(CondominiumDTO condominiumDTO, User user) {
        return Condominium.builder()
                .id(condominiumDTO.getId())
                .name(condominiumDTO.getName())
                .registrationNumber(condominiumDTO.getRegistrationNumber())
                .officeAddress(condominiumDTO.getOfficeAddress())
                .infohubContractNr(condominiumDTO.getInfohubContractNr())
                .status(condominiumDTO.getStatus())
                .dateCreated(condominiumDTO.getDateCreated())
                .user(user)
                .build();
    }

    public static CondominiumDTO toCondominiumDTO(Condominium condominium) {
        return CondominiumDTO.builder()
                .id(condominium.getId())
                .name(condominium.getName())
                .registrationNumber(condominium.getRegistrationNumber())
                .officeAddress(condominium.getOfficeAddress())
                .infohubContractNr(condominium.getInfohubContractNr())
                .status(condominium.getStatus())
                .dateCreated(condominium.getDateCreated())
                .tellerId(condominium.getUser().getId())
                .houses(HouseMapper.toHouseDTOS(condominium.getHouses()))
                .build();
    }

    public static List<CondominiumDTO> toCondominiumDTOS(List<Condominium> condominiumList) {
        return condominiumList.stream()
                .map(CondominiumMapper::toCondominiumDTO)
                .collect(Collectors.toList());
    }
}
