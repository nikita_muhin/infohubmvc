package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.ConsumptionPointDTO;
import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.utils.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConsumptionPointMapper {

    public static ConsumptionPoint toConsumptionPoint(ConsumptionPointDTO consumptionPointDTO, Condominium condominium,
                                                      House house, Property property, CalculationType calculationType,
                                                      Discount discount, User user, ServiceType serviceType) {
        LocalDate localDate = DateUtil.getLocalDateFromString(consumptionPointDTO.getValidityDateCounter());
        return ConsumptionPoint.builder()
                //.parentId(consumptionPointDTO.getParentId())
                .serviceType(serviceType)
                .discountValue(consumptionPointDTO.getDiscountValue())
                .counterNumber(consumptionPointDTO.getCounterNumber())
                .validityDateCounter(localDate)
                .initialCounterIndex(consumptionPointDTO.getInitialCounterIndex())
                .condominium(condominium)
                .house(house)
                .property(property)
                .calculationType(calculationType)
                .discountCode(discount.getDiscountType().getCode())
                .user(user)
                .build();
    }

    public static ConsumptionPointDTO toConsumptionPointDTO(ConsumptionPoint consumptionPoint) {
        return ConsumptionPointDTO.builder()
                .id(consumptionPoint.getId())
                .parentId((consumptionPoint.getParent() == null) ? null : consumptionPoint.getParent().getId())
                .serviceType(consumptionPoint.getServiceType().getCode())
                .discountValue(consumptionPoint.getDiscountValue())
                .counterNumber(consumptionPoint.getCounterNumber())
                .validityDateCounter(DateUtil.getDateAsString(consumptionPoint.getValidityDateCounter()))
                .initialCounterIndex(consumptionPoint.getInitialCounterIndex())
                .dateCreated(consumptionPoint.getDateCreated())
                .calculationType(consumptionPoint.getCalculationType().getCode())
                .build();
    }

    public static List<ConsumptionPointDTO> toConsumptionPointDTOS(List<ConsumptionPoint> consumptionPoints) {
        return consumptionPoints.stream()
                .map(ConsumptionPointMapper::toConsumptionPointDTO)
                .collect(Collectors.toList());
    }
}
