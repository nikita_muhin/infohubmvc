package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.HouseDTO;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.StatusCode;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HouseMapper {

    public static House toHouse(HouseDTO houseDTO, Condominium condominium, User user, StatusCode statusCode) {
        return House.builder()
                .name(houseDTO.getName())
                .houseAddress(houseDTO.getHouseAddress())
                .numberBlocks(houseDTO.getNumberBlocks())
                .numberFloors(houseDTO.getNumberFloors())
                .numberStairs(houseDTO.getNumberStairs())
                .totalArea(houseDTO.getTotalArea())
                .yearBuilt(houseDTO.getYearBuilt())
                .hasLift(houseDTO.getHasLift())
                .status(statusCode)
                .condominium(condominium)
                .user(user)
                .build();
    }

    public static House toHouseForEdit(HouseDTO houseDTO, Condominium condominium, User user, StatusCode statusCode) {
        return House.builder()
                .id(houseDTO.getId())
                .name(houseDTO.getName())
                .houseAddress(houseDTO.getHouseAddress())
                .numberBlocks(houseDTO.getNumberBlocks())
                .numberFloors(houseDTO.getNumberFloors())
                .numberStairs(houseDTO.getNumberStairs())
                .totalArea(houseDTO.getTotalArea())
                .yearBuilt(houseDTO.getYearBuilt())
                .hasLift(houseDTO.getHasLift())
                .status(statusCode)
                .condominium(condominium)
                .dateCreated(houseDTO.getDateCreated())
                .user(user)
                .build();
    }


    public static HouseDTO toHouseDTO(House house) {
        return HouseDTO.builder()
                .id(house.getId())
                .name(house.getName())
                .houseAddress(house.getHouseAddress())
                .numberBlocks(house.getNumberBlocks())
                .numberFloors(house.getNumberFloors())
                .numberStairs(house.getNumberStairs())
                .totalArea(house.getTotalArea())
                .yearBuilt(house.getYearBuilt())
                .hasLift(house.getHasLift())
                .status(house.getStatus() == null ? "" : house.getStatus().getCode())
                .dateCreated(house.getDateCreated())
                .condominiumId(house.getCondominium() == null ? null : house.getCondominium().getId())
                //.condominiumDTO(CondominiumMapper.toCondominiumDTO(house.getCondominium()))
                .properties(PropertyMapper.toPropertyDTOS(house.getProperty()))
                .build();
    }

    public static List<HouseDTO> toHouseDTOS(List<House> houses) {
        return houses.stream()
                .map(HouseMapper::toHouseDTO)
                .collect(Collectors.toList());
    }
}
