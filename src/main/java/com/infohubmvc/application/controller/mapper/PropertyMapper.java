package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.PropertyDTO;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.Property;
import com.infohubmvc.application.data.entity.PropertyType;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyMapper {

    public static PropertyDTO toPropertyDTO(Property property) {
        return PropertyDTO.builder()
                .id(property.getId())
                .propertyType(property.getPropertyType().getCode())
                .firstName(property.getFirstName())
                .lastName(property.getLastName())
                .floor(property.getFloor())
                .address(property.getAddress())
                .phoneNumbers(property.getPhoneNumbers())
                .email(property.getEmail())
                .staircase(property.getStaircase())
                .area(property.getArea())
                .numberRooms(property.getNumberRooms())
                .numberMembers(property.getNumberMembers())
                .userGroupId(property.getUserGroupId())
                .dateCreated(property.getDateCreated())
                .houseId(property.getHouse().getId())
                .houseDesc(property.getHouse().getName() + "("+property.getHouse().getCondominium().getName()+")")
                .tellerId(property.getUser().getId())
                .consumptionPoints(ConsumptionPointMapper.toConsumptionPointDTOS(property.getConsumptionPoints()))
                .build();
    }

    public static Property toProperty(PropertyDTO propertyDTO, User user, House house, PropertyType propertyType) {
        return Property.builder()
                .propertyType(propertyType)
                .firstName(propertyDTO.getFirstName())
                .lastName(propertyDTO.getLastName())
                .floor(propertyDTO.getFloor())
                .address(propertyDTO.getAddress())
                .phoneNumbers(propertyDTO.getPhoneNumbers())
                .email(propertyDTO.getEmail())
                .staircase(propertyDTO.getStaircase())
                .area(propertyDTO.getArea())
                .numberRooms(propertyDTO.getNumberRooms())
                .numberMembers(propertyDTO.getNumberMembers())
                .userGroupId(propertyDTO.getUserGroupId())
                .user(user)
                .house(house)
                .build();
    }

    public static Property toPropertyForUpdate(PropertyDTO propertyDTO, User user, House house, PropertyType propertyType) {
        return Property.builder()
                .id(propertyDTO.getId())
                .propertyType(propertyType)
                .firstName(propertyDTO.getFirstName())
                .lastName(propertyDTO.getLastName())
                .floor(propertyDTO.getFloor())
                .address(propertyDTO.getAddress())
                .phoneNumbers(propertyDTO.getPhoneNumbers())
                .email(propertyDTO.getEmail())
                .staircase(propertyDTO.getStaircase())
                .area(propertyDTO.getArea())
                .numberRooms(propertyDTO.getNumberRooms())
                .numberMembers(propertyDTO.getNumberMembers())
                .userGroupId(propertyDTO.getUserGroupId())
                .dateCreated(propertyDTO.getDateCreated())
                .user(user)
                .house(house)
                .build();
    }

    public static List<PropertyDTO> toPropertyDTOS(List<Property> properties) {
        return properties.stream()
                .map(PropertyMapper::toPropertyDTO)
                .collect(Collectors.toList());
    }
}
