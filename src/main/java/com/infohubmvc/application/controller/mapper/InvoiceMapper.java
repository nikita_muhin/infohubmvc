package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.InvoiceDTO;
import com.infohubmvc.application.data.entity.Beneficiary;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InvoiceMapper {

    public static Invoice toInvoice(InvoiceDTO invoiceDTO, Condominium condominium, Beneficiary beneficiary, User user) {
        return Invoice.builder()
                .condominium(condominium)
                .beneficiary(beneficiary)
                .invMonth(invoiceDTO.getInvMonth())
                .invYear(invoiceDTO.getInvYear())
                .calculatedAmount(invoiceDTO.getCalculatedAmount())
                .debt(invoiceDTO.getDebt())
                .penalty(invoiceDTO.getPenalty())
                .dateCreated(invoiceDTO.getDateCreated())
                .user(user)
                .build();
    }

    public static InvoiceDTO toInvoiceDTO(Invoice invoice) {
        return InvoiceDTO.builder()
                .id(invoice.getId())
                .beneficiaryId(invoice.getBeneficiary().getId())
                .condominiumId(invoice.getCondominium().getId())
                .calculatedAmount(invoice.getCalculatedAmount())
                .debt(invoice.getDebt())
                .penalty(invoice.getPenalty())
                .invMonth(invoice.getInvMonth())
                .invYear(invoice.getInvYear())
                .dateCreated(invoice.getDateCreated())
                .tellerId(invoice.getUser().getId())
                .build();
    }

    public static List<InvoiceDTO> toInvoiceDTOS(List<Invoice> invoices) {
        return invoices.stream()
                .map(InvoiceMapper::toInvoiceDTO)
                .collect(Collectors.toList());
    }
}
