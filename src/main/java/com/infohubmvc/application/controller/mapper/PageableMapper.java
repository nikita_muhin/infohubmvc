package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.PageDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PageableMapper {


    public static Pageable toPageable(Integer page, Integer size) {
        Integer pageL = 0;
        if (page != null && page > 0) {
            pageL = page - 1;
        }
        Integer sizeL = size;
        if (size == null || size == 0) {
            sizeL = 10;
        }
        return PageRequest.of(pageL, sizeL);
    }

    public static PageDTO toPageDTO(Page page) {
        return PageDTO.builder()
                .page(page.getPageable().getPageNumber() + 1)
                .size(page.getPageable().getPageSize())
                .totalElements(page.getTotalElements())
                .totalPages(page.getTotalPages())
                .build();
    }

}
