package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.CalculationFormulaDTO;
import com.infohubmvc.application.data.entity.CalculationFormula;
import com.infohubmvc.application.data.entity.CalculationType;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.utils.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CalculationFormulaMapper {

    public static CalculationFormula toCalculationFormula(CalculationFormulaDTO calculationFormulaDTO, User user, CalculationType calculationType) {
        return CalculationFormula.builder()
                .id(calculationFormulaDTO.getId())
                .code(calculationFormulaDTO.getCode())
                .description(calculationFormulaDTO.getDescription())
                .calculationType(calculationType)
                .formula(calculationFormulaDTO.getFormula())
                .dateFrom(DateUtil.getLocalDateFromString(calculationFormulaDTO.getDateFrom()))
                .dateTo(DateUtil.getLocalDateFromString(calculationFormulaDTO.getDateTo()))
                .active(calculationFormulaDTO.getActive())
                .user(user)
                .build();
    }

    public static CalculationFormulaDTO toCalculationFormulaDTO(CalculationFormula calculationFormula) {
        return CalculationFormulaDTO.builder()
                .id(calculationFormula.getId())
                .code(calculationFormula.getCode())
                .description(calculationFormula.getDescription())
                .calculationType((calculationFormula.getCalculationType() == null) ? "" : calculationFormula.getCalculationType().getCode())
                .formula(calculationFormula.getFormula())
                .dateCreated(calculationFormula.getDateCreated())
                .dateFrom((calculationFormula.getDateFrom() == null) ? "" : DateUtil.getDateAsString(calculationFormula.getDateFrom()))
                .dateTo((calculationFormula.getDateTo() == null) ? "" : DateUtil.getDateAsString(calculationFormula.getDateTo()))
                .active(calculationFormula.getActive())
                .build();
    }

    public static List<CalculationFormulaDTO> toCalculationFormulaDTOS(List<CalculationFormula> calculationFormulas) {
        return calculationFormulas.stream()
                .map(CalculationFormulaMapper::toCalculationFormulaDTO)
                .collect(Collectors.toList());
    }
}
