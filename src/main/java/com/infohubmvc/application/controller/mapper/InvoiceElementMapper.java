package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.InvoiceElementDTO;
import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.InvoiceElement;
import com.infohubmvc.application.data.entity.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InvoiceElementMapper {

    public static InvoiceElement toInvoiceElement(InvoiceElementDTO invoiceElementDTO, Invoice invoice,
                                                  ConsumptionPoint consumptionPoint, User user) {
        return InvoiceElement.builder()
                .dateFrom(invoiceElementDTO.getDateFrom())
                .payUntil(invoiceElementDTO.getPayUntil())
                .indiceStart(invoiceElementDTO.getIndiceStart())
                .indiceEnd(invoiceElementDTO.getIndiceEnd())
                .untCost(invoiceElementDTO.getUntCost())
                .units(invoiceElementDTO.getUnits())
                .calculatedAmount(invoiceElementDTO.getCalculatedAmount())
                .debt(invoiceElementDTO.getDebt())
                .penalty(invoiceElementDTO.getPenalty())
                .invoice(invoice)
                .consumptionPoint(consumptionPoint)
                .user(user)
                .build();
    }

    public static InvoiceElementDTO toInvoiceElementDTO(InvoiceElement invoiceElement) {
        return InvoiceElementDTO.builder()
                .dateFrom(invoiceElement.getDateFrom())
                .payUntil(invoiceElement.getPayUntil())
                .indiceStart(invoiceElement.getIndiceStart())
                .indiceEnd(invoiceElement.getIndiceEnd())
                .untCost(invoiceElement.getUntCost())
                .units(invoiceElement.getUnits())
                .calculatedAmount(invoiceElement.getCalculatedAmount())
                .debt(invoiceElement.getDebt())
                .penalty(invoiceElement.getPenalty())
                .build();
    }

}
