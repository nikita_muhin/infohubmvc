package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.BeneficiaryDTO;
import com.infohubmvc.application.data.entity.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BeneficiaryMapper {

    public static Beneficiary toBeneficiary(BeneficiaryDTO beneficiaryDTO, Condominium condominium, House house, Service service, User user) {
        return Beneficiary.builder()
                .condominium(condominium)
                .house(house)
                .service(service)
                .serviceName(beneficiaryDTO.getServiceName())
                .calculationCode(beneficiaryDTO.getCalculationCode())
                .tarif(beneficiaryDTO.getTarif())
                .initialCounterIndex(beneficiaryDTO.getInitialCounterIndex())
                .dateCreated(beneficiaryDTO.getDateCreated())
                .user(user)
                .build();
    }

    public static BeneficiaryDTO toBeneficiaryDTO(Beneficiary beneficiary) {
        return BeneficiaryDTO.builder()
                .id(beneficiary.getId())
                .condominiumId(beneficiary.getCondominium().getId())
                .houseId(beneficiary.getHouse().getId())
                .serviceId(beneficiary.getService().getId())
                .serviceName(beneficiary.getServiceName())
                .calculationCode(beneficiary.getCalculationCode())
                .tarif(beneficiary.getTarif())
                .initialCounterIndex(beneficiary.getInitialCounterIndex())
                .dateCreated(beneficiary.getDateCreated())
                .tellerId(beneficiary.getUser().getId())
                .build();
    }

    public static List<BeneficiaryDTO> toBeneficiaryDTOs(List<Beneficiary> beneficiaries) {
        return beneficiaries.stream()
                .map(BeneficiaryMapper::toBeneficiaryDTO)
                .collect(Collectors.toList());
    }
}
