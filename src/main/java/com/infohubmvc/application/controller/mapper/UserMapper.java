package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.UserDTO;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.data.entity.UserRole;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserMapper {

    public static User toUser(UserDTO userDTO, Set<UserRole> userRoles) {
        return User.builder()
                .groupId(userDTO.getGroupId())
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .userRoles(userRoles)
                .status(userDTO.getStatus())
                .email(userDTO.getEmail())
                .phoneNumber(userDTO.getPhoneNumber())
                .build();
    }

    public static UserDTO toUserDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .groupId(user.getGroupId())
                .username(user.getUsername())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                //.accessRoles(RoleMapper.toDTOS(user.getUserRoles()))
                .status(user.getStatus())
                .email(user.getEmail())
                .dateCreated(user.getDateCreated())
                .phoneNumber(user.getPhoneNumber())
                .build();
    }

    public static List<UserDTO> toUserDTOS(List<User> users) {
        return users.stream()
                .map(UserMapper::toUserDTO)
                .collect(Collectors.toList());
    }
}
