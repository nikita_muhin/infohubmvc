package com.infohubmvc.application.controller.mapper;

import com.infohubmvc.application.controller.dto.DictionaryDTO;
import com.infohubmvc.application.data.entity.*;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DictionaryMapper {


    public static DictionaryDTO toDTO(PropertyType ob) {
        return DictionaryDTO.builder()
                .code(ob.getCode())
                .description(ob.getDescription())
                .build();
    }

    public static List<DictionaryDTO> toPropertyTypeDTOS(List<PropertyType> obs) {
        return obs.stream()
                .map(DictionaryMapper::toDTO)
                .collect(Collectors.toList());
    }



    public static DictionaryDTO toDTO(CalculationType ob) {
        return DictionaryDTO.builder()
                .code(ob.getCode())
                .description(ob.getDescription())
                .build();
    }

    public static List<DictionaryDTO> toCalculationTypeDTOS(List<CalculationType> obs) {
        return obs.stream()
                .map(DictionaryMapper::toDTO)
                .collect(Collectors.toList());
    }

    public static DictionaryDTO toDTO(StatusCode ob) {
        return DictionaryDTO.builder()
                .code(ob.getCode())
                .description(ob.getDescription())
                .build();
    }

    public static List<DictionaryDTO> toStatusCodeDTOS(List<StatusCode> obs) {
        return obs.stream()
                .map(DictionaryMapper::toDTO)
                .collect(Collectors.toList());
    }

    public static DictionaryDTO toDTO(ServiceType ob) {
        return DictionaryDTO.builder()
                .code(ob.getCode())
                .description(ob.getDescription())
                .build();
    }

    public static List<DictionaryDTO> toServiceTypeDTOS(List<ServiceType> obs) {
        return obs.stream()
                .map(DictionaryMapper::toDTO)
                .collect(Collectors.toList());
    }

    public static DictionaryDTO toDTO(DiscountType ob) {
        return DictionaryDTO.builder()
                .code(ob.getCode())
                .description(ob.getDescription())
                .build();
    }

    public static List<DictionaryDTO> toDiscountTypeDTOS(List<DiscountType> obs) {
        return obs.stream()
                .map(DictionaryMapper::toDTO)
                .collect(Collectors.toList());
    }

    public static StatusCode toStatusCode(DictionaryDTO dictionaryDTO) {
        return StatusCode.builder()
                .code(dictionaryDTO.getCode())
                .description(dictionaryDTO.getDescription())
                .build();
    }

    public static PropertyType toPropertyType(DictionaryDTO dictionaryDTO) {
        return PropertyType.builder()
                .code(dictionaryDTO.getCode())
                .description(dictionaryDTO.getDescription())
                .build();
    }

    public static CalculationType toCalculation1Type(DictionaryDTO dictionaryDTO) {
        return CalculationType.builder()
                .code(dictionaryDTO.getCode())
                .description(dictionaryDTO.getDescription())
                .build();
    }

    public static DiscountType toDiscountType(DictionaryDTO dictionaryDTO) {
        return DiscountType.builder()
                .code(dictionaryDTO.getCode())
                .description(dictionaryDTO.getDescription())
                .build();
    }
    public static ServiceType toServiceType(DictionaryDTO dictionaryDTO) {
        return ServiceType.builder()
                .code(dictionaryDTO.getCode())
                .description(dictionaryDTO.getDescription())
                .build();
    }

    public static Unit toUnit(DictionaryDTO dictionaryDTO) {
        return Unit.builder()
                .code(dictionaryDTO.getCode())
                .description(dictionaryDTO.getDescription())
                .build();
    }

    public static DictionaryDTO toDTO(Unit ob) {
        return DictionaryDTO.builder()
                .code(ob.getCode())
                .description(ob.getDescription())
                .build();
    }

    public static List<DictionaryDTO> toUnitDTOS(List<Unit> obs) {
        return obs.stream()
                .map(DictionaryMapper::toDTO)
                .collect(Collectors.toList());
    }
}
