package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceElementDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotNull
    private LocalDate dateFrom;

    @NotNull
    private LocalDate payUntil;

    @NotNull
    private Long indiceStart;

    @NotNull
    private Long indiceEnd;

    @NotNull
    private BigDecimal untCost;

    @NotNull
    private BigDecimal units;

    @NotNull
    private BigDecimal calculatedAmount;

    @NotNull
    private BigDecimal debt;

    @NotNull
    private BigDecimal penalty;

    @Nullable
    private LocalDateTime dateCreated;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long invoiceId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long consumptionPointId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        InvoiceElementDTO that = (InvoiceElementDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (dateFrom != null ? !dateFrom.equals(that.dateFrom) : that.dateFrom != null) return false;
        if (payUntil != null ? !payUntil.equals(that.payUntil) : that.payUntil != null) return false;
        if (indiceStart != null ? !indiceStart.equals(that.indiceStart) : that.indiceStart != null) return false;
        if (indiceEnd != null ? !indiceEnd.equals(that.indiceEnd) : that.indiceEnd != null) return false;
        if (untCost != null ? !untCost.equals(that.untCost) : that.untCost != null) return false;
        if (units != null ? !units.equals(that.units) : that.units != null) return false;
        if (calculatedAmount != null ? !calculatedAmount.equals(that.calculatedAmount) : that.calculatedAmount != null)
            return false;
        if (debt != null ? !debt.equals(that.debt) : that.debt != null) return false;
        if (penalty != null ? !penalty.equals(that.penalty) : that.penalty != null) return false;
        if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
        if (invoiceId != null ? !invoiceId.equals(that.invoiceId) : that.invoiceId != null) return false;
        if (consumptionPointId != null ? !consumptionPointId.equals(that.consumptionPointId) : that.consumptionPointId != null)
            return false;
        return tellerId != null ? tellerId.equals(that.tellerId) : that.tellerId == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (dateFrom != null ? dateFrom.hashCode() : 0);
        result = 31 * result + (payUntil != null ? payUntil.hashCode() : 0);
        result = 31 * result + (indiceStart != null ? indiceStart.hashCode() : 0);
        result = 31 * result + (indiceEnd != null ? indiceEnd.hashCode() : 0);
        result = 31 * result + (untCost != null ? untCost.hashCode() : 0);
        result = 31 * result + (units != null ? units.hashCode() : 0);
        result = 31 * result + (calculatedAmount != null ? calculatedAmount.hashCode() : 0);
        result = 31 * result + (debt != null ? debt.hashCode() : 0);
        result = 31 * result + (penalty != null ? penalty.hashCode() : 0);
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (invoiceId != null ? invoiceId.hashCode() : 0);
        result = 31 * result + (consumptionPointId != null ? consumptionPointId.hashCode() : 0);
        result = 31 * result + (tellerId != null ? tellerId.hashCode() : 0);
        return result;
    }
}
