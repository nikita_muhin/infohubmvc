package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infohubmvc.application.data.entity.UserStatus;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends BaseDTO {

    @Nullable
    private Long id;

    @Nullable
    private Long groupId;

    @NotBlank
    private String username;

    @NotBlank
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String email;

    @NotBlank
    private Set<RoleDTO> accessRoles;

    @NotNull
    private UserStatus status;

    @Nullable
    private LocalDateTime dateCreated;

    @NotBlank
    private String phoneNumber;

}
