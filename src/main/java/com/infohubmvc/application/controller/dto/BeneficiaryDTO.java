package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BeneficiaryDTO extends BaseDTO  {

    @Nullable
    Long id;

    @NotNull
    private Long condominiumId;

    @NotNull
    private Long houseId;

    @NotNull
    private Long serviceId;

    @NotEmpty
    private String serviceName;

    @NotEmpty
    private String calculationCode;

    @NotNull
    private BigDecimal tarif;

    @Nullable
    private Long initialCounterIndex;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;
}