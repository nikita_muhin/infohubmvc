package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDTO extends BaseDTO  {

    @Nullable
    Long id;

    @NotNull
    private Long beneficiaryId;
    @NotNull
    private Long condominiumId;


    @NotNull
    private Integer invMonth;

    @NotNull
    private Integer invYear;

    @NotNull
    private BigDecimal calculatedAmount;

    @NotNull
    private BigDecimal debt;

    @NotNull
    private BigDecimal penalty;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;
}