package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HouseDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String houseAddress;

    @NotNull
    @Positive
    private Long numberBlocks;

    @NotNull
    @Positive
    private Long numberFloors;

    @NotNull
    @Positive
    private Long numberStairs;

    @NotNull
    @Positive
    private BigDecimal totalArea;

    @NotNull
    @Positive
    private Long yearBuilt;

    @NotNull
    private Boolean hasLift;

    @NotNull
    private String status;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long condominiumId;

    //private CondominiumDTO condominiumDTO;

    @Positive
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;

    private List<PropertyDTO> properties;
}
