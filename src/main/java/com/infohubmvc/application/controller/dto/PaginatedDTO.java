package com.infohubmvc.application.controller.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class PaginatedDTO {

    @NotNull
    private PageDTO page;

    private List<BaseDTO> payload;
}