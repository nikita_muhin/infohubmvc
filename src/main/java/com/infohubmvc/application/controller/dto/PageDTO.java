package com.infohubmvc.application.controller.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Positive;

@Data
@Builder
public class PageDTO {

    @Positive
    private Integer page;

    @Positive
    private Integer size;

    @Positive
    private Integer totalPages;

    @Positive
    private Long totalElements;

}