package com.infohubmvc.application.controller.dto;

import com.infohubmvc.application.data.entity.ServiceType;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String contractNumber;

    @NotNull
    private ServiceType calculationType;

    @NotNull
    private Long tarifUnit;

    @NotNull
    private Long tarif;

    @NotNull
    private LocalDate dateFrom;

    @NotNull
    private LocalDate dateTo;

    @Nullable
    private LocalDateTime dateCreated;

    @Nullable
    private Long tellerId;
}
