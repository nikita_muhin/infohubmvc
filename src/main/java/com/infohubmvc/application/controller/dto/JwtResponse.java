package com.infohubmvc.application.controller.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String username;
    private String email;
    private List<String> roles;
    private Map<String, Set<String>> menuAccess;

    public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles, Map<String, Set<String>> menuAccess) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.menuAccess = menuAccess;
    }

}