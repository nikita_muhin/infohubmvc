package com.infohubmvc.application.controller.dto;

import com.infohubmvc.application.data.entity.CondominiumStatus;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CondominiumDTO extends BaseDTO {

    @Nullable
    Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String registrationNumber;

    @NotBlank
    private String officeAddress;

    @NotBlank
    private String infohubContractNr;

    @NotNull
    private CondominiumStatus status;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;

    private List<HouseDTO> houses;
}