package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsumptionPointDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotNull
    private Long parentId;

    @NotNull
    private String serviceType;

    @NotNull
    private Long discountValue;

    @NotNull
    private String counterNumber;

    @NotNull
    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private String validityDateCounter; // 2021-12-01

    @NotNull
    private Long initialCounterIndex;

    @Nullable
    private LocalDateTime dateCreated;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long condominiumId;
    private String condominiumDesc;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long houseId;

    private String houseDesc;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long propertyId;
    private String propertyDesc;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String calculationType;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String discountCode;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;
}
