package com.infohubmvc.application.controller.dto;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PropertyDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotNull
    private String propertyType;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private Long floor;

    @NotNull
    private String address;

    @NotNull
    private String phoneNumbers;

    @NotNull
    private String email;

    @NotNull
    private Integer staircase;

    @NotNull
    private BigDecimal area;

    @NotNull
    private Integer numberRooms;

    @NotNull
    private Integer numberMembers;

    @NotNull
    private Long userGroupId;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long houseId;

    @Nullable
    private String houseDesc;

    @Positive
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;

    private List<ConsumptionPointDTO> consumptionPoints;

}
