package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DiscountDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotNull
    private String discountType;

    @NotNull
    private String description;

    @NotNull
    @Positive
    private BigDecimal discountValue;

    @NotNull
    private String discountUnit;

    @NotNull
    private String dateFrom;

    @NotNull
    private String dateTo;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;

    private Boolean active;
}
