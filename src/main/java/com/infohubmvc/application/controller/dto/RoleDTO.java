package com.infohubmvc.application.controller.dto;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDTO extends BaseDTO  {

    @Nullable
    private Integer id;

    @NotBlank
    private String name;

    private boolean viewPermission;

    private boolean managePermission;
}
