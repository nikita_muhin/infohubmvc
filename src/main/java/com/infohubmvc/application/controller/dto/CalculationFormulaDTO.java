package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CalculationFormulaDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String description;

    @NotNull
    private String calculationType;

    @NotNull
    private String formula;

    @Nullable
    private LocalDateTime dateCreated;

    @Positive
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;

    @NotNull
    private String dateFrom;

    @NotNull
    private String dateTo;

    private Boolean active;
}
