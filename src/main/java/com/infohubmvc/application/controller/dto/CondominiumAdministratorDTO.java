package com.infohubmvc.application.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.infohubmvc.application.data.entity.JobFunction;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CondominiumAdministratorDTO extends BaseDTO  {

    @Nullable
    private Long id;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long condominiumId;

    @NotNull
    private JobFunction jobFunction;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private LocalDate dateStart;

    @NotNull
    private LocalDate dateEnd;

    @NotNull
    private String email;

    @NotNull
    private String phoneNumbers;

    @Nullable
    private LocalDateTime dateCreated;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long tellerId;
}
