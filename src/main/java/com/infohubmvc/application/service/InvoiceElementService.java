package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.InvoiceElementDTO;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.InvoiceElement;

import java.util.List;

public interface InvoiceElementService {

    List<InvoiceElement> findAllByInvoice(Invoice invoice);

    InvoiceElement getById(Long id);

    void delete(InvoiceElement invoiceElement);

    InvoiceElement save(InvoiceElement invoiceElement);

    InvoiceElement addInvoiceElement(InvoiceElementDTO invoiceElementDTO);
}
