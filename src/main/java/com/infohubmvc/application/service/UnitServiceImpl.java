package com.infohubmvc.application.service;

import com.infohubmvc.application.data.entity.Unit;
import com.infohubmvc.application.repositories.UnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitServiceImpl implements UnitService{

    private final UnitRepository unitRepository;

    public UnitServiceImpl(UnitRepository unitRepository) {
        this.unitRepository = unitRepository;
    }

    @Override
    public List<Unit> findAll() {
        return unitRepository.findAll();
    }

    @Override
    public Unit save(Unit unit) {
        return unitRepository.save(unit);
    }
}
