package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.ConsumptionPointDTO;
import com.infohubmvc.application.controller.mapper.ConsumptionPointMapper;
import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.repositories.ConsumptionPointRepository;
import com.infohubmvc.application.service.exception.ConsumptionPointNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsumptionPointServiceImpl extends BaseService implements ConsumptionPointService {

    private final ConsumptionPointRepository consumptionPointRepository;
    private final CondominiumService condominiumService;
    private final HouseService houseService;
    private final PropertyService propertyService;
    private final CalculationFormulaService calculationFormulaService;
    private final DiscountService discountService;
    private final DictionaryService dictionaryService;

    public ConsumptionPointServiceImpl(UserService userService,
                                       ConsumptionPointRepository consumptionPointRepository,
                                       CondominiumService condominiumService,
                                       HouseService houseService,
                                       PropertyService propertyService,
                                       CalculationFormulaService calculationFormulaService,
                                       DiscountService discountService,
                                       DictionaryService dictionaryService) {
        super(userService);
        this.consumptionPointRepository = consumptionPointRepository;
        this.condominiumService = condominiumService;
        this.houseService = houseService;
        this.propertyService = propertyService;
        this.calculationFormulaService = calculationFormulaService;
        this.discountService = discountService;
        this.dictionaryService = dictionaryService;
    }

    @Override
    public ConsumptionPoint getById(Long id) {
        return consumptionPointRepository.getById(id);
    }

    @Override
    public ConsumptionPoint getByServiceType(ServiceType serviceType) {
        return consumptionPointRepository.getByServiceType(serviceType)
                .orElseThrow(() -> new ConsumptionPointNotFoundException(serviceType));
    }

    @Override
    public List<ConsumptionPoint> getAllByParentId(Long parentId) {
        return consumptionPointRepository.findAllByParentId(parentId);
    }

    @Override
    public List<ConsumptionPoint> getAllByCondominiumId(Long condominiumId) {
        return consumptionPointRepository.findAllByCondominiumId(condominiumId);
    }

    @Override
    public List<ConsumptionPoint> getAllByHouseId(Long houseId) {
        return consumptionPointRepository.findAllByHouseId(houseId);
    }

    @Override
    public List<ConsumptionPoint> getAllByPropertyId(Long propertyId) {
        return consumptionPointRepository.findAllByPropertyId(propertyId);
    }

    @Override
    public ConsumptionPoint addConsumptionPoint(ConsumptionPointDTO consumptionPointDTO) {
        Condominium condominium = null;
        if (consumptionPointDTO.getCondominiumId() != null)
            condominium = condominiumService.getCondominiumById(consumptionPointDTO.getCondominiumId());
        House house = null;
        if (consumptionPointDTO.getHouseId() != null)
            house = houseService.getHouseById(consumptionPointDTO.getHouseId());
        Property property = null;
        if (consumptionPointDTO.getPropertyId() != null)
            property = propertyService.getPropertyById(consumptionPointDTO.getPropertyId());

        CalculationType calculationType = null;
        if (consumptionPointDTO.getCalculationType() != null)
            calculationType = dictionaryService.getCalculationTypeByCode(consumptionPointDTO.getCalculationType());

        Discount discount = null;
        if (consumptionPointDTO.getDiscountCode() != null)
            discount = discountService.getByCode(consumptionPointDTO.getDiscountCode());

        User user = getUserById(consumptionPointDTO.getTellerId());
        ServiceType serviceType = dictionaryService.findServiceTypeByCode(consumptionPointDTO.getServiceType());

        ConsumptionPoint consumptionPoint = ConsumptionPointMapper
                .toConsumptionPoint(consumptionPointDTO, condominium, house, property, calculationType, discount, user, serviceType);
        return consumptionPointRepository.save(consumptionPoint);
    }

    @Override
    public Page getAllForUser(Long userId, Pageable pageable) {
        return consumptionPointRepository.getAllForUserId(userId, pageable);
    }

    @Override
    public List<ConsumptionPoint> findAll() {
        return consumptionPointRepository.findAll();
    }

    @Override
    public ConsumptionPoint save(ConsumptionPoint consumptionPoint) {
        return consumptionPointRepository.save(consumptionPoint);
    }

    @Override
    public void delete(ConsumptionPoint consumptionPoint) {
        consumptionPointRepository.delete(consumptionPoint);
    }

}
