package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.PropertyDTO;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.Property;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PropertyService {
    Property getPropertyById(Long propertyId);

    List<Property> getPropertiesByUserId(Long userId);

    List<Property> getAll();

    Page getProperties(Pageable pageable);

    Page getPropertiesByUserId(Long userId, Pageable pageable);

    Page getPropertiesByHouseId(Long houseId, Pageable pageable);
    List<Property> findAllPropertiesByHouseId(House house);
    Property saveProperty(Property property);

    Property addProperty(PropertyDTO propertyDTO);

    Property updateProperty(PropertyDTO propertyDTO);

    void deleteProperty(Property property);
}
