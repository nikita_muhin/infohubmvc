package com.infohubmvc.application.service;

import com.infohubmvc.application.data.entity.DiscountType;
import com.infohubmvc.application.data.entity.Unit;

import java.util.List;

public interface UnitService {

    List<Unit> findAll();

    Unit save(Unit unit);
}
