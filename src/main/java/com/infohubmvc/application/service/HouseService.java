package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.HouseDTO;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface HouseService {
    House getHouseById(Long id);

    Page<House> getAll(Pageable pageable);

    List<House> getAll();

    List<House> findAllByCondominiums(Set<Condominium> condominiums);
    List<House> findAllByCondominium(Condominium condominium);


    Page<House> getAllByUserId(Long userId, Pageable pageable);

    Page<House> getAllByCondominiumId(Long condominiumId, Pageable pageable);

    House addHouse(HouseDTO houseDTO);

    House updateHouse(HouseDTO houseDTO);

    House saveHouse(House house);

    void deleteHouse(House house);

}
