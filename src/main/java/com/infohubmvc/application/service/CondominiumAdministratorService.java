package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.CondominiumAdministratorDTO;
import com.infohubmvc.application.data.entity.CondominiumAdministrator;

public interface CondominiumAdministratorService {
    CondominiumAdministrator getById(Long id);

    CondominiumAdministrator addCondominiumAdministrator(CondominiumAdministratorDTO condominiumAdministratorDTO);
}
