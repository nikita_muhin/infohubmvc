package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.DictionaryDTO;
import com.infohubmvc.application.controller.mapper.DictionaryMapper;
import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.repositories.*;
import com.infohubmvc.application.service.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DictionaryServiceImpl extends BaseService implements DictionaryService {

    private final DictionaryRepository dictionaryRepository;
    private final ServiceTypeRepository serviceTypeRepository;
    private final CalculationTypeRepository calculationTypeRepository;
    private final PropertyTypeRepository propertyTypeRepository;
    private final DiscountTypeRepository discountTypeRepository;
    private final StatusCodeRepository statusCodeRepository;
    private final UnitRepository unitRepository;

    public DictionaryServiceImpl(UserService userService,
                                 DictionaryRepository dictionaryRepository, ServiceTypeRepository serviceTypeRepository,
                                 CalculationTypeRepository calculationTypeRepository,
                                 PropertyTypeRepository propertyTypeRepository,
                                 DiscountTypeRepository discountTypeRepository,
                                 StatusCodeRepository statusCodeRepository,
                                 UnitRepository unitRepository) {
        super(userService);
        this.dictionaryRepository = dictionaryRepository;
        this.serviceTypeRepository = serviceTypeRepository;
        this.calculationTypeRepository = calculationTypeRepository;
        this.propertyTypeRepository = propertyTypeRepository;
        this.discountTypeRepository = discountTypeRepository;
        this.statusCodeRepository = statusCodeRepository;
        this.unitRepository = unitRepository;
    }

    @Override
    public ServiceType getServiceTypeByCode(String code) {
        return serviceTypeRepository.getById(code);
    }

    @Override
    public ServiceType findServiceTypeByCode(String code) {
        Optional<ServiceType> optionalServiceType = serviceTypeRepository.findByCode(code);
        if (optionalServiceType.isPresent()) {
            return optionalServiceType.get();
        } else {
            throw new EntityNotFoundException(ServiceType.class.getSimpleName(), code);
        }
    }

    @Override
    public List<ServiceType> getAllServiceTypes() {
        return serviceTypeRepository.findAll();
    }


    @Override
    public CalculationType getCalculationTypeByCode(String code) {
        return calculationTypeRepository.getById(code);
    }

    @Override
    public CalculationType findCalculationTypeByCode(String code) {
        Optional<CalculationType> optionalServiceType = calculationTypeRepository.findByCode(code);
        if (optionalServiceType.isPresent()) {
            return optionalServiceType.get();
        } else {
            throw new EntityNotFoundException(CalculationType.class.getSimpleName(), code);
        }
    }

    @Override
    public List<CalculationType> getAllCalculationTypes() {
        return calculationTypeRepository.findAll();
    }


    @Override
    public PropertyType getPropertyTypeByCode(String code) {
        return propertyTypeRepository.getById(code);
    }

    @Override
    public PropertyType findPropertyTypeByCode(String code) {
        Optional<PropertyType> optionalServiceType = propertyTypeRepository.findByCode(code);
        if (optionalServiceType.isPresent()) {
            return optionalServiceType.get();
        } else {
            throw new EntityNotFoundException(PropertyType.class.getSimpleName(), code);
        }
    }

    @Override
    public List<PropertyType> getAllPropertyTypes() {
        return propertyTypeRepository.findAll();
    }

    @Override
    public DiscountType getDiscountTypeByCode(String code) {
        return discountTypeRepository.getById(code);
    }

    @Override
    public DiscountType findDiscountTypeByCode(String code) {
        Optional<DiscountType> optionalServiceType = discountTypeRepository.findByCode(code);
        if (optionalServiceType.isPresent()) {
            return optionalServiceType.get();
        } else {
            throw new EntityNotFoundException(DiscountType.class.getSimpleName(), code);
        }
    }

    @Override
    public List<DiscountType> getAllDiscountTypes() {
        return discountTypeRepository.findAll();
    }

    @Override
    public StatusCode getStatusCodeByCode(String code) {
        return statusCodeRepository.getById(code);
    }

    @Override
    public StatusCode findStatusCodeByCode(String code) {
        Optional<StatusCode> optionalServiceType = statusCodeRepository.findByCode(code);
        if (optionalServiceType.isPresent()) {
            return optionalServiceType.get();
        } else {
            throw new EntityNotFoundException(StatusCode.class.getSimpleName(), code);
        }
    }

    @Override
    public List<StatusCode> getAllStatusCodes() {
        return statusCodeRepository.findAll();
    }

    @Override
    public StatusCode saveStatus(DictionaryDTO dictionaryDTO) {
        return statusCodeRepository.save(DictionaryMapper.toStatusCode(dictionaryDTO));
    }

    @Override
    public CalculationType saveCalculationType(DictionaryDTO dictionaryDTO) {
        return calculationTypeRepository.save(DictionaryMapper.toCalculation1Type(dictionaryDTO));
    }

    @Override
    public DiscountType saveDiscountType(DictionaryDTO dictionaryDTO) {
        return discountTypeRepository.save(DictionaryMapper.toDiscountType(dictionaryDTO));
    }

    @Override
    public PropertyType savePropertyType(DictionaryDTO dictionaryDTO) {
        return propertyTypeRepository.save(DictionaryMapper.toPropertyType(dictionaryDTO));
    }

    @Override
    public ServiceType saveServiceType(DictionaryDTO dictionaryDTO) {
        return serviceTypeRepository.save(DictionaryMapper.toServiceType(dictionaryDTO));
    }

    @Override
    public Unit saveUnit(DictionaryDTO dictionaryDTO) {
        return unitRepository.save(DictionaryMapper.toUnit(dictionaryDTO));
    }

    @Override
    public void delete(DictionaryDTO dictionaryDTO, String tableName) {
        switch (tableName) {
            case ("CALCULATION_TYPE"):
                calculationTypeRepository.delete(DictionaryMapper.toCalculation1Type(dictionaryDTO));
                break;
            case ("DISCOUNT_TYPE"):
                discountTypeRepository.delete(DictionaryMapper.toDiscountType(dictionaryDTO));
                break;
            case ("PROPERTY_TYPE"):
                propertyTypeRepository.delete(DictionaryMapper.toPropertyType(dictionaryDTO));
                break;
            case ("SERVICE_TYPE"):
                serviceTypeRepository.delete(DictionaryMapper.toServiceType(dictionaryDTO));
                break;
            case ("STATUS_CODE"):
                statusCodeRepository.delete(DictionaryMapper.toStatusCode(dictionaryDTO));
                break;
            case ("UNITS"):
                unitRepository.delete(DictionaryMapper.toUnit(dictionaryDTO));
                break;
        }
    }


    @Override
    public Unit getUnitByCode(String code) {
        return unitRepository.getById(code);
    }

    @Override
    public Unit findUnitByCode(String code) {
        Optional<Unit> optionalUnit = unitRepository.findByCode(code);
        if (optionalUnit.isPresent()) {
            return optionalUnit.get();
        } else {
            throw new EntityNotFoundException(CalculationType.class.getSimpleName(), code);
        }
    }

    @Override
    public List<Unit> getAllUnits() {
        return unitRepository.findAll();
    }

    @Override
    public List<Dictionary> findAll() {
        return dictionaryRepository.findAll();
    }

}
