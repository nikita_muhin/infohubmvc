package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.PropertyDTO;
import com.infohubmvc.application.controller.mapper.PropertyMapper;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.Property;
import com.infohubmvc.application.data.entity.PropertyType;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.PropertyRepository;
import com.infohubmvc.application.security.UserDetailsImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PropertyServiceImpl extends BaseService implements PropertyService {

    private final PropertyRepository propertyRepository;
    private final HouseService houseService;
    private final DictionaryService dictionaryService;

    public PropertyServiceImpl(UserService userService,
                               PropertyRepository propertyRepository,
                               HouseService houseService,
                               DictionaryService dictionaryService) {
        super(userService);
        this.propertyRepository = propertyRepository;
        this.houseService = houseService;
        this.dictionaryService = dictionaryService;
    }

    @Override
    public Property getPropertyById(Long propertyId) {
        return propertyRepository.getById(propertyId);
    }

    @Override
    public List<Property> getPropertiesByUserId(Long userId) {
        return propertyRepository.findAllByUserId(userId);
    }

    @Override
    public List<Property> getAll() {
        return propertyRepository.findAll();
    }

    @Override
    public Page getProperties(Pageable pageable) {
        UserDetailsImpl userDetails = getLoggedInUser();
        //return propertyRepository.findAll(pageable);
        return propertyRepository.findAllByUserId(userDetails.getId(), pageable);
    }

    @Override
    public Page getPropertiesByUserId(Long userId, Pageable pageable) {
        return propertyRepository.findAllByUserId(userId, pageable);
    }

    @Override
    public Page getPropertiesByHouseId(Long houseId, Pageable pageable) {
        return propertyRepository.findAllByHouseId(houseId, pageable);
    }

    @Override
    public List<Property> findAllPropertiesByHouseId(House house) {
        if(house != null) {
            return propertyRepository.findAllByHouseId(house.getId());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Property saveProperty(Property property) {
        return propertyRepository.save(property);
    }

    @Override
    public Property addProperty(PropertyDTO propertyDTO) {
        User user = getUserById(propertyDTO.getTellerId());
        House house = houseService.getHouseById(propertyDTO.getHouseId());
        PropertyType propertyType = dictionaryService.getPropertyTypeByCode(propertyDTO.getPropertyType());
        return propertyRepository.save(PropertyMapper.toProperty(propertyDTO, user, house, propertyType));
    }

    @Override
    public Property updateProperty(PropertyDTO propertyDTO) {
        User user = getUserById(propertyDTO.getTellerId());
        if (propertyDTO.getId() == null) {
            throw new RuntimeException("Condominium ID is null!");
        }
        House house = houseService.getHouseById(propertyDTO.getHouseId());
        PropertyType propertyType = dictionaryService.getPropertyTypeByCode(propertyDTO.getPropertyType());
        return propertyRepository.save(PropertyMapper.toPropertyForUpdate(propertyDTO, user, house, propertyType));
    }

    @Override
    public void deleteProperty(Property property) {
        propertyRepository.delete(property);
    }


}
