package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.ServiceDTO;
import com.infohubmvc.application.controller.mapper.ServiceMapper;
import com.infohubmvc.application.data.entity.Service;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.ServiceRepository;

import java.util.List;

@org.springframework.stereotype.Service
public class ServiceServiceImpl extends BaseService implements ServiceService {

    private final ServiceRepository serviceRepository;

    public ServiceServiceImpl(UserService userService,
                              ServiceRepository serviceRepository) {
        super(userService);
        this.serviceRepository = serviceRepository;
    }

    @Override
    public Service getServiceById(Long id) {
        return serviceRepository.getById(id);
    }

    @Override
    public Service addService(ServiceDTO serviceDTO) {
        User user = getUserById(serviceDTO.getTellerId());
        return ServiceMapper.toService(serviceDTO, user);
    }

    @Override
    public List<Service> findAll() {
        return serviceRepository.findAll();
    }

    @Override
    public Service save(Service service) {
        return serviceRepository.save(service);
    }

    @Override
    public void delete(Service service) {
        serviceRepository.delete(service);
    }

}
