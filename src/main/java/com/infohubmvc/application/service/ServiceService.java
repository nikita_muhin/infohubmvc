package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.ServiceDTO;
import com.infohubmvc.application.data.entity.Service;
import com.infohubmvc.application.data.entity.User;

import java.util.List;

public interface ServiceService {
    Service getServiceById(Long id);

    Service addService(ServiceDTO serviceDTO);

    User getUserById(Long id);

    List<Service> findAll();

    Service save(Service service);

    void delete(Service service);
}
