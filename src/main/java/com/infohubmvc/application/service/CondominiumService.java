package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.CondominiumDTO;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface CondominiumService {
    Condominium getCondominiumById(Long id);

    Condominium findCondominiumByName(String name);

    Page<Condominium> getAll(Pageable pageable);

    Page<Condominium> getAllByUserId(Long userId, Pageable pageable);

    List<Condominium> getAll();
    Set<Condominium> findCondominiumByUserId(Long userId);

    Page<Condominium> getAllByUserName(String userName, Pageable pageable);

    Condominium createCondominium(CondominiumDTO condominiumDTO);

    Condominium saveCondominium(Condominium condominium);

    void deleteCondominium(Condominium condominium);

    Condominium updateCondominium(CondominiumDTO condominiumDTO);
}
