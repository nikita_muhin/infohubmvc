package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.InvoiceDTO;
import com.infohubmvc.application.data.entity.Invoice;

import java.util.List;

public interface InvoiceService {
    Invoice getInvoiceById(Long id);

    List<Invoice> findAll();

    void delete(Invoice invoice);

    Invoice save(Invoice invoice);

    List<Invoice> getAllByUserId(Long userId);

    List<Invoice> getAllByCondominiumId(Long condominiumId);

    Invoice addinvoice(InvoiceDTO invoiceDTO);
}
