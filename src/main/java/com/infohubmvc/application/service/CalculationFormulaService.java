package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.CalculationFormulaDTO;
import com.infohubmvc.application.data.entity.CalculationFormula;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CalculationFormulaService {

    List<CalculationFormula> findAll();

    void delete(CalculationFormula calculationFormula);

    CalculationFormula getById(Long id);

    CalculationFormula getByCode(String code);

    CalculationFormula save(CalculationFormula calculationFormula);

    List<CalculationFormula> getAllByCalculationType(String calculationType);

    Page<CalculationFormula> getAllByCalculationType(String calculationType, Pageable pageable);

    Page<CalculationFormula> getAllCalculationFormulas(Pageable pageable);

    List<CalculationFormula> getAllByUserId(Long userId);

    CalculationFormula addCalculationFormula(CalculationFormulaDTO calculationFormulaDTO);
}
