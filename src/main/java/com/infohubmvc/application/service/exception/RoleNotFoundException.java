package com.infohubmvc.application.service.exception;

public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException(String role) {
        super("Role not found by Name: " + role);
    }
}
