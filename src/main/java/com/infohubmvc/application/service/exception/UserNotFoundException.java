package com.infohubmvc.application.service.exception;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String firstName, String lastName) {
        super("User not found by firstName: " + firstName + " and lastName: " + lastName);
    }
}
