package com.infohubmvc.application.service.exception;

public class DiscountNotFoundException extends RuntimeException {
    public DiscountNotFoundException(String code) {
        super("Discount not found by discount_code: " + code);
    }
}
