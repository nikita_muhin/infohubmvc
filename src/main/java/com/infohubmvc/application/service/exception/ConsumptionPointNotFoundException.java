package com.infohubmvc.application.service.exception;

import com.infohubmvc.application.data.entity.ServiceType;

public class ConsumptionPointNotFoundException extends RuntimeException {
    public ConsumptionPointNotFoundException(ServiceType serviceType) {
        super("ConsumptionPoint not found by serviceType: " + serviceType);
    }
}
