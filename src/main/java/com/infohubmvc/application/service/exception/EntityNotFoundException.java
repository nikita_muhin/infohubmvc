package com.infohubmvc.application.service.exception;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String entityType, String entityName) {
        super("Entity : " + entityType + " for: " + entityName + " not found!");
    }
}
