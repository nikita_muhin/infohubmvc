package com.infohubmvc.application.service.exception;

public class CondominiumNotFoundException extends RuntimeException {
    public CondominiumNotFoundException(String name) {
        super("Condominium not found by name: " + name);
    }
}
