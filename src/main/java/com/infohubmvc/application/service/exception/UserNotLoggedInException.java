package com.infohubmvc.application.service.exception;

public class UserNotLoggedInException extends RuntimeException {
    public UserNotLoggedInException(String name) {
        super("User not logged in: " + name);
    }
}
