package com.infohubmvc.application.service.exception;

public class CalculationFormulaNotFoundException extends RuntimeException {
    public CalculationFormulaNotFoundException(String code) {
        super("Calculation formula not found by discount_code: " + code);
    }
}
