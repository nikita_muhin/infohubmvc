package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.InvoiceDTO;
import com.infohubmvc.application.controller.mapper.InvoiceMapper;
import com.infohubmvc.application.data.entity.Beneficiary;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.InvoiceRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImpl extends BaseService implements InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final CondominiumService condominiumService;
    private final BeneficiaryService beneficiaryService;

    public InvoiceServiceImpl(UserService userService,
                              InvoiceRepository invoiceRepository,
                              CondominiumService condominiumService,
                              BeneficiaryService beneficiaryService) {
        super(userService);
        this.invoiceRepository = invoiceRepository;
        this.condominiumService = condominiumService;
        this.beneficiaryService = beneficiaryService;
    }

    @Override
    public Invoice getInvoiceById(Long id) {
        return invoiceRepository.getById(id);
    }

    @Override
    public List<Invoice> findAll() {
        return invoiceRepository.findAll();
    }

    @Override
    public void delete(Invoice invoice) {
        invoiceRepository.delete(invoice);
    }

    @Override
    public Invoice save(Invoice invoice) {
        return invoiceRepository.save(invoice);
    }

    @Override
    public List<Invoice> getAllByUserId(Long userId) {
        return invoiceRepository.findAllByUserId(userId);
    }

    @Override
    public List<Invoice> getAllByCondominiumId(Long condominiumId) {
        return invoiceRepository.findAllByCondominiumId(condominiumId);
    }

    @Override
    public Invoice addinvoice(InvoiceDTO invoiceDTO) {
        User user = getUserById(invoiceDTO.getTellerId());
        Condominium condominium = condominiumService.getCondominiumById(invoiceDTO.getCondominiumId());
        Beneficiary beneficiary = beneficiaryService.getBeneficiaryById(invoiceDTO.getBeneficiaryId());
        return invoiceRepository.save(InvoiceMapper.toInvoice(invoiceDTO, condominium, beneficiary, user));
    }

}
