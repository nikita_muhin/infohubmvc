package com.infohubmvc.application.service;


import com.infohubmvc.application.controller.dto.CalculationFormulaDTO;
import com.infohubmvc.application.controller.mapper.CalculationFormulaMapper;
import com.infohubmvc.application.data.entity.CalculationFormula;
import com.infohubmvc.application.data.entity.CalculationType;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.CalculationFormulaRepository;
import com.infohubmvc.application.service.exception.CalculationFormulaNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalculationFormulaServiceImpl extends BaseService implements CalculationFormulaService {

    private final CalculationFormulaRepository calculationFormulaRepository;
    private final DictionaryService dictionaryService;

    public CalculationFormulaServiceImpl(UserService userService,
                                         CalculationFormulaRepository calculationFormulaRepository,
                                         UserService userService1,
                                         DictionaryService dictionaryService) {
        super(userService);
        this.calculationFormulaRepository = calculationFormulaRepository;
        this.dictionaryService = dictionaryService;
    }

    @Override
    public List<CalculationFormula> findAll() {
        return calculationFormulaRepository.findAll();
    }

    @Override
    public void delete(CalculationFormula calculationFormula) {
        calculationFormulaRepository.delete(calculationFormula);
    }

    @Override
    public CalculationFormula getById(Long id) {
        return calculationFormulaRepository.getById(id);
    }

    @Override
    public CalculationFormula getByCode(String code) {
        return calculationFormulaRepository.getByCode(code)
                .orElseThrow(() -> new CalculationFormulaNotFoundException(code));
    }

    @Override
    public CalculationFormula save(CalculationFormula calculationFormula) {
        return calculationFormulaRepository.save(calculationFormula);
    }

    @Override
    public List<CalculationFormula> getAllByCalculationType(String calculationType) {
        return calculationFormulaRepository.findAllByCalculationType(calculationType);
    }

    @Override
    public Page<CalculationFormula> getAllByCalculationType(String calculationType, Pageable pageable) {
        return calculationFormulaRepository.findAllByCalculationType(calculationType, pageable);
    }

    @Override
    public Page<CalculationFormula> getAllCalculationFormulas(Pageable pageable) {
        return calculationFormulaRepository.findAll(pageable);
    }

    @Override
    public List<CalculationFormula> getAllByUserId(Long userId) {
        return calculationFormulaRepository.findAllByUserId(userId);
    }

    @Override
    public CalculationFormula addCalculationFormula(CalculationFormulaDTO calculationFormulaDTO) {
        User user = getUserById(calculationFormulaDTO.getTellerId());
        CalculationType calculationType = dictionaryService.findCalculationTypeByCode(calculationFormulaDTO.getCalculationType());

        return calculationFormulaRepository.save(CalculationFormulaMapper.toCalculationFormula(calculationFormulaDTO, user, calculationType));
    }

}
