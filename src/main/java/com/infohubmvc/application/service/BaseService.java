package com.infohubmvc.application.service;

import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.security.UserDetailsImpl;
import com.infohubmvc.application.service.exception.UserNotLoggedInException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

//@Service
@RequiredArgsConstructor
public class BaseService {
    private final UserService userService;

    public UserDetailsImpl getLoggedInUser() {
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof String) {
            throw new UserNotLoggedInException(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        }

        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public User getUserById(Long id) {
        return userService.getUserById(id);
    }

    public User getUserByName(String userName) {
        return userService.getUserByName(userName);
    }

    public Optional<User> getByUsername(String username) {
        return userService.getByUsername(username);
    }

}
