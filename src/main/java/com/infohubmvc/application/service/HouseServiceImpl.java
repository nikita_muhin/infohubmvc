package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.HouseDTO;
import com.infohubmvc.application.controller.mapper.HouseMapper;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.StatusCode;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.HouseRepository;
import com.infohubmvc.application.repositories.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class HouseServiceImpl extends BaseService implements HouseService {

    private final HouseRepository houseRepository;

    private final UserRepository userRepository;
    private final CondominiumService condominiumService;
    private final DictionaryService dictionaryService;

    public HouseServiceImpl(UserService userService,
                            HouseRepository houseRepository,
                            UserRepository userRepository, CondominiumService condominiumService,
                            DictionaryService dictionaryService) {
        super(userService);
        this.houseRepository = houseRepository;
        this.userRepository = userRepository;
        this.condominiumService = condominiumService;
        this.dictionaryService = dictionaryService;
    }

    @Override
    public House getHouseById(Long id) {
        return houseRepository.getById(id);
    }

    @Override
    public Page<House> getAll(Pageable pageable) {
        //UserDetailsImpl user  = getLoggedInUser();
        return houseRepository.findAll(pageable);
    }

    @Override
    public List<House> getAll() {
        return houseRepository.findAll();
    }

    @Override
    public List<House> findAllByCondominiums(Set<Condominium> condominiums) {
        List<House> houseList = new ArrayList<>();
        for(Condominium condominium: condominiums){
            houseList.addAll(houseRepository.findAllByCondominiumId(condominium.getId()));
        }
        return houseList;
    }

    @Override
    public List<House> findAllByCondominium(Condominium condominium) {
        if(condominium == null) {
            return new ArrayList<>();
        } else {
            return houseRepository.findAllByCondominiumId(condominium.getId());
        }
    }
    @Override
    public Page<House> getAllByUserId(Long userId, Pageable pageable) {
        return houseRepository.findAllByUserId(userId, pageable);
    }

    @Override
    public Page<House> getAllByCondominiumId(Long condominiumId, Pageable pageable) {
        return houseRepository.findAllByCondominiumId(condominiumId, pageable);
    }

    @Override
    public House addHouse(HouseDTO houseDTO) {
        User user = userRepository.findByUsername("nikita07").get(); // must be current users
        Condominium condominium = condominiumService.getCondominiumById(houseDTO.getCondominiumId());
        StatusCode statusCode = dictionaryService.getStatusCodeByCode(houseDTO.getStatus());
        House house = HouseMapper.toHouse(houseDTO, condominium, user, statusCode);
        return houseRepository.save(house);
    }

    @Override
    public House updateHouse(HouseDTO houseDTO) {
        User user = userRepository.findByUsername("nikita07").get();// must be current users
        Condominium condominium = condominiumService.getCondominiumById(houseDTO.getCondominiumId());
        StatusCode statusCode = dictionaryService.getStatusCodeByCode(houseDTO.getStatus());
        return houseRepository.save(HouseMapper.toHouseForEdit(houseDTO, condominium, user, statusCode));
    }

    @Override
    public House saveHouse(House house) {
        return houseRepository.save(house);
    }

    @Override
    public void deleteHouse(House house) {
        houseRepository.delete(house);
    }

}
