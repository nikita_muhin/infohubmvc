package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.DiscountDTO;
import com.infohubmvc.application.controller.mapper.DiscountMapper;
import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.repositories.DiscountRepository;
import com.infohubmvc.application.repositories.DiscountTypeRepository;
import com.infohubmvc.application.security.UserDetailsImpl;
import com.infohubmvc.application.service.exception.DiscountNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DiscountServiceImpl extends BaseService implements DiscountService {

    private final DiscountRepository discountRepository;
    private final DiscountTypeRepository discountTypeRepository;
    private final DictionaryService dictionaryService;

    public DiscountServiceImpl(UserService userService,
                               DiscountRepository discountRepository,
                               DiscountTypeRepository discountTypeRepository,
                               DictionaryService dictionaryService) {
        super(userService);
        this.discountRepository = discountRepository;
        this.discountTypeRepository = discountTypeRepository;
        this.dictionaryService = dictionaryService;
    }

    @Override
    public Discount getById(Long id) {
        return discountRepository.getById(id);
    }

    @Override
    public Discount getByCode(String code) {

        return discountRepository.getByDiscountType(LocalDate.now(), code)
                .orElseThrow(() -> new DiscountNotFoundException(code));
    }

    @Override
    public List<Discount> getAllByUserId(Long userId) {
        return discountRepository.findAllByUserId(userId);
    }

    @Override
    public Discount addDiscount(DiscountDTO discountDTO) {
        User user = getUserById(discountDTO.getTellerId());
        DiscountType discountType = discountTypeRepository.getById(discountDTO.getDiscountType());
        Unit unit = dictionaryService.getUnitByCode(discountDTO.getDiscountUnit());
        return discountRepository.save(DiscountMapper.toDiscount(discountDTO, user, discountType, unit));
    }

    @Override
    public Discount updateDiscount(DiscountDTO discountDTO) {
        User user = getUserById(discountDTO.getTellerId());
        DiscountType discountType = discountTypeRepository.getById(discountDTO.getDiscountType());
        Unit unit = dictionaryService.getUnitByCode(discountDTO.getDiscountUnit());
        return discountRepository.save(DiscountMapper.toDiscount(discountDTO, user, discountType, unit));
    }

    @Override
    public List<Discount> getAllActive() {
        return discountRepository.findAllActiveForDate(LocalDate.now());
    }

    @Override
    public Page<Discount> getAll(DiscountStatus status, Pageable pageable) {
        UserDetailsImpl user = getLoggedInUser();
        if (DiscountStatus.ACTIVE == status) {
            return discountRepository.findAllActiveForDate(LocalDate.now(), pageable);
        } else if (DiscountStatus.EXPIRED == status) {
            return discountRepository.findAllExpiredForDate(LocalDate.now(), pageable);
        } else if (DiscountStatus.SCHEDULED == status) {
            return discountRepository.findAllScheduledForDate(LocalDate.now(), pageable);
        } else if (DiscountStatus.CLOSED == status) {
            return discountRepository.findAllDisabledForDate(pageable);
        }
        return discountRepository.findAllActiveForDate(LocalDate.now(), pageable);
    }

    @Override
    public List<Discount> findAll() {
        return discountRepository.findAll();
    }

    @Override
    public Discount save(Discount discount) {
        return discountRepository.save(discount);
    }

    @Override
    public void deleteDiscount(Discount discount) {
        discountRepository.delete(discount);
    }

}

