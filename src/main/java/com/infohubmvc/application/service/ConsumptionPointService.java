package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.ConsumptionPointDTO;
import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.infohubmvc.application.data.entity.ServiceType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ConsumptionPointService {
    ConsumptionPoint getById(Long id);

    ConsumptionPoint getByServiceType(ServiceType serviceType);

    List<ConsumptionPoint> getAllByParentId(Long parentId);

    List<ConsumptionPoint> getAllByCondominiumId(Long condominiumId);

    List<ConsumptionPoint> getAllByHouseId(Long houseId);

    List<ConsumptionPoint> getAllByPropertyId(Long propertyId);

    ConsumptionPoint addConsumptionPoint(ConsumptionPointDTO consumptionPointDTO);

    Page getAllForUser(Long userId, Pageable pageable);

    List<ConsumptionPoint> findAll();

    ConsumptionPoint save(ConsumptionPoint consumptionPoint);

    void delete(ConsumptionPoint consumptionPoint);
}
