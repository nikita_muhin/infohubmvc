package com.infohubmvc.application.service;

import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.repositories.PropertyRepository;
import com.infohubmvc.application.repositories.UserRightRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class UserRightServerImpl implements UserRightServer {

    private UserRightRepository userRightRepository;

    private PropertyRepository propertyRepository;

    @Override
    public void saveUserRights(Condominium condominium, User user) {
        condominium.getHouses().forEach(house -> userRightRepository.save(createUserRight(condominium, user, house)));
    }

    @Override
    public void deleteUserRights(Condominium condominium, User user) {
        userRightRepository.deleteByUserIdAndCondominiumId(user.getId(), condominium.getId());
    }

    @Override
    public void deleteUserRightsByHouse(Condominium condominium, User user, House house) {
        userRightRepository.deleteByUserIdAndCondominiumIdAndHouseId(user.getId(), condominium.getId(), house.getId());
    }

    @Override
    public List<UserRight> findAllByUserAndCondominium(Condominium condominium, User user) {
        if (condominium == null) {
             return Collections.emptyList();
        } else {
            return userRightRepository.findAllByUserIdAndCondominiumId(user.getId(), condominium.getId());
        }
    }

    private UserRight createUserRight(Condominium condominium, User user, House house) {
        UserRight userRight = new UserRight();
        userRight.setUser(user);
        userRight.setHouse(house);
        userRight.setCondominium(condominium);
        userRight.setTeller(condominium.getUser());
        userRight.setProperty(propertyRepository.findAll().stream().findFirst().get());
        return userRight;
    }
}
