package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.CondominiumDTO;
import com.infohubmvc.application.controller.mapper.CondominiumMapper;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.CondominiumRepository;
import com.infohubmvc.application.repositories.UserRightRepository;
import com.infohubmvc.application.security.UserDetailsImpl;
import com.infohubmvc.application.service.exception.CondominiumNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CondominiumServiceImpl extends BaseService implements CondominiumService {

    private final CondominiumRepository condominiumRepository;

    public final UserRightRepository userRightRepository;

    public CondominiumServiceImpl(UserService userService,
                                  CondominiumRepository condominiumRepository,
                                  UserRightRepository userRightRepository) {
        super(userService);
        this.condominiumRepository = condominiumRepository;
        this.userRightRepository = userRightRepository;
    }

    @Override
    public Condominium getCondominiumById(Long id) {
        return condominiumRepository.getById(id);
    }

    @Override
    public Condominium findCondominiumByName(String name) {
        return condominiumRepository.getByName(name).
                orElseThrow((() -> new CondominiumNotFoundException(name)));
    }

    @Override
    public Page<Condominium> getAll(Pageable pageable) {
        UserDetailsImpl user = getLoggedInUser();
        //return condominiumRepository.findAll(pageable);
        return condominiumRepository.findAllByUserId(user.getId(), pageable);

    }

    @Override
    public Page<Condominium> getAllByUserId(Long userId, Pageable pageable) {
        return condominiumRepository.findAllByUserId(userId, pageable);
    }

    @Override
    public List<Condominium> getAll() {
        return condominiumRepository.findAll();
    }

    @Override
    public Set<Condominium> findCondominiumByUserId(Long id) {
        Set<Condominium> condominiumSet = new HashSet<>();
        userRightRepository.findAllByUserId(id).forEach(e -> condominiumSet.add(e.getCondominium()));
        return condominiumSet;
    }

    @Override
    public Page<Condominium> getAllByUserName(String userName, Pageable pageable) {
        User user = getUserByName(userName);
        if (user != null) {
            return condominiumRepository.findAllByUserId(user.getId(), pageable);
        } else {
            return Page.empty(pageable);
        }
    }

    @Override
    public Condominium createCondominium(CondominiumDTO condominiumDTO) {
        User user = getUserById(condominiumDTO.getTellerId());
        return condominiumRepository.save(CondominiumMapper.toCondominium(condominiumDTO, user));
    }

    @Override
    public Condominium saveCondominium(Condominium condominium) {
        return condominiumRepository.save(condominium);
    }

    @Override
    public void deleteCondominium(Condominium condominium) {
         condominiumRepository.delete(condominium);
    }

    @Override
    public Condominium updateCondominium(CondominiumDTO condominiumDTO) {
        User user = getUserById(condominiumDTO.getTellerId());
        if (condominiumDTO.getId() == null) {
            throw new RuntimeException("Condominium ID is null!");
        }
        return condominiumRepository.save(CondominiumMapper.toCondominiumForEdit(condominiumDTO, user));
    }

}
