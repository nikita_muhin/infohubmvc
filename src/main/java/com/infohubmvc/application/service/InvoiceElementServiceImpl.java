package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.InvoiceElementDTO;
import com.infohubmvc.application.controller.mapper.InvoiceElementMapper;
import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.InvoiceElement;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.InvoiceElementRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceElementServiceImpl extends BaseService implements InvoiceElementService {

    private final InvoiceElementRepository invoiceElementRepository;
    private final InvoiceService invoiceService;
    private final ConsumptionPointService consumptionPointService;

    public InvoiceElementServiceImpl(UserService userService,
                                     InvoiceElementRepository invoiceElementRepository,
                                     InvoiceService invoiceService,
                                     ConsumptionPointService consumptionPointService) {
        super(userService);
        this.invoiceElementRepository = invoiceElementRepository;
        this.invoiceService = invoiceService;
        this.consumptionPointService = consumptionPointService;
    }

    @Override
    public List<InvoiceElement> findAllByInvoice(Invoice invoice) {
        return invoiceElementRepository.findAllByInvoiceId(invoice.getId());
    }

    @Override
    public InvoiceElement getById(Long id) {
        return invoiceElementRepository.getById(id);
    }

    @Override
    public void delete(InvoiceElement invoiceElement) {
        invoiceElementRepository.delete(invoiceElement);
    }

    @Override
    public InvoiceElement save(InvoiceElement invoiceElement) {
        return invoiceElementRepository.save(invoiceElement);
    }

    @Override
    public InvoiceElement addInvoiceElement(InvoiceElementDTO invoiceElementDTO) {
        Invoice invoice = invoiceService.getInvoiceById(invoiceElementDTO.getInvoiceId());
        ConsumptionPoint consumptionPoint = consumptionPointService.getById(invoiceElementDTO.getConsumptionPointId());
        User user = getUserById(invoiceElementDTO.getTellerId());
        InvoiceElement invoiceElement = InvoiceElementMapper.toInvoiceElement(invoiceElementDTO, invoice, consumptionPoint, user);
        return invoiceElementRepository.save(invoiceElement);
    }

}
