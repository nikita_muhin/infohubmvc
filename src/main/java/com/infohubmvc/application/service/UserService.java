package com.infohubmvc.application.service;

import com.infohubmvc.application.data.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User getUserById(Long id);

    User getUserByName(String name);

    void deleteUser(User user);

    User getUserByFirstAndLastName(String firstName, String lastName);

    Optional<User> getByUsername(String username);

    Page<User> getAll(Pageable pageable);

    List<User> getAll();

    User saveUser(User user);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
