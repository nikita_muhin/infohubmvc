package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.DictionaryDTO;
import com.infohubmvc.application.data.entity.*;

import java.util.List;

public interface DictionaryService {
    ServiceType getServiceTypeByCode(String code);

    ServiceType findServiceTypeByCode(String code);

    List<ServiceType> getAllServiceTypes();

    CalculationType getCalculationTypeByCode(String code);

    CalculationType findCalculationTypeByCode(String code);

    List<CalculationType> getAllCalculationTypes();

    PropertyType getPropertyTypeByCode(String code);

    PropertyType findPropertyTypeByCode(String code);

    List<PropertyType> getAllPropertyTypes();

    DiscountType getDiscountTypeByCode(String code);

    DiscountType findDiscountTypeByCode(String code);

    List<DiscountType> getAllDiscountTypes();

    StatusCode getStatusCodeByCode(String code);

    StatusCode findStatusCodeByCode(String code);

    List<StatusCode> getAllStatusCodes();

    StatusCode saveStatus(DictionaryDTO dictionaryDTO);

    CalculationType saveCalculationType(DictionaryDTO dictionaryDTO);

    DiscountType saveDiscountType(DictionaryDTO dictionaryDTO);

    PropertyType savePropertyType(DictionaryDTO dictionaryDTO);

    ServiceType saveServiceType(DictionaryDTO dictionaryDTO);

    Unit saveUnit(DictionaryDTO dictionaryDTO);

    void delete(DictionaryDTO dictionaryDTO, String tableName);

    Unit getUnitByCode(String code);

    Unit findUnitByCode(String code);

    List<Unit> getAllUnits();

    List<Dictionary> findAll();
}
