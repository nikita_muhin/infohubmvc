package com.infohubmvc.application.service;

import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.data.entity.UserRight;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRightServer {

    void saveUserRights(Condominium condominium, User user);
    @Transactional
    void deleteUserRights(Condominium condominium, User user);

    @Transactional
    void deleteUserRightsByHouse(Condominium condominium, User user, House house);
    List<UserRight> findAllByUserAndCondominium(Condominium condominium, User user);
}
