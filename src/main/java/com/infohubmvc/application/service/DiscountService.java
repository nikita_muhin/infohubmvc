package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.DiscountDTO;
import com.infohubmvc.application.data.entity.Discount;
import com.infohubmvc.application.data.entity.DiscountStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DiscountService {
    Discount getById(Long id);

    Discount getByCode(String code);

    List<Discount> getAllByUserId(Long userId);

    Discount addDiscount(DiscountDTO discountDTO);

    Discount updateDiscount(DiscountDTO discountDTO);

    List<Discount> getAllActive();

    Page<Discount> getAll(DiscountStatus status, Pageable pageable);
    List<Discount> findAll();
    Discount save(Discount discount);

    void deleteDiscount(Discount discount);
}
