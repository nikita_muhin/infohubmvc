package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.BeneficiaryDTO;
import com.infohubmvc.application.data.entity.Beneficiary;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.BeneficiaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeneficiaryServiceImpl extends BaseService implements BeneficiaryService {

    private final BeneficiaryRepository beneficiaryRepository;
    private final CondominiumService condominiumService;
    private final HouseService houseService;

    public BeneficiaryServiceImpl(UserService userService,
                                  BeneficiaryRepository beneficiaryRepository,
                                  CondominiumService condominiumService,
                                  HouseService houseService) {
        super(userService);
        this.beneficiaryRepository = beneficiaryRepository;
        this.condominiumService = condominiumService;
        this.houseService = houseService;
    }

    @Override
    public Beneficiary getBeneficiaryById(Long id) {
        return beneficiaryRepository.getById(id);
    }

    @Override
    public List<Beneficiary> findAll() {
        return beneficiaryRepository.findAll();
    }

    @Override
    public Beneficiary save(Beneficiary beneficiary) {
        return beneficiaryRepository.save(beneficiary);
    }

    @Override
    public void delete(Beneficiary beneficiary) {
       beneficiaryRepository.delete(beneficiary);
    }

    @Override
    public House addBeneficiary(BeneficiaryDTO beneficiaryDTO) {
        User user = getUserById(beneficiaryDTO.getTellerId());
        Condominium condominium = condominiumService.getCondominiumById(beneficiaryDTO.getCondominiumId());
        House house = houseService.getHouseById(beneficiaryDTO.getHouseId());

        //return beneficiaryRepository.save(BeneficiaryMapper.toBeneficiary(beneficiaryDTO, condominium, house, new com.proit.infohub.entity.Service(), user));
        return null;
    }

}
