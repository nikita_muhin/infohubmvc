package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.CondominiumAdministratorDTO;
import com.infohubmvc.application.controller.mapper.CondominiumAdministratorMapper;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.CondominiumAdministrator;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.repositories.CondominiumAdministratorRepository;
import org.springframework.stereotype.Service;

@Service
public class CondominiumAdministratorServiceImpl extends BaseService implements CondominiumAdministratorService {

    private final CondominiumAdministratorRepository condominiumAdministratorRepository;
    private final CondominiumService condominiumService;

    public CondominiumAdministratorServiceImpl(UserService userService,
                                               CondominiumAdministratorRepository condominiumAdministratorRepository,
                                               CondominiumService condominiumService) {
        super(userService);
        this.condominiumAdministratorRepository = condominiumAdministratorRepository;
        this.condominiumService = condominiumService;
    }

    @Override
    public CondominiumAdministrator getById(Long id) {
        return condominiumAdministratorRepository.getById(id);
    }

    @Override
    public CondominiumAdministrator addCondominiumAdministrator(CondominiumAdministratorDTO condominiumAdministratorDTO) {
        User user = getUserById(condominiumAdministratorDTO.getTellerId());
        Condominium condominium = condominiumService.getCondominiumById(condominiumAdministratorDTO.getCondominiumId());

        CondominiumAdministrator condominiumAdministrator =
                CondominiumAdministratorMapper.toCondominiumAdministrator(condominiumAdministratorDTO, condominium, user);

        return condominiumAdministratorRepository.save(condominiumAdministrator);
    }

}
