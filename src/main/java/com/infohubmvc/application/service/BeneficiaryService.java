package com.infohubmvc.application.service;

import com.infohubmvc.application.controller.dto.BeneficiaryDTO;
import com.infohubmvc.application.data.entity.Beneficiary;
import com.infohubmvc.application.data.entity.House;

import java.util.List;

public interface BeneficiaryService {
    Beneficiary getBeneficiaryById(Long id);

    List<Beneficiary> findAll();
    Beneficiary save(Beneficiary beneficiary);
    void delete(Beneficiary beneficiary);

    House addBeneficiary(BeneficiaryDTO beneficiaryDTO);
}
