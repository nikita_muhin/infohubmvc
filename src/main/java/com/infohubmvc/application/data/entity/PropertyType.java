package com.infohubmvc.application.data.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PROPERTY_TYPE")
public class PropertyType {

  @Id
  private String code;
  private String description;

}
