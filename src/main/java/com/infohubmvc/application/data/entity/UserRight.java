package com.infohubmvc.application.data.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USER_ACCESS_RIGHTS")
public class UserRight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne()
    @JoinColumn(name = "condominium_id")
    private Condominium condominium;

    @ManyToOne()
    @JoinColumn(name = "house_id")
    private House house;

    @ManyToOne()
    @JoinColumn(name = "property_id")
    private Property property;

    @CreationTimestamp
    private LocalDateTime dateCreated;

    @ManyToOne()
    @JoinColumn(name = "teller_id")
    private User teller;

}