package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.Discount;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class DiscountGrid extends Grid<Discount> {

    public DiscountGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getDiscountType().getDescription()).setHeader("Type");
        addColumn(Discount::getDescription).setHeader("Description");
        addColumn(Discount::getDiscountValue).setHeader("Value");
        addColumn(e -> e.getDiscountUnit().getDescription()).setHeader("Unit");
        addColumn(Discount::getDateFrom).setHeader("Date from");
        addColumn(Discount::getDateTo).setHeader("Date to");
        addColumn(Discount::getActive).setHeader("Active");
        addColumn(Discount::getDateCreated).setHeader("Date created");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName()).setHeader("User");
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }

}
