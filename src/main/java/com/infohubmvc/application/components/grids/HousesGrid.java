package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.House;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class HousesGrid extends Grid<House> {

    public HousesGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getCondominium().getName()).setHeader("Condominium").setAutoWidth(true);
        addColumn(House::getName).setHeader("Name").setAutoWidth(true);
        addColumn(House::getHouseAddress).setHeader("Address").setAutoWidth(true);
        addColumn(House::getNumberBlocks).setHeader("Block Nr").setAutoWidth(true);
        addColumn(House::getNumberFloors).setHeader("Floors").setAutoWidth(true);
        addColumn(House::getNumberStairs).setHeader("Stairs").setAutoWidth(true);
        addColumn(House::getTotalArea).setHeader("Total Area").setAutoWidth(true);
        addColumn(House::getYearBuilt).setHeader("Year").setAutoWidth(true);
        addColumn(House::getHasLift).setHeader("Has Lift").setAutoWidth(true);
        addColumn(e -> e.getProperty().size()).setHeader("Properties").setAutoWidth(true);
        addColumn(e -> e.getConsumptionPoints().size()).setHeader("Consumption Points").setAutoWidth(true);
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }
}
