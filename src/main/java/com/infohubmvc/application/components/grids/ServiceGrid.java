package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.Service;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class ServiceGrid extends Grid<Service> {

    public ServiceGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(Service::getName).setHeader("Name");
        addColumn(Service::getContractNumber).setHeader("Contract number");
        addColumn(e -> e.getServiceType().getDescription()).setHeader("Service type");
        addColumn(Service::getTarifUnit).setHeader("Tarif unit");
        addColumn(Service::getTarif).setHeader("Tarif");
        addColumn(Service::getDateFrom).setHeader("From");
        addColumn(Service::getDateTo).setHeader("To");
        addColumn(Service::getDateCreated).setHeader("Create");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName()).setHeader("User");
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }


}
