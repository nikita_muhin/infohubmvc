package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.User;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

import java.util.stream.Collectors;

public class UserGrid extends Grid<User> {

    public UserGrid() {
       configureColumn();
    }

    private void configureColumn() {
        addColumn(User::getUsername).setHeader("Username").setAutoWidth(true);
        addColumn(User::getLastName).setHeader("Last Name").setAutoWidth(true);
        addColumn(User::getEmail).setHeader("Email").setAutoWidth(true);
        addColumn(User::getPhoneNumber).setHeader("Phone").setAutoWidth(true);
        addColumn(e -> e.getStatus().name()).setHeader("Status").setAutoWidth(true);
        addColumn(User::getDateCreated).setHeader("Created Date").setAutoWidth(true);
        addColumn(this::generateRoleString).setHeader("Roles").setAutoWidth(true);
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }

    private String generateRoleString(User user) {
        return user.getUserRoles().stream().map(e -> e.getRole().name()).collect(Collectors.joining(","));
    }

}
