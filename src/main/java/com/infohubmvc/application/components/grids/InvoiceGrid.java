package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.Invoice;
import com.vaadin.flow.component.grid.Grid;

public class InvoiceGrid extends Grid<Invoice> {

    public InvoiceGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getCondominium().getName()).setHeader("Condominium");
        addColumn(Invoice::getInvYear).setHeader("Year");
        addColumn(Invoice::getInvMonth).setHeader("Month");
        addColumn(e -> e.getBeneficiary().getCalculationCode()).setHeader("Beneficiary");
        addColumn(Invoice::getCalculatedAmount).setHeader("Calculated amount");
        addColumn(Invoice::getDebt).setHeader("Debt");
        addColumn(Invoice::getPenalty).setHeader("Penalty");
        addColumn(Invoice::getDateCreated).setHeader("Date create");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName());
    }
}
