package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.Condominium;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class CondominiumsGrid extends Grid<Condominium> {

    public CondominiumsGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(Condominium::getName).setHeader("Name");
        addColumn(Condominium::getRegistrationNumber).setHeader("Registration Number");
        addColumn(Condominium::getOfficeAddress).setHeader("Office address");
        addColumn(Condominium::getInfohubContractNr).setHeader("Contract NR");
        addColumn(Condominium::getStatus).setHeader("Status");
        addColumn(Condominium::getDateCreated).setHeader("Created Date");
        addColumn(this::userString).setHeader("User");
        addColumn(e -> e.getConsumptionPoints().size()).setHeader("Consumtions points");
        addColumn(e -> e.getHouses().size()).setHeader("Houses");
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }

    private String userString(Condominium condominium) {
        return condominium.getUser().getFirstName() + " " + condominium.getUser().getLastName();
    }
}
