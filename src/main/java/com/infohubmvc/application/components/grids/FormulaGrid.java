package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.CalculationFormula;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class FormulaGrid extends Grid<CalculationFormula> {

    public FormulaGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(CalculationFormula::getCode).setHeader("Code");
        addColumn(CalculationFormula::getDescription).setHeader("Description");
        addColumn(e -> e.getCalculationType().getDescription()).setHeader("Type");
        addColumn(CalculationFormula::getFormula).setHeader("Formula");
        addColumn(CalculationFormula::getDateFrom).setHeader("Data from");
        addColumn(CalculationFormula::getDateTo).setHeader("Data to");
        addColumn(CalculationFormula::getActive).setHeader("Active");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName()).setHeader("User");
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }
}
