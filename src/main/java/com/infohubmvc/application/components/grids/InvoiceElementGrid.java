package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.InvoiceElement;
import com.vaadin.flow.component.grid.Grid;

public class InvoiceElementGrid extends Grid<InvoiceElement> {

    public InvoiceElementGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getConsumptionPoint().getCounterNumber()).setHeader("Consumption point");
        addColumn(InvoiceElement::getDateFrom).setHeader("Date From");
        addColumn(InvoiceElement::getPayUntil).setHeader("Pay Until");
        addColumn(InvoiceElement::getIndiceStart).setHeader("Indice Start");
        addColumn(InvoiceElement::getIndiceEnd).setHeader("Indice End");
        addColumn(InvoiceElement::getUntCost).setHeader("Unt Cost");
        addColumn(InvoiceElement::getUnits).setHeader("Units");
        addColumn(InvoiceElement::getCalculatedAmount).setHeader("Calculated Amount");
        addColumn(InvoiceElement::getDebt).setHeader("Debt");
        addColumn(InvoiceElement::getPenalty).setHeader("Penalty");
        addColumn(InvoiceElement::getDateCreated).setHeader("Date Created");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName()).setHeader("User");
    }
}
