package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.vaadin.flow.component.grid.Grid;

public class ConsumptionPointGrid extends Grid<ConsumptionPoint> {

    public ConsumptionPointGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getCondominium().getName()).setHeader("Condominium");
        addColumn(e -> e.getHouse().getName()).setHeader("House");
        addColumn(e -> e.getProperty().getAddress()).setHeader("Property");
        addColumn(e -> e.getServiceType().getDescription()).setHeader("Service type");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName()).setHeader("User");
        addColumn(e -> e.getCalculationType().getDescription()).setHeader("Calculation type");
        addColumn(ConsumptionPoint::getDiscountCode).setHeader("Discount code");
        addColumn(ConsumptionPoint::getDiscountValue).setHeader("Discount value");
        addColumn(ConsumptionPoint::getCounterNumber).setHeader("Counter number");
        addColumn(ConsumptionPoint::getInitialCounterIndex).setHeader("Initial Counter Index");
        addColumn(ConsumptionPoint::getValidityDateCounter).setHeader("Validity Date Counter");
        addColumn(ConsumptionPoint::getDateCreated).setHeader("Date create");
    }
}
