package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.Property;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class PropertyGrid extends Grid<Property> {

    public PropertyGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getHouse().getName()).setHeader("House");
        addColumn(e -> e.getPropertyType().getDescription()).setHeader("Type");
        addColumn(this::generateName).setHeader("Name");
        addColumn(Property::getFloor).setHeader("Floor");
        addColumn(Property::getPhoneNumbers).setHeader("Telefon");
        addColumn(Property::getEmail).setHeader("Email");
        addColumn(Property::getNumberRooms).setHeader("Rooms");
        addColumn(Property::getArea).setHeader("Area");
        addColumn(e -> e.getConsumptionPoints().size()).setHeader("Consumption Points");
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }

    private String generateName(Property property) {
        return property.getFirstName() + " " + property.getLastName();
    }
}
