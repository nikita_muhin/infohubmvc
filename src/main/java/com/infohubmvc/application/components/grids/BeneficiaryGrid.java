package com.infohubmvc.application.components.grids;

import com.infohubmvc.application.data.entity.Beneficiary;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;

public class BeneficiaryGrid extends Grid<Beneficiary> {

    public BeneficiaryGrid() {
        configureColumn();
    }

    private void configureColumn() {
        addColumn(e -> e.getCondominium().getName()).setHeader("Condominium");
        addColumn(e -> e.getHouse().getName()).setHeader("House");
        addColumn(e -> e.getService().getName()).setHeader("Service");
        addColumn(Beneficiary::getServiceName).setHeader("Service name");
        addColumn(Beneficiary::getCalculationCode).setHeader("Calculation code");
        addColumn(Beneficiary::getTarif).setHeader("Tarif");
        addColumn(Beneficiary::getInitialCounterIndex).setHeader("Initial counter index");
        addColumn(Beneficiary::getDateCreated).setHeader("Date created");
        addColumn(e -> e.getUser().getFirstName() + " " + e.getUser().getLastName()).setHeader("User");
        addThemeVariants(GridVariant.LUMO_COLUMN_BORDERS);
        addThemeVariants(GridVariant.LUMO_COMPACT);
    }
}
