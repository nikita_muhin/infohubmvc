package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.IntegerToLongConverter;
import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.service.*;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;

public class ConsumptionPointDialog extends Dialog {

    private ConsumptionPointService consumptionPointService;
    private CondominiumService condominiumService;
    private UserService userService;
    private HouseService houseService;
    private PropertyService propertyService;
    private DictionaryService dictionaryService;

    private ComboBox<ConsumptionPoint> consumptionPointComboBox = new ComboBox<>("Parent");
    private ComboBox<House> houseComboBox = new ComboBox<>("House");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private ComboBox<Property> propertyComboBox = new ComboBox<>("Property");
    private ComboBox<ServiceType> serviceTypeComboBox = new ComboBox<>("Service type");
    private ComboBox<CalculationType> calculationTypeComboBox = new ComboBox<>("Calculation type");
    private ComboBox<Condominium> condominiumComboBox = new ComboBox<>("Condominium");
    private DatePicker validityDateCounterDatePicker = new DatePicker("Validity Date Counter");
    private IntegerField initialCounterIndexIntegerFiled = new IntegerField("Initial Counter Index");
    private IntegerField discountValueIntegerField = new IntegerField("Discount value");
    private TextField discountCodeTextField = new TextField("Discount code");
    private TextField counterNumberTextFiled = new TextField("Counter number");

    private Button saveButton;
    private FormLayout formLayout;
    private Binder<ConsumptionPoint> binder = new Binder<>(ConsumptionPoint.class);
    private boolean isEditable;

    public ConsumptionPointDialog(String header, ConsumptionPoint consumptionPoint, boolean isEditable, boolean isCreate) {
        consumptionPointService = WebApplicationUtils.get(ConsumptionPointService.class);
        houseService = WebApplicationUtils.get(HouseService.class);
        userService = WebApplicationUtils.get(UserService.class);
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        propertyService = WebApplicationUtils.get(PropertyService.class);
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("50%");
        configureCloseButton();
        configureSaveButton(isCreate);
        configureComponent(consumptionPoint);
        configureBinder(consumptionPoint);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(ConsumptionPoint consumptionPoint) {
        configureConsumptionPointComboBox(consumptionPoint);
        configureHouseComboBox(consumptionPoint);
        configurePropertyComboBox(consumptionPoint);
        configureUserComboBox(consumptionPoint);
        configureServiceTypeComboBox(consumptionPoint);
        configureCalculationTypeComboBox(consumptionPoint);
        configureCondominiumComboBox(consumptionPoint);
        configureValidityDateCounterDatePicker();
        configureInitialCounterIndexIntegerFiled();
        configureDiscountValueIntegerField();
        configureDiscountCodeTextField();
        configureDiscountCodeTextField();
        configureCounterNumberTextFiled();
    }

    private void configureConsumptionPointComboBox (ConsumptionPoint consumptionPoint) {
        consumptionPointComboBox.setItems(consumptionPointService.findAll());
        consumptionPointComboBox.setItemLabelGenerator(ConsumptionPoint::getCounterNumber);
        consumptionPointComboBox.setValue(null);
        consumptionPointComboBox.setReadOnly(!isEditable);
        consumptionPointComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureHouseComboBox (ConsumptionPoint consumptionPoint) {
        houseComboBox.setItems(houseService.getAll());
        houseComboBox.setItemLabelGenerator(House::getName);
        houseComboBox.setValue(consumptionPoint.getHouse());
        houseComboBox.setReadOnly(!isEditable);
        houseComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureUserComboBox (ConsumptionPoint consumptionPoint) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(consumptionPoint.getUser());
        userComboBox.setReadOnly(!isEditable);
        userComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configurePropertyComboBox (ConsumptionPoint consumptionPoint) {
        propertyComboBox.setItems(propertyService.getAll());
        propertyComboBox.setItemLabelGenerator(Property::getAddress);
        propertyComboBox.setValue(consumptionPoint.getProperty());
        propertyComboBox.setReadOnly(!isEditable);
        propertyComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureServiceTypeComboBox (ConsumptionPoint consumptionPoint) {
        serviceTypeComboBox.setItems(dictionaryService.getAllServiceTypes());
        serviceTypeComboBox.setItemLabelGenerator(ServiceType::getDescription);
        serviceTypeComboBox.setValue(consumptionPoint.getServiceType());
        serviceTypeComboBox.setReadOnly(!isEditable);
        serviceTypeComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureCalculationTypeComboBox (ConsumptionPoint consumptionPoint) {
        calculationTypeComboBox.setItems(dictionaryService.getAllCalculationTypes());
        calculationTypeComboBox.setItemLabelGenerator(CalculationType::getDescription);
        calculationTypeComboBox.setValue(consumptionPoint.getCalculationType());
        calculationTypeComboBox.setReadOnly(!isEditable);
        calculationTypeComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureCondominiumComboBox (ConsumptionPoint consumptionPoint) {
        condominiumComboBox.setItems(condominiumService.getAll());
        condominiumComboBox.setItemLabelGenerator(Condominium::getName);
        condominiumComboBox.setValue(consumptionPoint.getCondominium());
        condominiumComboBox.setReadOnly(!isEditable);
        condominiumComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureValidityDateCounterDatePicker () {
        validityDateCounterDatePicker.setReadOnly(!isEditable);
        validityDateCounterDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureInitialCounterIndexIntegerFiled () {
        initialCounterIndexIntegerFiled.setReadOnly(!isEditable);
        initialCounterIndexIntegerFiled.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configureDiscountValueIntegerField () {
        discountValueIntegerField.setReadOnly(!isEditable);
        discountValueIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configureDiscountCodeTextField () {
        discountCodeTextField.setReadOnly(!isEditable);
        discountCodeTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }
    private void configureCounterNumberTextFiled () {
        counterNumberTextFiled.setReadOnly(!isEditable);
        counterNumberTextFiled.addValueChangeListener(this::setEnableSaveButtonForText);
    }


    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(consumptionPointComboBox, userComboBox, houseComboBox, propertyComboBox,
                serviceTypeComboBox, calculationTypeComboBox, condominiumComboBox, validityDateCounterDatePicker,
                initialCounterIndexIntegerFiled, discountValueIntegerField, discountCodeTextField, counterNumberTextFiled);
        formLayout.setColspan(consumptionPointComboBox, 2);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(houseComboBox, 2);
        formLayout.setColspan(propertyComboBox, 2);
        formLayout.setColspan(serviceTypeComboBox, 2);
        formLayout.setColspan(calculationTypeComboBox, 2);
        formLayout.setColspan(condominiumComboBox, 2);
        formLayout.setColspan(validityDateCounterDatePicker, 2);
        formLayout.setColspan(initialCounterIndexIntegerFiled, 2);
        formLayout.setColspan(discountValueIntegerField, 2);
        formLayout.setColspan(discountCodeTextField, 2);
        formLayout.setColspan(counterNumberTextFiled, 2);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 6)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
            }
            consumptionPointService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The consumption point with the name " + binder.getBean().getCounterNumber() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(ConsumptionPoint consumptionPoint) {
        binder.setBean(consumptionPoint);
        binder.forField(consumptionPointComboBox).bind(ConsumptionPoint::getParent, ConsumptionPoint::setParent);
        binder.forField(houseComboBox).asRequired().bind(ConsumptionPoint::getHouse, ConsumptionPoint::setHouse);
        binder.forField(userComboBox).asRequired().bind(ConsumptionPoint::getUser, ConsumptionPoint::setUser);
        binder.forField(propertyComboBox).asRequired().bind(ConsumptionPoint::getProperty, ConsumptionPoint::setProperty);
        binder.forField(serviceTypeComboBox).asRequired().bind(ConsumptionPoint::getServiceType, ConsumptionPoint::setServiceType);
        binder.forField(calculationTypeComboBox).asRequired().bind(ConsumptionPoint::getCalculationType, ConsumptionPoint::setCalculationType);
        binder.forField(condominiumComboBox).asRequired().bind(ConsumptionPoint::getCondominium, ConsumptionPoint::setCondominium);
        binder.forField(validityDateCounterDatePicker).asRequired().bind(ConsumptionPoint::getValidityDateCounter, ConsumptionPoint::setValidityDateCounter);
        binder.forField(initialCounterIndexIntegerFiled).asRequired().withConverter(new IntegerToLongConverter()).bind(ConsumptionPoint::getInitialCounterIndex, ConsumptionPoint::setInitialCounterIndex);
        binder.forField(discountValueIntegerField).asRequired().withConverter(new IntegerToLongConverter()).bind(ConsumptionPoint::getDiscountValue, ConsumptionPoint::setDiscountValue);
        binder.forField(discountCodeTextField).asRequired().bind(ConsumptionPoint::getDiscountCode, ConsumptionPoint::setDiscountCode);
        binder.forField(counterNumberTextFiled).asRequired().bind(ConsumptionPoint::getCounterNumber, ConsumptionPoint::setCounterNumber);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !counterNumberTextFiled.getValue().equals("")
                && !discountCodeTextField.getValue().equals("")
                && discountValueIntegerField.getValue() != null
                && initialCounterIndexIntegerFiled.getValue() != null
                && validityDateCounterDatePicker.getValue() != null
                && condominiumComboBox.getValue() != null
                && serviceTypeComboBox.getValue() != null
                && houseComboBox.getValue() != null
                && propertyComboBox.getValue() != null
                && userComboBox.getValue() != null
                && calculationTypeComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }

}
