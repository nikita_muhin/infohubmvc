package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.data.entity.Role;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.data.entity.UserRole;
import com.infohubmvc.application.data.entity.UserStatus;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import org.vaadin.gatanaso.MultiselectComboBox;

import java.util.HashSet;
import java.util.Set;

public class UsersDialog extends Dialog {

    private UserService userService;
    private TextField usernameTextField = new TextField("Username");
    private PasswordField passwordField = new PasswordField("Password");
    private TextField firstNameTextField = new TextField("First name");
    private TextField lastNameTextField = new TextField("Last name");
    private EmailField emailField = new EmailField("Email");
    private TextField phoneTextField = new TextField("Phone");
    private ComboBox<UserStatus> userStatusComboBox = new ComboBox<>("Status");
    private MultiselectComboBox<Role> roleComboBox = new MultiselectComboBox<>("Role");
    private Button saveButton;
    private FormLayout formLayout;
    private Binder<User> binder = new Binder<>(User.class);
    private boolean isEditable;
    private boolean isFromLoginForm;

    public UsersDialog(String header, User user, boolean isEditable, boolean isFromLoginForm) throws ValidationException {
        userService = WebApplicationUtils.get(UserService.class);
        this.isEditable = isEditable;
        this.isFromLoginForm = isFromLoginForm;
        setHeaderTitle(header);
        configureCloseButton();
        configureSaveButton();
        configureComponent(user);
        configureBinder(user);
        addComponent();
        add(formLayout);
    }


    private void configureComponent(User user) {
        configureUserStatusComboBox(user);
        configureEmailField();
        configureUsernameTextField();
        configurePasswordField();
        configureFirstNameTextField();
        configureLastNameTextField();
        configurePhoneTextField();
        configureRoleComboBox(user);
    }

    private void configureUserStatusComboBox(User user) {
        userStatusComboBox.setReadOnly(!isEditable);
        if(isFromLoginForm) {
            user.setStatus(UserStatus.ACTIVE);
            userStatusComboBox.setReadOnly(true);
        }
        userStatusComboBox.setItems(UserStatus.values());
        userStatusComboBox.setItemLabelGenerator(Enum::name);
        userStatusComboBox.setValue(user.getStatus());
        userStatusComboBox.setWidthFull();
        userStatusComboBox.setRequired(true);
        userStatusComboBox.addValueChangeListener(this::setEnableSaveButton);

    }

    private void configureRoleComboBox(User user) {
        Set<Role> roleSet = new HashSet<>();
        if(user.getUserRoles() != null) {
            user.getUserRoles().forEach(e -> roleSet.add(e.getRole()));
        }
        roleComboBox.setReadOnly(!isEditable);
        if(isFromLoginForm) {
            roleSet.add(Role.USER);
            Set<UserRole> userRoles = new HashSet<>();
            userRoles.add(new UserRole(true,true,user,Role.USER));
            user.setUserRoles(userRoles);
            roleComboBox.setReadOnly(true);
        }
        roleComboBox.setItems(Role.values());
        roleComboBox.setItemLabelGenerator(Enum::name);
        roleComboBox.setValue(roleSet);
        roleComboBox.setWidth("400px");
        roleComboBox.setRequired(true);
        roleComboBox.addValueChangeListener(this::setEnableSaveButton);
    }
    private void configureEmailField() {
        emailField.setWidthFull();
        emailField.setReadOnly(!isEditable);
        emailField.setRequiredIndicatorVisible(true);
        emailField.addValueChangeListener(this::setEnableSaveButton);
    }

    private void configureUsernameTextField() {
        usernameTextField.setReadOnly(!isEditable);
        usernameTextField.setRequired(true);
        usernameTextField.addValueChangeListener(this::setEnableSaveButton);
    }

    private void configurePasswordField() {
        passwordField.setReadOnly(!isEditable);
        passwordField.setRequired(true);
        passwordField.addValueChangeListener(this::setEnableSaveButton);
    }

    private void configureFirstNameTextField() {
        firstNameTextField.setReadOnly(!isEditable);
        firstNameTextField.setRequired(true);
        firstNameTextField.addValueChangeListener(this::setEnableSaveButton);
    }

    private void configureLastNameTextField() {
        lastNameTextField.setReadOnly(!isEditable);
        lastNameTextField.setRequired(true);
        lastNameTextField.addValueChangeListener(this::setEnableSaveButton);
    }

    private void configurePhoneTextField() {
        phoneTextField.setReadOnly(!isEditable);
        phoneTextField.setRequired(true);
        phoneTextField.addValueChangeListener(this::setEnableSaveButton);
    }
    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton() {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            userService.saveUser(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("User " + binder.getBean().getFirstName() + " " + binder.getBean().getLastName() + " successfully saved.");
        });
        saveButton.setVisible(isEditable);
        saveButton.setEnabled(false);
        getFooter().add(saveButton);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        HorizontalLayout horizontalLayout1 = new HorizontalLayout(usernameTextField,
                passwordField);
        HorizontalLayout horizontalLayout2 = new HorizontalLayout(firstNameTextField,
                lastNameTextField);
        HorizontalLayout horizontalLayout3 = new HorizontalLayout(emailField,
                phoneTextField);
        formLayout.add(new VerticalLayout(
                horizontalLayout1,
                horizontalLayout2,
                horizontalLayout3,
                userStatusComboBox,
                roleComboBox));
    }

    private void setEnableSaveButton(AbstractField.ComponentValueChangeEvent e) {
        if(e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !usernameTextField.getValue().equals("")
                && !passwordField.getValue().equals("")
                && !firstNameTextField.getValue().equals("")
                && !lastNameTextField.getValue().equals("")
                && !emailField.getValue().equals("")
                && !phoneTextField.getValue().equals("")
                && userStatusComboBox.getValue() != null
                && roleComboBox.getValue().size() != 0;
    }


    private void configureBinder(User user) {
        binder.setBean(user);
        binder.forField(usernameTextField).asRequired().bind(User::getUsername, User::setUsername);
        binder.forField(passwordField).asRequired().bind(User::getPassword, User::setPassword);
        binder.forField(firstNameTextField).asRequired().bind(User::getFirstName, User::setFirstName);
        binder.forField(lastNameTextField).asRequired().bind(User::getLastName, User::setLastName);
        binder.forField(emailField).asRequired().bind(User::getEmail, User::setEmail);
        binder.forField(phoneTextField).asRequired().bind(User::getPhoneNumber, User::setPhoneNumber);
        binder.forField(userStatusComboBox).asRequired().bind(User::getStatus, User::setStatus);
        binder.forField(roleComboBox).asRequired().bind(e -> getUserRole(e.getUserRoles()), this::setUserRole);
    }

    private Set<Role> getUserRole(Set<UserRole> roleSet) {
        Set<Role> roles = new HashSet<>();
        if(roleSet != null) {
            roleSet.forEach(e -> roles.add(e.getRole()));
        }
        return roles;
    }

    private User setUserRole(User user, Set<Role> roles) {
        Set<UserRole> userRoles = new HashSet<>();
        roles.forEach(e -> userRoles.add(new UserRole(true,true,user,e)));
        user.setUserRoles(userRoles);
        return user;
    }
    public Button getSaveButton() {
        return saveButton;
    }
}
