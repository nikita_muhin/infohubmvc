package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.CondominiumStatus;
import com.infohubmvc.application.service.CondominiumService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;


public class CondominiumDialog extends Dialog {

    private TextField nameTextField = new TextField("Name");
    private TextField registrationNumberTextField = new TextField("Registration Number");
    private TextField addressTextField = new TextField("Address");
    private TextField contractNumber = new TextField("Contract Number");
    private ComboBox<CondominiumStatus> statusComboBox = new ComboBox<>("Status");
    private FormLayout formLayout;
    private Button saveButton;
    private Binder<Condominium> binder = new Binder<>(Condominium.class);
    private CondominiumService condominiumService;
    private UserService userService;
    private boolean isEditable;

    public CondominiumDialog(String header, Condominium condominium, boolean isEditable) {
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        userService = WebApplicationUtils.get(UserService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("30%");
        configureCloseButton();
        configureSaveButton();
        configureComponent(condominium);
        configureBinder(condominium);
        addComponent();
        add(formLayout);
    }

    private void addComponent() {
        formLayout = new FormLayout(nameTextField,
                registrationNumberTextField,
                addressTextField,
                contractNumber,
                statusComboBox);
        formLayout.setColspan(nameTextField, 1);
        formLayout.setColspan(registrationNumberTextField, 1);
        formLayout.setColspan(addressTextField, 1);
        formLayout.setColspan(contractNumber, 1);
        formLayout.setColspan(statusComboBox, 2);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("100px", 2)
        );
    }

    private void configureComponent(Condominium condominium) {
        configureNameTextField();
        configureRegistrationNumberTextField();
        configureAddressTextField();
        configureContractNumber();
        configureStatusComboBox(condominium);
    }

    private void configureNameTextField() {
        nameTextField.setReadOnly(!isEditable);
        nameTextField.addValueChangeListener(this::setEnableSaveButton);
    }

    private void configureRegistrationNumberTextField() {
        registrationNumberTextField.setReadOnly(!isEditable);
        registrationNumberTextField.addValueChangeListener(this::setEnableSaveButton);
    }
    private void configureAddressTextField() {
        addressTextField.setReadOnly(!isEditable);
        addressTextField.addValueChangeListener(this::setEnableSaveButton);
    }
    private void configureContractNumber() {
        contractNumber.setReadOnly(!isEditable);
        contractNumber.addValueChangeListener(this::setEnableSaveButton);
    }
    private void configureStatusComboBox(Condominium condominium) {
        statusComboBox.setItems(CondominiumStatus.values());
        statusComboBox.setItemLabelGenerator(Enum::name);
        statusComboBox.setValue(condominium.getStatus());
        statusComboBox.setReadOnly(!isEditable);
        statusComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureBinder(Condominium condominium) {
        binder.setBean(condominium);
        binder.forField(nameTextField).bind(Condominium::getName, Condominium::setName);
        binder.forField(registrationNumberTextField).bind(Condominium::getRegistrationNumber, Condominium::setRegistrationNumber);
        binder.forField(addressTextField).bind(Condominium::getOfficeAddress, Condominium::setOfficeAddress);
        binder.forField(contractNumber).bind(Condominium::getInfohubContractNr, Condominium::setInfohubContractNr);
        binder.forField(statusComboBox).bind(Condominium::getStatus, Condominium::setStatus);
    }

    private void setEnableSaveButton(AbstractField.ComponentValueChangeEvent e) {
        if(e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !nameTextField.getValue().equals("")
                && !registrationNumberTextField.getValue().equals("")
                && !addressTextField.getValue().equals("")
                && !contractNumber.getValue().equals("")
                && statusComboBox.getValue() != null;
    }


    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton() {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (binder.getBean().getId() == null)
                binder.getBean().setUser(userService.getUserById(1L));
            condominiumService.saveCondominium(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("Condominium witch name " + binder.getBean().getName() + " successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
