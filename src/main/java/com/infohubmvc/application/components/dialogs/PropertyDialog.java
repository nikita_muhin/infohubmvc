package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.DoubleToBigDecimalConverter;
import com.infohubmvc.application.converters.IntegerToLongConverter;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.Property;
import com.infohubmvc.application.data.entity.PropertyType;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.service.PropertyService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class PropertyDialog extends Dialog {

    private PropertyService propertyService;
    private UserService userService;
    private DictionaryService dictionaryService;
    private ComboBox<PropertyType> propertyTypeComboBox = new ComboBox<>("Type");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private IntegerField floorIntegerField = new IntegerField("Floor");
    private TextField phoneTextField = new TextField("Telefon");
    private EmailField emailField = new EmailField("Email");
    private IntegerField roomIntegerField = new IntegerField("Rooms");
    private NumberField areaNumberField = new NumberField("Area");
    private TextField firstNameTextField = new TextField("First name");
    private TextField lastNameTextField = new TextField("Last name");
    private TextField addressTextField = new TextField("Address");
    private IntegerField membersIntegerField = new IntegerField("Members");
    private IntegerField staircaseIntegerFiled = new IntegerField("Staircase");
    private Button saveButton;
    private FormLayout formLayout;
    private Binder<Property> binder = new Binder<>(Property.class);
    private boolean isEditable;
    private House selectHouse;

    public PropertyDialog(String header, House selectHouse, Property selectProperty, boolean isEditable) {
        propertyService = WebApplicationUtils.get(PropertyService.class);
        userService = WebApplicationUtils.get(UserService.class);
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        this.selectHouse = selectHouse;
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureSaveButton();
        configureCloseButton();
        configureComponent(selectProperty);
        configureBinder(selectProperty);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(Property property) {
        configureUserComboBox(property);
        configurePropertyTypeComboBox(property);
        configureFirstNameTextField();
        configureLastNameTextField();
        configureAddressTextField();
        configureMembersIntegerField();
        configureStaircaseIntegerFiled();
        configureEmailField();
        configurePhoneTextField();
        configureFloorIntegerField();
        configureRoomIntegerField();
        configureAreaNumberField();
    }

    private void configureUserComboBox(Property property) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(property.getUser());
        userComboBox.setReadOnly(!isEditable);
        userComboBox.setRequiredIndicatorVisible(true);
        userComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configurePropertyTypeComboBox(Property property) {
        propertyTypeComboBox.setItems(dictionaryService.getAllPropertyTypes());
        propertyTypeComboBox.setItemLabelGenerator(PropertyType::getDescription);
        propertyTypeComboBox.setValue(property.getPropertyType());
        propertyTypeComboBox.setReadOnly(!isEditable);
        propertyTypeComboBox.setRequiredIndicatorVisible(true);
        propertyTypeComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureFirstNameTextField() {
        firstNameTextField.setReadOnly(!isEditable);
        firstNameTextField.setRequired(true);
        firstNameTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureLastNameTextField() {
        lastNameTextField.setReadOnly(!isEditable);
        lastNameTextField.setRequired(true);
        lastNameTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureAddressTextField() {
        addressTextField.setReadOnly(!isEditable);
        addressTextField.setRequired(true);
        addressTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureMembersIntegerField() {
        membersIntegerField.setReadOnly(!isEditable);
        membersIntegerField.setRequiredIndicatorVisible(true);
        membersIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureStaircaseIntegerFiled() {
        staircaseIntegerFiled.setReadOnly(!isEditable);
        staircaseIntegerFiled.setRequiredIndicatorVisible(true);
        staircaseIntegerFiled.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureEmailField() {
        emailField.setReadOnly(!isEditable);
        emailField.setRequiredIndicatorVisible(true);
        emailField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configurePhoneTextField() {
        phoneTextField.setReadOnly(!isEditable);
        phoneTextField.setRequiredIndicatorVisible(true);
        phoneTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureFloorIntegerField() {
        floorIntegerField.setReadOnly(!isEditable);
        floorIntegerField.setRequiredIndicatorVisible(true);
        floorIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureRoomIntegerField() {
        roomIntegerField.setReadOnly(!isEditable);
        roomIntegerField.setRequiredIndicatorVisible(true);
        roomIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureAreaNumberField() {
        areaNumberField.setReadOnly(!isEditable);
        areaNumberField.setRequiredIndicatorVisible(true);
        areaNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(propertyTypeComboBox, userComboBox, firstNameTextField,
                lastNameTextField, addressTextField, membersIntegerField,
                staircaseIntegerFiled, emailField, phoneTextField,
                floorIntegerField, roomIntegerField, areaNumberField);
        formLayout.setColspan(propertyTypeComboBox, 2);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(firstNameTextField, 2);
        formLayout.setColspan(lastNameTextField, 2);
        formLayout.setColspan(addressTextField, 2);
        formLayout.setColspan(membersIntegerField, 1);
        formLayout.setColspan(staircaseIntegerFiled, 1);
        formLayout.setColspan(emailField, 2);
        formLayout.setColspan(phoneTextField, 2);
        formLayout.setColspan(floorIntegerField, 1);
        formLayout.setColspan(roomIntegerField, 1);
        formLayout.setColspan(areaNumberField, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 4)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton() {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            binder.getBean().setHouse(selectHouse);
            propertyService.saveProperty(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The property with the name " + binder.getBean().getPropertyType().getDescription() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(Property property) {
        binder.setBean(property);
        binder.forField(propertyTypeComboBox).bind(Property::getPropertyType, Property::setPropertyType);
        binder.forField(userComboBox).bind(Property::getUser, Property::setUser);
        binder.forField(floorIntegerField).withConverter(new IntegerToLongConverter()).bind(Property::getFloor, Property::setFloor);
        binder.forField(phoneTextField).bind(Property::getPhoneNumbers, Property::setPhoneNumbers);
        binder.forField(emailField).bind(Property::getEmail, Property::setEmail);
        binder.forField(roomIntegerField).bind(Property::getNumberRooms, Property::setNumberRooms);
        binder.forField(areaNumberField).withConverter(new DoubleToBigDecimalConverter()).bind(Property::getArea, Property::setArea);
        binder.forField(firstNameTextField).bind(Property::getFirstName, Property::setFirstName);
        binder.forField(lastNameTextField).bind(Property::getLastName, Property::setLastName);
        binder.forField(addressTextField).bind(Property::getAddress, Property::setAddress);
        binder.forField(membersIntegerField).bind(Property::getNumberMembers, Property::setNumberMembers);
        binder.forField(staircaseIntegerFiled).bind(Property::getStaircase, Property::setStaircase);
    }

    public Button getSaveButton() {
        return saveButton;
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !firstNameTextField.getValue().equals("")
                && !lastNameTextField.getValue().equals("")
                && !addressTextField.getValue().equals("")
                && !emailField.getValue().equals("")
                && !phoneTextField.getValue().equals("")
                && membersIntegerField.getValue() != null
                && staircaseIntegerFiled.getValue() != null
                && floorIntegerField.getValue() != null
                && roomIntegerField.getValue() != null
                && areaNumberField.getValue() != null
                && propertyTypeComboBox.getValue() != null
                && userComboBox.getValue() != null;
    }
}
