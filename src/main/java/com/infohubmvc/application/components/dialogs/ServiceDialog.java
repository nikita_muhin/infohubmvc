package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.IntegerToLongConverter;
import com.infohubmvc.application.data.entity.Service;
import com.infohubmvc.application.data.entity.ServiceType;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.service.ServiceService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;


public class ServiceDialog extends Dialog {

    private ServiceService serviceService;
    private UserService userService;
    private DictionaryService dictionaryService;
    private ComboBox<ServiceType> serviceTypeComboBox = new ComboBox<>("Type");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private TextField nameTextField = new TextField("Name");
    private TextField contractNumberTextField = new TextField("Contract number");
    private IntegerField tarifIntegerField = new IntegerField("Tarif");
    private IntegerField tarifUnitIntegerField = new IntegerField("Tarif unit");
    private DatePicker fromDatePicker = new DatePicker("From");
    private DatePicker toDatePicker = new DatePicker("To");
    private Button saveButton;
    private FormLayout formLayout;
    private Binder<Service> binder = new Binder<>(Service.class);
    private boolean isEditable;

    public ServiceDialog(String header, Service service, boolean isEditable, boolean isCreate) {
        serviceService = WebApplicationUtils.get(ServiceService.class);
        userService = WebApplicationUtils.get(UserService.class);
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton(isCreate);
        configureComponent(service);
        configureBinder(service);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(Service service) {
       configureServiceTypeComboBox(service);
       configureUserComboBox(service);
       configureContractNumberTextField();
       configureNameTextField();
       configureTarifIntegerField();
       configureTarifUnitIntegerField();
       configureFromDatePicker();
       configureToDatePicker();
    }

    private void configureServiceTypeComboBox (Service service) {
        serviceTypeComboBox.setItems(dictionaryService.getAllServiceTypes());
        serviceTypeComboBox.setItemLabelGenerator(ServiceType::getDescription);
        serviceTypeComboBox.setValue(service.getServiceType());
        serviceTypeComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
        serviceTypeComboBox.setReadOnly(!isEditable);
    }
    private void configureUserComboBox (Service service) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(service.getUser());
        userComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
        userComboBox.setReadOnly(!isEditable);
    }
    private void configureNameTextField () {
        nameTextField.setReadOnly(!isEditable);
        nameTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }
    private void configureContractNumberTextField () {
        contractNumberTextField.setReadOnly(!isEditable);
        contractNumberTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }
    private void configureTarifIntegerField () {
        tarifIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
        tarifIntegerField.setReadOnly(!isEditable);
    }
    private void configureTarifUnitIntegerField () {
        tarifUnitIntegerField.setReadOnly(!isEditable);
        tarifUnitIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configureFromDatePicker () {
        fromDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
        fromDatePicker.setReadOnly(!isEditable);
    }
    private void configureToDatePicker () {
        toDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
        toDatePicker.setReadOnly(!isEditable);
    }


    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(serviceTypeComboBox, userComboBox, nameTextField, contractNumberTextField,
                tarifIntegerField, tarifUnitIntegerField, fromDatePicker, toDatePicker);
        formLayout.setColspan(serviceTypeComboBox, 2);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(nameTextField, 2);
        formLayout.setColspan(contractNumberTextField, 2);
        formLayout.setColspan(tarifIntegerField, 2);
        formLayout.setColspan(tarifUnitIntegerField, 2);
        formLayout.setColspan(fromDatePicker, 2);
        formLayout.setColspan(toDatePicker, 2);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 4)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
            }
            serviceService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The service with the name " + binder.getBean().getName() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(Service service) {
        binder.setBean(service);
        binder.forField(serviceTypeComboBox).asRequired().bind(Service::getServiceType, Service::setServiceType);
        binder.forField(userComboBox).asRequired().bind(Service::getUser, Service::setUser);
        binder.forField(nameTextField).asRequired().bind(Service::getName, Service::setName);
        binder.forField(contractNumberTextField).asRequired().bind(Service::getContractNumber, Service::setContractNumber);
        binder.forField(tarifIntegerField).withConverter(new IntegerToLongConverter()).asRequired().bind(Service::getTarif, Service::setTarif);
        binder.forField(tarifUnitIntegerField).withConverter(new IntegerToLongConverter()).asRequired().bind(Service::getTarifUnit, Service::setTarifUnit);
        binder.forField(fromDatePicker).asRequired().bind(Service::getDateFrom, Service::setDateFrom);
        binder.forField(toDatePicker).asRequired().bind(Service::getDateTo, Service::setDateTo);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !contractNumberTextField.getValue().equals("")
                && !nameTextField.getValue().equals("")
                && tarifIntegerField.getValue() != null
                && tarifUnitIntegerField.getValue() != null
                && fromDatePicker.getValue() != null
                && toDatePicker.getValue() != null
                && serviceTypeComboBox.getValue() != null
                && userComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }

}
