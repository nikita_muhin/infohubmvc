package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.data.entity.CalculationFormula;
import com.infohubmvc.application.data.entity.CalculationType;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.CalculationFormulaService;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;

public class FormulaDialog extends Dialog {

    private CalculationFormulaService calculationFormulaService;
    private DictionaryService dictionaryService;
    private UserService userService;
    private ComboBox<CalculationType> calculationTypeComboBox = new ComboBox<>("Type");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private TextField codeTextField = new TextField("Code");
    private TextField descriptionTextField = new TextField("Description");
    private TextField formulaTextField = new TextField("Formula");
    private DatePicker fromDataPicker = new DatePicker("From");
    private DatePicker toDataPicker = new DatePicker("To");
    private Checkbox isActiveCheckbox = new Checkbox("Active");
    private Button saveButton;
    private FormLayout formLayout;
    private Binder<CalculationFormula> binder = new Binder<>(CalculationFormula.class);
    private boolean isEditable;

    public FormulaDialog(String header, CalculationFormula calculationFormula, boolean isEditable, boolean isCreate) {
        calculationFormulaService = WebApplicationUtils.get(CalculationFormulaService.class);
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        userService = WebApplicationUtils.get(UserService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton(isCreate);
        configureComponent(calculationFormula);
        configureBinder(calculationFormula);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(CalculationFormula calculationFormula) {
        configureCalculationTypeComboBox(calculationFormula);
        configureUserComboBox(calculationFormula);
        configureCodeTextField();
        configureDescriptionTextField();
        configureFormulaTextField();
        configureFromDataPicker();
        configureToDataPicker();
        configureIsActiveCheckbox();
    }

    private void configureCalculationTypeComboBox(CalculationFormula calculationFormula) {
        calculationTypeComboBox.setItems(dictionaryService.getAllCalculationTypes());
        calculationTypeComboBox.setItemLabelGenerator(CalculationType::getDescription);
        calculationTypeComboBox.setValue(calculationFormula.getCalculationType());
        calculationTypeComboBox.addValueChangeListener(e -> getSaveButton().setEnabled(isRequiredField()));
        calculationTypeComboBox.setReadOnly(!isEditable);
    }

    private void configureUserComboBox(CalculationFormula calculationFormula) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(calculationFormula.getUser());
        userComboBox.addValueChangeListener(e -> getSaveButton().setEnabled(isRequiredField()));
        userComboBox.setReadOnly(!isEditable);
    }

    private void configureCodeTextField() {
        codeTextField.addValueChangeListener(this::setEnableSaveButtonForText);
        codeTextField.setReadOnly(!isEditable);
    }

    private void configureDescriptionTextField() {
        descriptionTextField.addValueChangeListener(this::setEnableSaveButtonForText);
        descriptionTextField.setReadOnly(!isEditable);
    }

    private void configureFormulaTextField() {
        formulaTextField.addValueChangeListener(this::setEnableSaveButtonForText);
        formulaTextField.setReadOnly(!isEditable);
    }

    private void configureFromDataPicker() {
        fromDataPicker.addValueChangeListener(e -> getSaveButton().setEnabled(isRequiredField()));
        fromDataPicker.setReadOnly(!isEditable);
    }

    private void configureToDataPicker() {
        toDataPicker.addValueChangeListener(e -> getSaveButton().setEnabled(isRequiredField()));
        toDataPicker.setReadOnly(!isEditable);
    }

    private void configureIsActiveCheckbox() {
        isActiveCheckbox.setReadOnly(!isEditable);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(calculationTypeComboBox, userComboBox, codeTextField, formulaTextField,
                descriptionTextField, fromDataPicker, toDataPicker, isActiveCheckbox);
        formLayout.setColspan(calculationTypeComboBox, 2);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(codeTextField, 2);
        formLayout.setColspan(formulaTextField, 2);
        formLayout.setColspan(fromDataPicker, 2);
        formLayout.setColspan(toDataPicker, 2);
        formLayout.setColspan(descriptionTextField, 2);
        formLayout.setColspan(isActiveCheckbox, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 4)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
            }
            calculationFormulaService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The formula with the name " + binder.getBean().getDescription() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(CalculationFormula calculationFormula) {
        binder.setBean(calculationFormula);
        binder.forField(calculationTypeComboBox).asRequired().bind(CalculationFormula::getCalculationType, CalculationFormula::setCalculationType);
        binder.forField(userComboBox).asRequired().bind(CalculationFormula::getUser, CalculationFormula::setUser);
        binder.forField(codeTextField).asRequired().bind(CalculationFormula::getCode, CalculationFormula::setCode);
        binder.forField(descriptionTextField).asRequired().bind(CalculationFormula::getDescription, CalculationFormula::setDescription);
        binder.forField(formulaTextField).asRequired().bind(CalculationFormula::getFormula, CalculationFormula::setFormula);
        binder.forField(isActiveCheckbox).asRequired().bind(CalculationFormula::getActive, CalculationFormula::setActive);
        binder.forField(fromDataPicker).asRequired().bind(CalculationFormula::getDateFrom, CalculationFormula::setDateFrom);
        binder.forField(toDataPicker).asRequired().bind(CalculationFormula::getDateTo, CalculationFormula::setDateTo);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !descriptionTextField.getValue().equals("")
                && !codeTextField.getValue().equals("")
                && !formulaTextField.getValue().equals("")
                && fromDataPicker.getValue() != null
                && toDataPicker.getValue() != null
                && calculationTypeComboBox.getValue() != null
                && userComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
