package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.DoubleToBigDecimalConverter;
import com.infohubmvc.application.converters.IntegerToLongConverter;
import com.infohubmvc.application.data.entity.*;
import com.infohubmvc.application.service.*;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;

public class BeneficiaryDialog extends Dialog {

    private BeneficiaryService beneficiaryService;
    private UserService userService;
    private CondominiumService condominiumService;
    private HouseService houseService;
    private ServiceService serviceService;

    private ComboBox<Condominium> condominiumComboBox = new ComboBox<>("Condominium");
    private ComboBox<House> houseComboBox = new ComboBox<>("House");
    private ComboBox<Service> serviceComboBox = new ComboBox<>("Service");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private TextField serviceNameTextField = new TextField("Service name");
    private TextField calculationTextField = new TextField("Calculation code");
    private NumberField tarifNumberField = new NumberField("Tarif");
    private IntegerField indexIntegerField = new IntegerField("Initial counter index");

    private Button saveButton;
    private FormLayout formLayout;
    private Binder<Beneficiary> binder = new Binder<>(Beneficiary.class);
    private boolean isEditable;

    public BeneficiaryDialog(String header, Beneficiary beneficiary, boolean isEditable, boolean isCreate) {
        beneficiaryService = WebApplicationUtils.get(BeneficiaryService.class);
        userService = WebApplicationUtils.get(UserService.class);
        condominiumService  = WebApplicationUtils.get(CondominiumService.class);
        houseService = WebApplicationUtils.get(HouseService.class);
        serviceService = WebApplicationUtils.get(ServiceService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton(isCreate);
        configureComponent(beneficiary);
        configureBinder(beneficiary);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(Beneficiary beneficiary) {
        configureCondominiumComboBox(beneficiary);
        configureHouseComboBox(beneficiary);
        configureServiceComboBox(beneficiary);
        configureUserComboBox(beneficiary);
        configureServiceNameTextField();
        configureCalculationTextField();
        configureTarifNumberField();
        configureIndexIntegerField();
    }

    private void configureCondominiumComboBox(Beneficiary beneficiary) {
        condominiumComboBox.setItems(condominiumService.getAll());
        condominiumComboBox.setItemLabelGenerator(Condominium::getName);
        condominiumComboBox.setValue(beneficiary.getCondominium());
        condominiumComboBox.setReadOnly(!isEditable);
        condominiumComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureHouseComboBox(Beneficiary beneficiary) {
        houseComboBox.setItems(houseService.getAll());
        houseComboBox.setItemLabelGenerator(House::getName);
        houseComboBox.setValue(beneficiary.getHouse());
        houseComboBox.setReadOnly(!isEditable);
        houseComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureServiceComboBox(Beneficiary beneficiary) {
        serviceComboBox.setItems(serviceService.findAll());
        serviceComboBox.setItemLabelGenerator(Service::getName);
        serviceComboBox.setValue(beneficiary.getService());
        serviceComboBox.setReadOnly(!isEditable);
        serviceComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureUserComboBox(Beneficiary beneficiary) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(beneficiary.getUser());
        userComboBox.setReadOnly(!isEditable);
        userComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureServiceNameTextField() {
        serviceNameTextField.setReadOnly(!isEditable);
        serviceNameTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureCalculationTextField() {
        calculationTextField.setReadOnly(!isEditable);
        calculationTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureTarifNumberField() {
        tarifNumberField.setReadOnly(!isEditable);
    tarifNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureIndexIntegerField() {
        indexIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
        indexIntegerField.setReadOnly(!isEditable);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(condominiumComboBox, houseComboBox, serviceComboBox, userComboBox,
                serviceNameTextField, calculationTextField, tarifNumberField, indexIntegerField);
        formLayout.setColspan(condominiumComboBox, 1);
        formLayout.setColspan(houseComboBox, 1);
        formLayout.setColspan(serviceComboBox, 1);
        formLayout.setColspan(userComboBox, 1);
        formLayout.setColspan(serviceNameTextField, 1);
        formLayout.setColspan(calculationTextField, 1);
        formLayout.setColspan(tarifNumberField, 1);
        formLayout.setColspan(indexIntegerField, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 2)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
            }
            beneficiaryService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The beneficiary with the name " + binder.getBean().getServiceName() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(Beneficiary beneficiary) {
        binder.setBean(beneficiary);
        binder.forField(condominiumComboBox).asRequired().bind(Beneficiary::getCondominium, Beneficiary::setCondominium);
        binder.forField(houseComboBox).asRequired().bind(Beneficiary::getHouse, Beneficiary::setHouse);
        binder.forField(serviceComboBox).asRequired().bind(Beneficiary::getService, Beneficiary::setService);
        binder.forField(serviceNameTextField).asRequired().bind(Beneficiary::getServiceName, Beneficiary::setServiceName);
        binder.forField(calculationTextField).asRequired().bind(Beneficiary::getCalculationCode, Beneficiary::setCalculationCode);
        binder.forField(tarifNumberField).asRequired().withConverter(new DoubleToBigDecimalConverter()).bind(Beneficiary::getTarif, Beneficiary::setTarif);
        binder.forField(indexIntegerField).asRequired().withConverter(new IntegerToLongConverter()).bind(Beneficiary::getInitialCounterIndex, Beneficiary::setInitialCounterIndex);
        binder.forField(userComboBox).asRequired().bind(Beneficiary::getUser, Beneficiary::setUser);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !serviceNameTextField.getValue().equals("")
                && !calculationTextField.getValue().equals("")
                && tarifNumberField.getValue() != null
                && indexIntegerField.getValue() != null
                && serviceComboBox.getValue() != null
                && houseComboBox.getValue() != null
                && userComboBox.getValue() != null
                && condominiumComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
