package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.DoubleToBigDecimalConverter;
import com.infohubmvc.application.converters.IntegerToLongConverter;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.data.entity.StatusCode;
import com.infohubmvc.application.service.CondominiumService;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.service.HouseService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;

public class HouseDialog extends Dialog {

    private HouseService houseService;
    private DictionaryService dictionaryService;
    private UserService userService;
    private CondominiumService condominiumService;
    private ComboBox<Condominium> condominiumComboBox = new ComboBox<>("Condominium");
    private TextField nameTextField = new TextField("Name");
    private TextField addressTextField = new TextField("Adresa");
    private IntegerField blocIntegerField = new IntegerField("Numar de blocuri");
    private IntegerField floorIntegerField = new IntegerField("Numar de etaje");
    private IntegerField stairsIntegerField = new IntegerField("Numar de scari");
    private IntegerField yearIntegerField = new IntegerField("Anul darii in exploatare");
    private NumberField areaIntegerField = new NumberField("Suprafata totala");
    private ComboBox<StatusCode> statusComboBox = new ComboBox<>("Status");
    private Button saveButton;
    private FormLayout formLayout;
    private Binder<House> binder = new Binder<>(House.class);
    private boolean isEditable;

    public HouseDialog(String header, House house, boolean isEditable) throws ValidationException {
        houseService = WebApplicationUtils.get(HouseService.class);
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        userService = WebApplicationUtils.get(UserService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton();
        configureComponent(house);
        configureBinder(house);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(House house) {
        configureStatusComboBox(house);
        configureCondominiumComboBox(house);
        configureNameTextField();
        configureAddressTextField();
        configureBlocIntegerField();
        configureFloorIntegerField();
        configureStairsIntegerField();
        configureYearIntegerField();
        configureAreaIntegerField();
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton() {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (binder.getBean().getId() == null)
                binder.getBean().setUser(userService.getUserById(1L));
            houseService.saveHouse(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The house with the name " + binder.getBean().getName() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(condominiumComboBox, nameTextField, addressTextField,
                blocIntegerField, floorIntegerField, statusComboBox,
                stairsIntegerField, yearIntegerField, areaIntegerField);
        formLayout.setColspan(condominiumComboBox, 2);
        formLayout.setColspan(nameTextField, 2);
        formLayout.setColspan(addressTextField, 2);
        formLayout.setColspan(blocIntegerField, 1);
        formLayout.setColspan(floorIntegerField, 1);
        formLayout.setColspan(statusComboBox, 2);
        formLayout.setColspan(stairsIntegerField, 1);
        formLayout.setColspan(areaIntegerField, 1);
        formLayout.setColspan(yearIntegerField, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 4)
        );
    }

    private void configureCondominiumComboBox(House house) {
        condominiumComboBox.setItems(condominiumService.getAll());
        condominiumComboBox.setItemLabelGenerator(Condominium::getName);
        condominiumComboBox.setValue(house.getCondominium());
        condominiumComboBox.setReadOnly(!isEditable);
        condominiumComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureStatusComboBox(House house) {
        statusComboBox.setItems(dictionaryService.getAllStatusCodes());
        statusComboBox.setItemLabelGenerator(StatusCode::getDescription);
        statusComboBox.setValue(house.getStatus());
        statusComboBox.setReadOnly(!isEditable);
        statusComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureNameTextField() {
        nameTextField.setReadOnly(!isEditable);
        nameTextField.setRequired(true);
        nameTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureAddressTextField() {
        addressTextField.setReadOnly(!isEditable);
        addressTextField.setRequired(true);
        addressTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureBlocIntegerField() {
        blocIntegerField.setReadOnly(!isEditable);
        blocIntegerField.setRequiredIndicatorVisible(true);
        blocIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureFloorIntegerField() {
        floorIntegerField.setReadOnly(!isEditable);
        floorIntegerField.setRequiredIndicatorVisible(true);
        floorIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureStairsIntegerField() {
        stairsIntegerField.setReadOnly(!isEditable);
        stairsIntegerField.setRequiredIndicatorVisible(true);
        stairsIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureYearIntegerField() {
        yearIntegerField.setReadOnly(!isEditable);
        yearIntegerField.setRequiredIndicatorVisible(true);
        yearIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureAreaIntegerField() {
        areaIntegerField.setReadOnly(!isEditable);
        areaIntegerField.setRequiredIndicatorVisible(true);
        areaIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }


    private void configureBinder(House house) {
        binder.setBean(house);
        binder.forField(condominiumComboBox).asRequired().bind(House::getCondominium, House::setCondominium);
        binder.forField(nameTextField).asRequired().bind(House::getName, House::setName);
        binder.forField(addressTextField).asRequired().bind(House::getHouseAddress, House::setHouseAddress);
        binder.forField(blocIntegerField).asRequired().withConverter(new IntegerToLongConverter()).bind(House::getNumberBlocks, House::setNumberBlocks);
        binder.forField(floorIntegerField).asRequired().withConverter(new IntegerToLongConverter()).bind(House::getNumberFloors, House::setNumberFloors);
        binder.forField(stairsIntegerField).asRequired().withConverter(new IntegerToLongConverter()).bind(House::getNumberStairs, House::setNumberStairs);
        binder.forField(yearIntegerField).asRequired().withConverter(new IntegerToLongConverter()).bind(House::getYearBuilt, House::setYearBuilt);
        binder.forField(areaIntegerField).asRequired().withConverter(new DoubleToBigDecimalConverter()).bind(House::getTotalArea, House::setTotalArea);
        binder.forField(statusComboBox).asRequired().bind(House::getStatus, House::setStatus);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !nameTextField.getValue().equals("")
                && !addressTextField.getValue().equals("")
                && blocIntegerField.getValue() != null
                && floorIntegerField.getValue() != null
                && stairsIntegerField.getValue() != null
                && yearIntegerField.getValue() != null
                && areaIntegerField.getValue() != null
                && condominiumComboBox.getValue() != null
                && statusComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
