package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.grids.HousesGrid;
import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.House;
import com.infohubmvc.application.service.HouseService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.provider.ListDataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class SelectHouseDialog extends Dialog {

    private HouseService houseService;
    private ListDataProvider<House> dataProvider;
    private Grid<House> houseGrid;
    private Button saveButton;

    public SelectHouseDialog(Condominium condominium, List<House> houseList) {
        houseService = WebApplicationUtils.get(HouseService.class);
        dataProvider = new ListDataProvider<>(houseService.getAll());
        setHeaderTitle("Select house");
        setWidthFull();
        setHeightFull();
        configureGrid(houseList);
        add(houseGrid);
        configureCloseButton();
        configureSaveButton(houseList, condominium);
    }

    private void configureGrid(List<House> houseList) {
        houseGrid = new HousesGrid();
        houseGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        houseGrid.setHeightFull();
        houseGrid.setDataProvider(dataProvider);
        for (House house : houseList) {
            dataProvider.getItems().forEach(c -> {
                if (Objects.equals(c.getId(), house.getId())) {
                    houseGrid.select(c);
                }
            });
        }
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(List<House> houseList, Condominium condominium) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            close();
//            for (House house : houseGrid.getSelectedItems()) {
//                house.setCondominium(condominium);
//                houseService.saveHouse(house);
//            }
            List<House> housesForDelete = new ArrayList<>(houseList);
            for (House house : houseGrid.getSelectedItems()) {
                AtomicBoolean isHousePresent = new AtomicBoolean(false);
                houseList.forEach(e -> {
                    if (e.getId().equals(house.getId())) {
                        isHousePresent.set(true);
                    }
                });
                if (!isHousePresent.get()) {
                    house.setCondominium(condominium);
                    houseService.saveHouse(house);
                } else {
                    for (House house1 : housesForDelete)
                        if (Objects.equals(house1.getId(), house.getId())) {
                            housesForDelete.remove(house1);
                            break;
                        }
                }
            }
            housesForDelete.forEach(e -> houseService.deleteHouse(e));
            InfoHubNotification.showSuccessful("Houses " + " " + " successfully saved.");
        });
        getFooter().add(saveButton);
    }

    public Button getSaveButton() {
        return saveButton;
    }

}
