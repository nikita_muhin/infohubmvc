package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.DoubleToBigDecimalConverter;
import com.infohubmvc.application.converters.IntegerToLongConverter;
import com.infohubmvc.application.data.entity.ConsumptionPoint;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.InvoiceElement;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.ConsumptionPointService;
import com.infohubmvc.application.service.InvoiceElementService;
import com.infohubmvc.application.service.InvoiceService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;

public class InvoiceElementDialog extends Dialog {


    private InvoiceElementService invoiceElementService;
    private InvoiceService invoiceService;
    private UserService userService;
    private ConsumptionPointService consumptionPointService;
    private ComboBox<ConsumptionPoint> consumptionPointComboBox = new ComboBox<>("Consumption Point");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private DatePicker fromDatePicker = new DatePicker("Date from");
    private DatePicker payUntilDatePicker = new DatePicker("Pay until");
    private IntegerField indiceStartIntegerField = new IntegerField("Indice start");
    private IntegerField indiceEndIntegerField = new IntegerField("Indice end");
    private NumberField untCostNumberField = new NumberField("Unt cost");
    private NumberField unitsNumberField = new NumberField("Units");
    private NumberField calculatedAmountNumberField = new NumberField("Calculated amount");
    private NumberField debtNumberField = new NumberField("Debt");
    private NumberField penaltyNumberFiled = new NumberField("Penalty");

    private Button saveButton;
    private FormLayout formLayout;
    private Binder<InvoiceElement> binder = new Binder<>(InvoiceElement.class);
    private boolean isEditable;

    public InvoiceElementDialog(String header, Invoice invoice, InvoiceElement invoiceElement, boolean isEditable, boolean isCreate) {
        invoiceElementService = WebApplicationUtils.get(InvoiceElementService.class);
        invoiceService = WebApplicationUtils.get(InvoiceService.class);
        userService = WebApplicationUtils.get(UserService.class);
        consumptionPointService = WebApplicationUtils.get(ConsumptionPointService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton(isCreate,invoice);
        configureComponent(invoiceElement);
        configureBinder(invoiceElement);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(InvoiceElement invoiceElement) {
        configureConsumptionPointComboBox(invoiceElement);
        configureUserComboBox(invoiceElement);
        configureFromDatePicker();
        configurePayUntilDatePicker();
        configureIndiceStartIntegerField();
        configureIndiceEndIntegerField();
        configureUntCostNumberField();
        configureUnitsNumberField();
        configureDebtNumberField();
        configureCalculatedAmountNumberField();
        configurePenaltyNumberFiled();
    }

    private void configureConsumptionPointComboBox(InvoiceElement invoiceElement) {
        consumptionPointComboBox.setItems(consumptionPointService.findAll());
        consumptionPointComboBox.setItemLabelGenerator(ConsumptionPoint::getDiscountCode);
        consumptionPointComboBox.setValue(invoiceElement.getConsumptionPoint());
        consumptionPointComboBox.setReadOnly(!isEditable);
        consumptionPointComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureUserComboBox(InvoiceElement invoiceElement) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(invoiceElement.getUser());
        userComboBox.setReadOnly(!isEditable);
        userComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureFromDatePicker() {
        fromDatePicker.setReadOnly(!isEditable);
        fromDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configurePayUntilDatePicker() {
        payUntilDatePicker.setReadOnly(!isEditable);
        payUntilDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }

    private void configureIndiceStartIntegerField() {
        indiceStartIntegerField.setReadOnly(!isEditable);
        indiceStartIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureIndiceEndIntegerField() {
        indiceEndIntegerField.setReadOnly(!isEditable);
        indiceEndIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureUntCostNumberField() {
        untCostNumberField.setReadOnly(!isEditable);
        untCostNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureUnitsNumberField() {
        unitsNumberField.setReadOnly(!isEditable);
        unitsNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureCalculatedAmountNumberField() {
        calculatedAmountNumberField.setReadOnly(!isEditable);
        calculatedAmountNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configureDebtNumberField() {
        debtNumberField.setReadOnly(!isEditable);
        debtNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void configurePenaltyNumberFiled() {
        penaltyNumberFiled.setReadOnly(!isEditable);
        penaltyNumberFiled.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(userComboBox, consumptionPointComboBox, fromDatePicker, payUntilDatePicker,
                calculatedAmountNumberField, debtNumberField, indiceStartIntegerField,
                indiceEndIntegerField, unitsNumberField, untCostNumberField, penaltyNumberFiled);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(consumptionPointComboBox, 2);
        formLayout.setColspan(fromDatePicker, 2);
        formLayout.setColspan(payUntilDatePicker,2);
        formLayout.setColspan(calculatedAmountNumberField, 2);
        formLayout.setColspan(debtNumberField, 2);
        formLayout.setColspan(indiceStartIntegerField, 2);
        formLayout.setColspan(indiceEndIntegerField, 2);
        formLayout.setColspan(unitsNumberField, 2);
        formLayout.setColspan(untCostNumberField, 2);
        formLayout.setColspan(penaltyNumberFiled, 2);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 6)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate, Invoice invoice) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
                binder.getBean().setInvoice(invoice);
            }
            invoiceElementService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The invoice element with the name " + binder.getBean().getPayUntil() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(InvoiceElement invoiceElement) {
        binder.setBean(invoiceElement);
        binder.forField(userComboBox).asRequired().bind(InvoiceElement::getUser, InvoiceElement::setUser);
        binder.forField(consumptionPointComboBox).asRequired().bind(InvoiceElement::getConsumptionPoint, InvoiceElement::setConsumptionPoint);
        binder.forField(fromDatePicker).asRequired().bind(InvoiceElement::getDateFrom, InvoiceElement::setDateFrom);
        binder.forField(payUntilDatePicker).asRequired().bind(InvoiceElement::getPayUntil, InvoiceElement::setPayUntil);
        binder.forField(calculatedAmountNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(InvoiceElement::getCalculatedAmount, InvoiceElement::setCalculatedAmount);
        binder.forField(debtNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(InvoiceElement::getDebt, InvoiceElement::setDebt);
        binder.forField(penaltyNumberFiled).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(InvoiceElement::getPenalty, InvoiceElement::setPenalty);
        binder.forField(untCostNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(InvoiceElement::getUntCost, InvoiceElement::setUntCost);
        binder.forField(unitsNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(InvoiceElement::getUnits, InvoiceElement::setUnits);
        binder.forField(indiceStartIntegerField).withConverter(new IntegerToLongConverter()).asRequired().bind(InvoiceElement::getIndiceStart, InvoiceElement::setIndiceStart);
        binder.forField(indiceEndIntegerField).withConverter(new IntegerToLongConverter()).asRequired().bind(InvoiceElement::getIndiceEnd, InvoiceElement::setIndiceEnd);

    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return indiceEndIntegerField.getValue() != null
                && indiceStartIntegerField.getValue() != null
                && calculatedAmountNumberField.getValue() != null
                && debtNumberField.getValue() != null
                && penaltyNumberFiled.getValue() != null
                && unitsNumberField.getValue() != null
                && untCostNumberField.getValue() != null
                && fromDatePicker.getValue() != null
                && payUntilDatePicker.getValue() != null
                && consumptionPointComboBox.getValue() != null
                && userComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }

}
