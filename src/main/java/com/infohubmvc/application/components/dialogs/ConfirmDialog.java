package com.infohubmvc.application.components.dialogs;


import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;

public class ConfirmDialog extends Dialog {

    private Button deleteButton;

    public ConfirmDialog(String header, String mainText) {
        setHeaderTitle(header);
        add(mainText);
        configureDeleteButton();
        configureCloseButton();
    }

    private void configureCloseButton() {
        Button cancelButton = new Button("Cancel", (e) -> close());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getFooter().add(cancelButton);
    }

    private void configureDeleteButton() {
        deleteButton = new Button("Delete", (e) -> close());
        deleteButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);
        deleteButton.getStyle().set("margin-right", "auto");
        deleteButton.getStyle().set("margin-right", "auto");
        getFooter().add(deleteButton);
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

}
