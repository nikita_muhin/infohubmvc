package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.grids.CondominiumsGrid;
import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.CondominiumService;
import com.infohubmvc.application.service.UserRightServer;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.provider.ListDataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class SelectCondominiumsDialog extends Dialog {

    private CondominiumService condominiumService;
    private UserRightServer userRightServer;
    private ListDataProvider<Condominium> dataProvider;
    private User user;
    private Grid<Condominium> condominiumGrid;
    private Button saveButton;

    public SelectCondominiumsDialog(Set<Condominium> selectedCondominium, User user) {
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        userRightServer = WebApplicationUtils.get(UserRightServer.class);
        this.user = user;
        setHeaderTitle("Select condominium");
        setWidthFull();
        setHeightFull();
        configureHouseGrid(selectedCondominium);
        add(condominiumGrid);
        configureCloseButton();
        configureSaveButton(selectedCondominium);

    }


    private void configureHouseGrid(Set<Condominium> selectedCondominium) {
        dataProvider = new ListDataProvider<>(condominiumService.getAll());
        condominiumGrid = new CondominiumsGrid();
        condominiumGrid.setWidthFull();
        condominiumGrid.setHeightFull();
        condominiumGrid.setDataProvider(dataProvider);
        condominiumGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        for (Condominium condominium : selectedCondominium) {
            dataProvider.getItems().forEach(c -> {
                if (Objects.equals(c.getId(), condominium.getId())) {
                    condominiumGrid.select(c);
                }
            });
        }
    }


    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(Set<Condominium> selectedCondominium) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            close();
            List<Condominium> condominiumsForDelete = new ArrayList<>(selectedCondominium);
            for (Condominium condominium : condominiumGrid.getSelectedItems()) {
                AtomicBoolean isCondominiumPresent = new AtomicBoolean(false);
                selectedCondominium.forEach(e -> {
                    if (e.getId().equals(condominium.getId())) {
                        isCondominiumPresent.set(true);
                    }
                });
                if (!isCondominiumPresent.get()) {
                    userRightServer.saveUserRights(condominium, user);
                } else {
                    for (Condominium condominium1 : condominiumsForDelete)
                        if (Objects.equals(condominium1.getId(), condominium.getId())) {
                            condominiumsForDelete.remove(condominium1);
                            break;
                        }
                }
            }
            condominiumsForDelete.forEach(e -> userRightServer.deleteUserRights(e, user));
            InfoHubNotification.showSuccessful("Condominiums " + " " + " successfully saved.");
        });
        getFooter().add(saveButton);
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
