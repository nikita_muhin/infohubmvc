package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.controller.dto.DictionaryDTO;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class TypeDialog extends Dialog {

    private DictionaryService dictionaryService;
    private TextField codeTextField = new TextField("Code");
    private TextField descriptionTextField = new TextField("Description");
    private Button saveButton;
    private FormLayout formLayout;
    private Binder<DictionaryDTO> binder = new Binder<>(DictionaryDTO.class);
    private boolean isEditable;
    private boolean isCreate;
    private String tableName;

    public TypeDialog(String header, DictionaryDTO dictionaryDTO, boolean isEditable, boolean isCreate, String tableName){
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        this.isEditable = isEditable;
        this.isCreate = isCreate;
        this.tableName = tableName;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton();
        configureComponent();
        configureBinder(dictionaryDTO);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(){
     configureCodeTextField();
     configureDescriptionField();
    }

    private void configureCodeTextField() {
        codeTextField.setReadOnly(!isCreate);
        codeTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void configureDescriptionField() {
        descriptionTextField.setReadOnly(!isEditable);
        descriptionTextField.addValueChangeListener(this::setEnableSaveButtonForText);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(codeTextField, descriptionTextField);
        formLayout.setColspan(codeTextField, 1);
        formLayout.setColspan(descriptionTextField, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 2)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton() {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            save();
            close();
            InfoHubNotification.showSuccessful("The discount with the name " + binder.getBean().getDescription() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void save() {
        switch (tableName) {
            case ("CALCULATION_TYPE"):
                dictionaryService.saveCalculationType(binder.getBean());
                break;
            case ("DISCOUNT_TYPE"):
                dictionaryService.saveDiscountType(binder.getBean());
                break;
            case ("PROPERTY_TYPE"):
                dictionaryService.savePropertyType(binder.getBean());
                break;
            case ("SERVICE_TYPE"):
                dictionaryService.saveServiceType(binder.getBean());
                break;
            case ("STATUS_CODE"):
                dictionaryService.saveStatus(binder.getBean());
                break;
            case ("UNITS"):
                dictionaryService.saveUnit(binder.getBean());
                break;
        }
    }

    private void configureBinder(DictionaryDTO dictionaryDTO) {
        binder.setBean(dictionaryDTO);
        binder.forField(codeTextField).asRequired().bind(DictionaryDTO::getCode, DictionaryDTO::setCode);
        binder.forField(descriptionTextField).asRequired().bind(DictionaryDTO::getDescription, DictionaryDTO::setDescription);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !descriptionTextField.getValue().equals("")
                && !codeTextField.getValue().equals("");
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
