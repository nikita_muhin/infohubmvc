package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.DoubleToBigDecimalConverter;
import com.infohubmvc.application.data.entity.Discount;
import com.infohubmvc.application.data.entity.DiscountType;
import com.infohubmvc.application.data.entity.Unit;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.DictionaryService;
import com.infohubmvc.application.service.DiscountService;
import com.infohubmvc.application.service.UnitService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;

public class DiscountDialog extends Dialog {

    private UserService userService;
    private DictionaryService dictionaryService;
    private UnitService unitService;
    private DiscountService discountService;

    private ComboBox<DiscountType> discountTypeComboBox = new ComboBox<>("Type");
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private ComboBox<Unit> unitComboBox = new ComboBox<>("Unit");
    private TextField descriptionTextField = new TextField("Description");
    private NumberField valueNumberField = new NumberField("Value");
    private DatePicker fromDatePicker = new DatePicker("From");
    private DatePicker toDatePicker = new DatePicker("To");
    private Checkbox isActiveCheckbox = new Checkbox("Active");

    private Button saveButton;
    private FormLayout formLayout;
    private Binder<Discount> binder = new Binder<>(Discount.class);
    private boolean isEditable;

    public DiscountDialog(String header, Discount discount, boolean isEditable, boolean isCreate) {
        userService = WebApplicationUtils.get(UserService.class);
        unitService = WebApplicationUtils.get(UnitService.class);
        dictionaryService = WebApplicationUtils.get(DictionaryService.class);
        discountService = WebApplicationUtils.get(DiscountService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton(isCreate);
        configureComponent(discount);
        configureBinder(discount);
        addComponent();
        add(formLayout);
    }


    private void configureComponent(Discount discount) {
        configureDiscountTypeComboBox(discount);
        configureUnitComboBox(discount);
        configureUserComboBox(discount);
        configureDescriptionTextField();
        configureValueNumberField();
        configureIsActiveCheckbox();
        configureFromDatePicker();
        configureToDatePicker();
    }

    private void configureDiscountTypeComboBox(Discount discount) {
        discountTypeComboBox.setItems(dictionaryService.getAllDiscountTypes());
        discountTypeComboBox.setItemLabelGenerator(DiscountType::getDescription);
        discountTypeComboBox.setValue(discount.getDiscountType());
        discountTypeComboBox.setReadOnly(!isEditable);
    }

    private void configureUserComboBox(Discount discount) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(discount.getUser());
        userComboBox.setReadOnly(!isEditable);
    }

    private void configureUnitComboBox(Discount discount) {
        unitComboBox.setItems(unitService.findAll());
        unitComboBox.setItemLabelGenerator(Unit::getDescription);
        unitComboBox.setValue(discount.getDiscountUnit());
        unitComboBox.setReadOnly(!isEditable);
    }

    private void configureDescriptionTextField() {
        descriptionTextField.addValueChangeListener(this::setEnableSaveButtonForText);
        descriptionTextField.setReadOnly(!isEditable);
    }

    private void configureValueNumberField() {
        valueNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
        valueNumberField.setReadOnly(!isEditable);
    }

    private void configureFromDatePicker() {
        fromDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
        fromDatePicker.setReadOnly(!isEditable);
    }

    private void configureToDatePicker() {
        toDatePicker.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
        toDatePicker.setReadOnly(!isEditable);
    }

    private void configureIsActiveCheckbox() {
        isActiveCheckbox.setReadOnly(!isEditable);
    }

    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(discountTypeComboBox, userComboBox, unitComboBox, descriptionTextField,
                valueNumberField, fromDatePicker, toDatePicker, isActiveCheckbox);
        formLayout.setColspan(discountTypeComboBox, 2);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(unitComboBox, 2);
        formLayout.setColspan(descriptionTextField, 2);
        formLayout.setColspan(valueNumberField, 1);
        formLayout.setColspan(fromDatePicker, 1);
        formLayout.setColspan(toDatePicker, 1);
        formLayout.setColspan(isActiveCheckbox, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 4)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if(isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
            }
            discountService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The discount with the name " + binder.getBean().getDescription() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(Discount discount) {
        binder.setBean(discount);
        binder.forField(discountTypeComboBox).asRequired().bind(Discount::getDiscountType, Discount::setDiscountType);
        binder.forField(unitComboBox).asRequired().bind(Discount::getDiscountUnit, Discount::setDiscountUnit);
        binder.forField(userComboBox).asRequired().bind(Discount::getUser, Discount::setUser);
        binder.forField(descriptionTextField).asRequired().bind(Discount::getDescription, Discount::setDescription);
        binder.forField(valueNumberField).asRequired().withConverter(new DoubleToBigDecimalConverter()).bind(Discount::getDiscountValue, Discount::setDiscountValue);
        binder.forField(isActiveCheckbox).asRequired().bind(Discount::getActive, Discount::setActive);
        binder.forField(fromDatePicker).asRequired().bind(Discount::getDateFrom, Discount::setDateFrom);
        binder.forField(toDatePicker).asRequired().bind(Discount::getDateTo, Discount::setDateTo);
    }

    private void setEnableSaveButtonForText(AbstractField.ComponentValueChangeEvent e) {
        if (e.getOldValue() != null) {
            saveButton.setEnabled(!e.getValue().equals("") && !e.getValue().equals(e.getOldValue()) && isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return !descriptionTextField.getValue().equals("")
                && fromDatePicker.getValue() != null
                && toDatePicker.getValue() != null
                && valueNumberField.getValue() != null
                && discountTypeComboBox.getValue() != null
                && userComboBox.getValue() != null
                && unitComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
