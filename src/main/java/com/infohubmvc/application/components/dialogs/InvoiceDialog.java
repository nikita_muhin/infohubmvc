package com.infohubmvc.application.components.dialogs;

import com.infohubmvc.application.components.notifications.InfoHubNotification;
import com.infohubmvc.application.converters.DoubleToBigDecimalConverter;
import com.infohubmvc.application.data.entity.Beneficiary;
import com.infohubmvc.application.data.entity.Condominium;
import com.infohubmvc.application.data.entity.Invoice;
import com.infohubmvc.application.data.entity.User;
import com.infohubmvc.application.service.BeneficiaryService;
import com.infohubmvc.application.service.CondominiumService;
import com.infohubmvc.application.service.InvoiceService;
import com.infohubmvc.application.service.UserService;
import com.infohubmvc.application.utils.WebApplicationUtils;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.data.binder.Binder;

import java.time.LocalDateTime;

public class InvoiceDialog extends Dialog {

    private InvoiceService invoiceService;
    private UserService userService;
    private CondominiumService condominiumService;
    private BeneficiaryService beneficiaryService;
    private ComboBox<User> userComboBox = new ComboBox<>("User");
    private ComboBox<Condominium> condominiumComboBox = new ComboBox<>("Condominium");
    private ComboBox<Beneficiary> beneficiaryComboBox = new ComboBox<>("Beneficiary");
    private IntegerField invMonthIntegerField = new IntegerField("Month");
    private IntegerField invYearIntegerField = new IntegerField("Year");
    private NumberField calculatedAmountNumberField = new NumberField("Calculated amount");
    private NumberField debtNumberField = new NumberField("Debt");
    private NumberField penaltyNumberField = new NumberField("Penalty");

    private Button saveButton;
    private FormLayout formLayout;
    private Binder<Invoice> binder = new Binder<>(Invoice.class);
    private boolean isEditable;

    public InvoiceDialog(String header, Invoice invoice, boolean isEditable, boolean isCreate) {
        invoiceService = WebApplicationUtils.get(InvoiceService.class);
        userService = WebApplicationUtils.get(UserService.class);
        condominiumService = WebApplicationUtils.get(CondominiumService.class);
        beneficiaryService = WebApplicationUtils.get(BeneficiaryService.class);
        this.isEditable = isEditable;
        setHeaderTitle(header);
        setWidth("40%");
        configureCloseButton();
        configureSaveButton(isCreate);
        configureComponent(invoice);
        configureBinder(invoice);
        addComponent();
        add(formLayout);
    }

    private void configureComponent(Invoice invoice) {
        configureCondominiumComboBox(invoice);
        configureUserComboBox(invoice);
        configureBeneficiaryComboBox(invoice);
        configureInvMonthIntegerField();
        configureInvYearIntegerField();
        configureDebtNumberField();
        configurePenaltyNumberField();
        configureCalculatedAmountNumberField();
    }

    private void configureUserComboBox (Invoice invoice) {
        userComboBox.setItems(userService.getAll());
        userComboBox.setItemLabelGenerator(e -> e.getFirstName() + " " + e.getLastName());
        userComboBox.setValue(invoice.getUser());
        userComboBox.setReadOnly(!isEditable);
        userComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureCondominiumComboBox (Invoice invoice) {
        condominiumComboBox.setItems(condominiumService.getAll());
        condominiumComboBox.setItemLabelGenerator(Condominium::getName);
        condominiumComboBox.setValue(invoice.getCondominium());
        condominiumComboBox.setReadOnly(!isEditable);
        condominiumComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureBeneficiaryComboBox (Invoice invoice) {
        beneficiaryComboBox.setItems(beneficiaryService.findAll());
        beneficiaryComboBox.setItemLabelGenerator(Beneficiary::getCalculationCode);
        beneficiaryComboBox.setValue(invoice.getBeneficiary());
        beneficiaryComboBox.setReadOnly(!isEditable);
        beneficiaryComboBox.addValueChangeListener(e -> saveButton.setEnabled(isRequiredField()));
    }
    private void configureInvMonthIntegerField () {
        invMonthIntegerField.setReadOnly(!isEditable);
        invMonthIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configureInvYearIntegerField () {
        invYearIntegerField.setReadOnly(!isEditable);
        invYearIntegerField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configureCalculatedAmountNumberField () {
        calculatedAmountNumberField.setReadOnly(!isEditable);
        calculatedAmountNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configureDebtNumberField () {
        debtNumberField.setReadOnly(!isEditable);
        debtNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }
    private void configurePenaltyNumberField () {
        penaltyNumberField.setReadOnly(!isEditable);
        penaltyNumberField.addValueChangeListener(this::setEnableSaveButtonForNumber);
    }


    private void addComponent() {
        formLayout = new FormLayout();
        formLayout.add(condominiumComboBox, userComboBox, beneficiaryComboBox, invMonthIntegerField,
                invYearIntegerField, calculatedAmountNumberField, debtNumberField, penaltyNumberField);
        formLayout.setColspan(condominiumComboBox, 2);
        formLayout.setColspan(userComboBox, 2);
        formLayout.setColspan(beneficiaryComboBox, 2);
        formLayout.setColspan(invMonthIntegerField, 1);
        formLayout.setColspan(invYearIntegerField, 1);
        formLayout.setColspan(calculatedAmountNumberField, 2);
        formLayout.setColspan(debtNumberField, 1);
        formLayout.setColspan(penaltyNumberField, 1);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("200px", 4)
        );
    }

    private void configureCloseButton() {
        Button closeButton = new Button(new Icon("lumo", "cross"), (e) -> close());
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        getHeader().add(closeButton);
    }

    private void configureSaveButton(boolean isCreate) {
        saveButton = new Button("Save");
        saveButton.addClickListener(event -> {
            if (isCreate) {
                binder.getBean().setDateCreated(LocalDateTime.now());
            }
            invoiceService.save(binder.getBean());
            close();
            InfoHubNotification.showSuccessful("The formula with the name " + binder.getBean().getCalculatedAmount() + " has been successfully saved.");
        });
        saveButton.setVisible(isEditable);
        getFooter().add(saveButton);
    }

    private void configureBinder(Invoice invoice) {
        binder.setBean(invoice);
        binder.forField(condominiumComboBox).asRequired().bind(Invoice::getCondominium, Invoice::setCondominium);
        binder.forField(userComboBox).asRequired().bind(Invoice::getUser, Invoice::setUser);
        binder.forField(beneficiaryComboBox).asRequired().bind(Invoice::getBeneficiary, Invoice::setBeneficiary);
        binder.forField(invMonthIntegerField).asRequired().bind(Invoice::getInvMonth, Invoice::setInvMonth);
        binder.forField(invYearIntegerField).asRequired().bind(Invoice::getInvYear, Invoice::setInvYear);
        binder.forField(calculatedAmountNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(Invoice::getCalculatedAmount, Invoice::setCalculatedAmount);
        binder.forField(debtNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(Invoice::getDebt, Invoice::setDebt);
        binder.forField(penaltyNumberField).withConverter(new DoubleToBigDecimalConverter()).asRequired().bind(Invoice::getPenalty, Invoice::setPenalty);
    }

    private void setEnableSaveButtonForNumber(AbstractField.ComponentValueChangeEvent e) {
        if (e.getValue() != null) {
            saveButton.setEnabled(isRequiredField());
        } else {
            saveButton.setEnabled(false);
        }
    }

    private Boolean isRequiredField() {
        return invMonthIntegerField.getValue() != null
                && invYearIntegerField.getValue() != null
                && calculatedAmountNumberField.getValue() != null
                && debtNumberField.getValue() != null
                && penaltyNumberField.getValue() != null
                && beneficiaryComboBox.getValue() != null
                && condominiumComboBox.getValue() != null
                && userComboBox.getValue() != null
                && userComboBox.getValue() != null;
    }

    public Button getSaveButton() {
        return saveButton;
    }
}
