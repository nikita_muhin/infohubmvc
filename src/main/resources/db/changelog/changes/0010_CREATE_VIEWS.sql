create or replace view dictionary as
select 'CALCULATION_TYPE' as code, 'Calculation types' as description
union all
select 'DISCOUNT_TYPE' as code, 'Discount types' as description
union all
select 'PROPERTY_TYPE' as code, 'Property types' as description
union all
select 'SERVICE_TYPE' as code, 'Service types' as description
union all
select 'STATUS_CODE' as code, 'Status codes' as description
union all
select 'UNITS' as code, 'Units' as description
order by 1
